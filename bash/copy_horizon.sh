#!/bin/bash

#Put this in the HORIZON directory, and use it to copy all sink_props files that
#I actually have matched ones for into a local directory

for i in $(cat nstepcoarse_AGN.txt); do
    cp /greenwhale/jeg/Sugata/H-AGN/SINK_PROPS/sink_$i.dat ./SINK_PROPS/
done
