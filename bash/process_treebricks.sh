#!/bin/bash

echo "Which output do you want to process? Type 'all' for all"
read output

if (("$output" != "all")); then
    echo "You chose output" $output
else
    echo "All outputs chosen" $output
    output=$( ls tree_bricks* | tr -d 'tree_bricks')
    echo $output
fi 


for i in $output; do
    for npart in '10' '100' '1000' ; do
	echo '##################'
	echo "Calling the routine with" $npart "particles for" $i
	idl -e get_halo_info,nout=$i,boxsize=1020.6794,minnum=$npart
    done
done