import sinks
import math
import myutils
import oldplots
import analytic_drag as analytic
import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.lines as mlines
import numpy as np
from yt.fields.api import ValidateParameter
from yt import derived_field
import yt
import collections as clct
import os
import csv
from scipy.optimize import fsolve

font_size=16
mpl.rcParams.update({'font.size': font_size})
all_machs=['mach0.0','mach0.5','mach0.9','mach1.01','mach1.2','mach1.5','mach3','mach10']
all_colours=[3,5]

def resample(folder='.',nsample=None,sinkfile=None):
    ds=myutils.load(1,folder=folder)
    if not sinkfile:
        sinkfile=sinks.Sinkfile(folder=folder,ds=ds)
    
    print("From File",sinkfile.getattr('dotMacc').mean())

    if nsample:
        time=sinkfile.getattr('time')[::nsample].copy()
        time_step=(time[1:]-time[:-1])
        masses=sinkfile.getattr('BHmass')[::nsample].copy()
        dotMacc=(masses[1:]-masses[:-1])/time_step
        return time[:-1],dotMacc,masses[:-1]

    else:
        for nsample in np.logspace(0,3,10):
            nsample=int(nsample)
            time=sinkfile.getattr('time')[::nsample].copy()
            time_step=(time[1:]-time[:-1])
            masses=sinkfile.getattr('BHmass')[::nsample].copy()
            dotMacc=(masses[1:]-masses[:-1])/time_step
            print(nsample,':',dotMacc.mean())

def final_runtime(folder='.'):
    run_parameters=get_parameters(folder)
    scale_radius=get_time_unit(folder)

    outputs=[int(name[-5:]) for name in os.listdir(folder) if name[:6]=='output']
    ds=myutils.load(max(outputs),folder)
    print(make_name(folder),ds.current_time/scale_radius)
    
    return max(outputs)

def get_scale_radius(folder):
    run_parameters=get_parameters(folder)
    scale_radius=run_parameters['alpha']
    mach=run_parameters['mach']
    if mach>1:
        scale_radius=2*scale_radius/mach**2
    return scale_radius

def get_time_unit(folder,real_time=False):
    scale_radius=get_scale_radius(folder)
    run_parameters=get_parameters(folder)
    mach=run_parameters['mach']
    unit_t=scale_radius
    if real_time:
        unit_t=unit_t/(1+mach)
    elif mach>2.9 and run_parameters['nres']>500:
        unit_t=unit_t/(mach)
    if run_parameters['nres']>1000:
        unit_t=unit_t/2
    if mach==10:
        unit_t=unit_t/1.25
    if run_parameters['gamma']<1.3 and run_parameters['nres']>200:
        print('time cheat')
        unit_t=unit_t/2
    

    return unit_t

def get_output_time(time,guess=1,folder='.',real_time=False):
    outputs=np.array([int(name[-5:]) for name in os.listdir(folder) if name[:6]=='output'])
    output_max=outputs.max()
    run_parameters=get_parameters(folder)
    time_unit=get_time_unit(folder,real_time=real_time)
    t=0
    output=outputs[outputs>1].min()-1
    while t<time and output<output_max:
        output+=1
        ds=myutils.load(output,folder=folder,silent=True)
        t=ds.current_time.in_units('code_time')/time_unit
    output=max(output,2)
    return output

def get_parameters(folder='.'):
    with open('{0}/parameters.out'.format(folder)) as f:
        reader=myutils.csv.reader(f)
        rows=[row[0].split(' = ') for row in reader]

        parameters={}
        for row in rows:
            parameters[row[0].split(' [')[0]]=float(row[-1])

    return parameters

def make_name(folder):
    params=get_parameters(folder)
    mach=params['mach']
    nres=params['nres']
    if nres<1:
        N=str(round(nres,2))
    elif nres<5:
        N=str(round(nres,1))
    else:
        N=str(int(nres))
    if mach>=10:
        mach=int(mach)
    name="m{0}n{1}".format(str(mach),N)
    if params['gamma']>1.3:
        name+='a'
    else:
        name+='i'
    return name

def get_resolutions(folder):
    resolutions=["./{0}/{1}".format(folder,resolution) for resolution in os.listdir(folder) if resolution[0]=='n']
    resolutions=[resolution for resolution in resolutions if os.path.isdir("{0}/output_00001/".format(resolution))]
    nres=[get_parameters(folder)['nres'] for folder in resolutions]
    resolutions=[value for y,value  in sorted(zip(nres, resolutions))]
    return resolutions


def wake_size(outputs,normalized=True):
    fig,ax=plt.subplots()
    mach=get_parameters()['mach']
    for i,output in enumerate(outputs):
        sink=sinks.read(output)[0]
        box=add_drag_fields(myobject=sink.ds.all_data(),sink=sink,rho0=1)

        colour=sinks.ygb_big(i)
        density_cuts=np.logspace(-6,1,100)

        sizes=[overdensity_region(box,density_cut=density_cut)['r_tosink'].max() for density_cut in density_cuts]
        if normalized:
            sizes=sizes/sink.ds.current_time.in_units('code_time')
        ax.plot(density_cuts,sizes,color=colour,label="{0}: cs*t/Rbondi={1}".format(output,round(sink.ds.current_time.in_units('code_time')/sink.mass.in_units('code_mass'),2)))

    if normalized:
        ax.axhline(1+mach,color='k',linestyle=":",label='t x (cs+v)')

    ax.set_xscale('log')
    ax.set_xlabel('percentage cut in overdensity')
    if normalized:
        ax.set_ylabel('Rmax in [1/cs*t]')
    else:
        ax.set_ylabel('Rmax in [code_length]')
    ax.set_yscale('log')
    ax.legend(frameon=False,loc=3)
    plotname='wake_size'
    if normalized:
        plotname+='_cst'
    else:
        plotname+='_codelength'
    print("Saving as {0}.png".format(plotname))
    plt.savefig(plotname)


#replicate Fig5 from Chapon2013
def fdrag(rho0=1,G=1,cs=1,folders=['.'],addendum=None,colours=[0,0],pdf=True):
    #Function to actually calculate values and overplot

    fig,ax=plt.subplots()
    machs=np.linspace(0,11,500)
    colours=myutils.get_colours(colours[0],colours[1])
    ax.plot(machs,[analytic.Fdrag_analytic(m,3.2) for m in machs],color='k',label=r'$\ln(\Lambda) = 3.2$ (Chapon2013)',linestyle='--')
    ax.plot(machs,[analytic.Fdrag_analytic(m,3.9) for m in machs],color='k',label='$\ln(\Lambda)= 3.9$',linestyle='-')
    ax.plot(machs,[analytic.Fdrag_analytic(m,5) for m in machs],color='k',label='$\ln(\Lambda) = 5.0$',linestyle=':')
    ax.axvline(1.0,linestyle='-',color='grey')    
    for k,folder in enumerate(folders):
        print("##########################################")
        mach=get_parameters(folder)['mach']
        print("Run:",folder,mach)
        cell_drag=Fdrag(folder,real_time=False)
        times=range(10,35,3)[:-1]
        fdrag_3d=[np.linalg.norm(cell_drag.at_time(time)) for time in times]
        colour=colours[k]
        ax.plot(mach*np.ones(len(times)),fdrag_3d,color=colour,marker='o',markeredgewidth=0)

    plt.legend(frameon=False)
    plt.ylabel(r'$F^D_\diamond$  [$4 \pi \rho_\infty (GM_{sink})^2/c_{s,\infty}^2]$')
    plt.xlabel(r'$\mathcal{M}_\infty$')
    plotname='fdrag'
    if len(folders)>1:
        plotname+="_{0}".format(len(folders))
    if addendum:
        plotname+='_{0}'.format(addendum)
    myutils.saveas(fig,plotname,addendum=False,pdf=pdf)
    return 


def fdrag_deltaX(folders=None,create_file=False,on_the_fly=False,addendum=None,colours=None,pdf=False):
    fig,ax=plt.subplots()
    
    if not colours:
        colours=myutils.get_colours(all_colours[0],all_colours[1])
    if not folders:
        folders=all_machs

    for i, folder in enumerate(folders):
        resolutions=["./{0}/{1}".format(folder,resolution) for resolution in os.listdir(folder) if resolution[0]=='n']
        resolutions=[resolution for resolution in resolutions if os.path.isdir("{0}/output_00001/".format(resolution))]

        print(folder,resolutions)
        if len(resolutions)==0:
            print("There are no folders named n* here")
            return
        fdrag_slice=[];nres=[];fdrag_cells=[]; fdrag_numeric=[];
        for resolution in resolutions:
            print("Working on ",resolution)

            #Load pre=existing file with values
            cell_drag=Fdrag(folder=resolution,real_time=False)
            times=cell_drag.times
            forces=cell_drag.forces

            output_times=range(10,20)
            if times.max()>min(output_times):
                drags=np.array([np.linalg.norm(cell_drag.at_time(time)) for time in output_times])
                fdrag_cells.append(drags.mean())
                nres.append(cell_drag.parameters['nres'])
            else:
                print("NOT ENOUGH OUTPUTS",resolution)
            
            if on_the_fly:
                ds=myutils.load(cell_drag.outputs[0],folder=resolution)
                sinkfile=sinks.Sinkfile(resolution,create_file=create_file,ds=ds)
                x_data=sinkfile.getattr('time')
                radii=np.ones(len(x_data))*cell_drag.radius
                x_data=sinkfile.getattr('time')/radii
                numeric=sinkfile.getattr('Idrag')
                numeric=time_average(numeric,x_data,cell_drag.parameters['mach'],cell_drag.parameters['nres'])
                fdrag_numeric.append(numeric.mean())

        fdrag_cells=np.array([value for y,value  in sorted(zip(nres, fdrag_cells))])
        if on_the_fly:
            fdrag_numeric=np.array([value for y,value  in sorted(zip(nres, fdrag_numeric))])
        nres=sorted(nres)

        #ax.plot(nres,fdrag_slice/ostriker_scaling,color=colours[i],marker='o',label=folder)
        ax.plot(nres,fdrag_cells,color=colours[i],marker='o',label=folder)
        if on_the_fly:
            ax.plot(nres,fdrag_numeric+fdrag_cells,color=colours[i],marker='^',linestyle='--')

    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_ylim(1E-2,5E0)
    #ax.set_ylim(0,3.5)
    ax.legend(loc=2,frameon=False,fontsize=10)
    ax.set_xlabel(r'$N$')
    if not on_the_fly:
        ax.set_ylabel(r'$F^D_\diamond$  [$4 \pi \rho_\infty (GM_{sink})^2/c_{s,\infty}^2]$')
    else:
        ax.set_ylabel(r'$F^D  [$4 \pi \rho_\infty (GM_{sink})^2/c_{s,\infty}^2]$')
    plotname='fdrag_deltaX'
    if on_the_fly:
        plotname+='_numeric'
    myutils.saveas(fig,plotname,pdf=pdf,addendum=addendum)


def fdrag_angle(create_file=False,folders=None,limits=[0,25],colours=[0,0],components=True,pdf=True,fontsize=font_size):
    if not folders:
        folders=all_machs

    if components:
        fig,(ax2,ax4,ax5)=plt.subplots(3,1,sharex=True,figsize=(8.27,7))
    else:
        fig,(ax4,ax5)=plt.subplots(2,1,sharex=True,figsize=(8.27,9))

    colours=myutils.get_colours(colours[0],colours[1])[::-1]
    for ax in [ax4,ax5]:
        ax.axhline(1.0,linestyle='--',color='k')
        ax.axhline(0.0,linestyle=":",color='k')

    time=25
    axis=np.array([1/np.sqrt(2),1/np.sqrt(2),0])
    for i,folder in enumerate(folders):
        resolutions=get_resolutions(folder)

        #Load converged drag force, using mag from final, and flow axis          
        cell_fdrag=Fdrag(folder=resolutions[-2],real_time=False)
        fdrag_converged=np.linalg.norm(cell_fdrag.at_time(time))*axis

        for j,resolution in enumerate(resolutions[::-1]):
            parameters=get_parameters(resolution)
            mach=parameters['mach']
            radius=get_time_unit(resolution)

            ds=myutils.load(1,folder=resolution)
            sinkfile=sinks.Sinkfile(resolution,create_file=create_file,ds=ds)
            x_data=sinkfile.getattr('time')/radius


            gas_velocities=np.array([velocity/np.linalg.norm(velocity) for velocity in sinkfile.getattr('vrel')])
            angles=np.array([np.arccos(np.dot(velocity,axis))/np.pi for velocity in gas_velocities])
            if components:
                #ax1.plot(x_data,angles,color=colours[j])
                ax2.plot(x_data,sinkfile.getattr('Fdrag'),color=colours[j])

            cell_fdrag=Fdrag(folder=resolution,create_file=False)
            #cell_angles=np.array([np.arccos(np.dot(fdrag,axis)/np.linalg.norm(fdrag))/np.pi for fdrag in cell_fdrag.forces])
            #cell_magnitude=np.array([np.linalg.norm(fdrag) for fdrag in cell_fdrag.forces])
            #if components:
            #    #ax1.plot(cell_fdrag.times,cell_angles,color=colours[j],marker='o',linestyle='')
            #    ax2.plot(cell_fdrag.times,cell_magnitude,color=colours[j],marker='o',linestyle='')

            #Compare output cell data to fiducial
            times=cell_fdrag.times

            #Compare finestep subgrid data to fiducial
            fine_forces_all=np.multiply(sinkfile.getattr('Fdrag').reshape(-1,1),gas_velocities)
            dot_fine=np.array([np.dot(fine/np.linalg.norm(fine),fdrag_converged/np.linalg.norm(fdrag_converged)) for fine in fine_forces_all])
            ax4.plot(x_data,dot_fine,color=colours[j],linestyle='-')

            dot_cells=np.array([np.dot(cell/np.linalg.norm(cell),fdrag_converged/np.linalg.norm(fdrag_converged)) for cell in cell_fdrag.forces])
            ax4.plot(times,dot_cells,color=colours[j],linestyle='',marker='o')


             #Get values from finestep data                                                                                       
            #print x_data[-1],cell_fdrag.times[-1]                                                                                       
            fine_velocities=[gas_velocities[x_data>=time*0.999][0] for time in cell_fdrag.times if time<=x_data[-1]]                  
            fine_magnitudes=[sinkfile.getattr('Fdrag')[x_data>=time*0.999][0] for time in cell_fdrag.times if time<=x_data[-1]]       
            times=cell_fdrag.times[:len(fine_velocities)]                 
            cell_forces=cell_fdrag.forces[:len(fine_velocities)]
            fine_forces_output=np.array([vel*mag for vel,mag in zip(fine_velocities,fine_magnitudes)])  

            if len(fine_forces_output)>0:
                total_forces=fine_forces_output+cell_forces
                dot_products=np.array([np.dot(total,fdrag_converged)/np.linalg.norm(fdrag_converged)**2 for total in total_forces])
                ax5.plot(times,dot_products,color=colours[j],linestyle='-',marker='o')

        colours=colours[::-1]
        for j,resolution in enumerate(resolutions[:]):
            cell_fdrag=Fdrag(folder=resolution,create_file=False)
            cell_angles=np.array([np.arccos(np.dot(fdrag,axis)/np.linalg.norm(fdrag))/np.pi for fdrag in cell_fdrag.forces])
            cell_magnitude=np.array([np.linalg.norm(fdrag) for fdrag in cell_fdrag.forces])
            if components:
                #ax1.plot(cell_fdrag.times,cell_angles,color=colours[j],marker='o',linestyle='')                                                                    
                ax2.plot(cell_fdrag.times,cell_magnitude,color=colours[j],marker='o',linestyle='')

    legend_folders(ax5,resolutions,colours,name=True,fontsize=fontsize-2,columns=3,loc=2)#,anchor=[0.95,0.85])#loc='lower right',anchor=[1.0,0.02],columns=3,fontsize=fontsize-2)
    if components:
        #Add legend to magnitude panel
        legend_lines=[mlines.Line2D([], [], color='k', label=label,linestyle=linestyle,marker=marker) for label,linestyle,marker in zip([r'$F^D_\bullet$',r'$F^D_\diamond$'],['-',''],['','o'])]
        legend=ax2.legend(handles=legend_lines,frameon=False,loc=1,ncol=2,fontsize=fontsize-2)
        ax2.add_artist(legend)

        #ax1.set_xlim(limits[0],limits[1])
        #ax1.set_ylim(0,1.1)
        #ax2.set_yscale('log')
        #ax2.set_ylim(1E-3,1E0)
        #ax1.set_ylabel(r'$\theta$: $v_\infty \cdot F^D$ [rad/$\pi$]')
        ax2.set_ylabel(r'$F^D$')

    ax4.set_xlim(limits[0],limits[1])
    for ax in [ax4,ax5]:
            y_limits=ax.get_ylim()
            ax.set_ylim(min(-0.25,y_limits[0]*1.1),y_limits[1]*1.1)

    ax4.set_ylim(-1.5,1.5)
    ax5.set_ylim(-2,3.0)
    ax5.set_xlabel('time')
    ax4.set_ylabel(r'$\hat{\mathbf{F}}^D \cdot \hat{\mathbf{F}}^D_{fid}$',fontsize=fontsize-1)
    ax5.set_ylabel(r'$(\mathbf{F}^D_\bullet+\mathbf{F}^D_\diamond) \cdot \mathbf{F}^D_{fid} / {{F}^D_{fid}}^2$',fontsize=fontsize-1)


    for ax in [ax2,ax4,ax5]:
        ax.yaxis.set_label_coords(-0.1,0.5)
    plt.tight_layout(h_pad=0.0)
    filename='fdrag_angle'
    if components:
        filename+='_component'
    if pdf:
        filename+='.pdf'
    else:
        filename+='.png'
    print("Saving as",filename)
    plt.savefig(filename)
    plt.close()

def dotM_deltaX(create_file=False,folders=None,addendum=None,pdf=False):
    rho=1; cs=1;

    fig,ax = plt.subplots()

    colours=myutils.get_colours(all_colours[0],all_colours[1])
    if not folders:
        folders=all_machs
    #colours={'mach0.0':colours[1],'mach0.5':colours[2],'mach0.9':colours[3],'mach1.01':colours[4],'mach1.5':colours[5],'mach3':colours[6],'mach10':colours[7]}

    ax.axhline(1.0,linestyle='--',color='grey')
    for i,folder in enumerate(folders):
        resolutions=["./{0}/{1}".format(folder,resolution) for resolution in os.listdir(folder) if resolution[0]=='n']
        resolutions=[resolution for resolution in resolutions if os.path.isdir("{0}/output_00001/".format(resolution))]
        
        print(resolutions)
        dotM=[];nres=[]
        for resolution in resolutions:
            ds=myutils.load(1,folder=resolution)
            sinkfile=sinks.Sinkfile(resolution,create_file=create_file,ds=ds)
            params=get_parameters(resolution)
            mach=params['mach']
            ncells=params['nres']

            radius=get_time_unit(resolution)
                
            bondi_analytic=sinkfile.getattr('BHmass')**2*4*np.pi*rho/(cs**3*(1+mach**2)**(3./2))
            accretion=time_average(sinkfile.getattr('dotMacc')/bondi_analytic,sinkfile.getattr('time')/radius,mach,ncells)

            dotM.append(accretion.mean())
            nres.append(ncells)
            
            del sinkfile
            del ds
        dotM=[value for y,value  in sorted(zip(nres, dotM))]
        nres=sorted(nres)    
        print(nres,dotM)
        ax.plot(nres,dotM,marker='o',color=colours[i],label=folder)
        
    #ax.set_xscale('log')
    all_N=ax.get_xticks()
    r_star=2/np.array(all_N[1:-1])

    #ax2 = ax.twiny()
    #ax2.set_xticks(r_star);
    #ax2.invert_xaxis()
    ax.set_xscale('log')
    #ax2.set_xscale('log')
    ax.legend(loc=2,frameon=False,fontsize=10)
    ax.set_xlabel(r'$N$')
    ax.set_ylabel(r'<$\dot{M}/\dot{M}_\infty^{\rm BHL}$>')


    plotname='dotM_deltaX_{0}'.format(len(folders))
    if addendum:
        plotname+='_{0}'.format(addendum)
    if pdf:
        plotname+='.pdf'
    plt.savefig(plotname)
    plt.clf()


def get_colours(red,blue):
    if not(red+blue):
        colours=['#8dd3c7','#bebada','#fb8072','#80b1d3','#fdb462','#b3de69','#fccde5','#d9d9d9','#bc80bd','#ccebc5','#ffed6f']+['#f7f4f9','#e7e1ef','#d4b9da','#c994c7','#df65b0','#e7298a','#ce1256','#980043','#67001f'][::-1]
    else:
        reds=['#ffffd4','#fed98e','#fe9929','#d95f0e','#993404'][::-1]  #['#fecc5c','#fd8d3c','#f03b20','#bd0026'][::-1]
        blues=['#c7e9b4','#7fcdbb','#41b6c4','#2c7fb8','#253494']      #~['#ffffcc','#a1dab4','#41b6c4','#2c7fb8','#253494']    ['#ccebc5','#7bccc4','#43a2ca','#0868ac','#016c59']   #['#bdc9e1','#a1dab4','#2b8cbe','#016c59']
        if blue<=3:
            blues=['#c7e9b4','#7fcdbb','#2c7fb8','#253494']
        colours=reds[:red]+blues[:blue]
    if len(colours)==1:
        return colours[0]
    else:
        return colours


def time_average(data,x_data,mach,ncells):

    data=np.array(data)
    x_data=np.array(x_data)

    data=data[x_data>10]

    print(len(data)," values to average over")
    print("Maximum time",x_data.max())
    return data


def accretion(cs=1,rho=1,nsmooth=1,create_file=False,folders=['./'],legend_panel=0,legend_columns=1,legend_loc=0,limits=[None,None],nfields=4,addendum=None,colours=[0,0],pdf=True,name=True,mach0=False,velocities_legend=None):
    legend_lines=[]
    labels=clct.defaultdict(dict)
    x='time'
    fields=['accretion','rho','velocities','mach'] #, 'R']#'BHmass [M_initial]','radii']#,'fdrag']
    field_labels={'accretion':r'$\dot{M}_\bullet/\dot{M}_\infty^{BHL}$','rho':r'$\rho_\bullet/\rho_\infty$','velocities':'velocity','mach':r'$\mathcal{M}_\bullet/\mathcal{M}_\infty$'}
    xlabel=r't'
    colours=myutils.get_colours(colours[0],colours[1])

    if len(folders)==1:
        try:
            get_parameters(folders[0])
        except:
            print("Loading resolutions")
            folders=get_resolutions('.')[::-1]
            colours=colours[::-1]

    if not(nfields):
        nfields=len(fields)

    fig,axes=plt.subplots(nfields,1,figsize=(8.27,2.7*nfields),sharex=True)
    nsmooth_orig=nsmooth

    dotM=[]
    nres=[]
    for j,folder in enumerate(folders):
        print("#####################")
        print("working on",folder)
        parameters=get_parameters(folder)
        mach=parameters['mach']
        Mbh=parameters['alpha']
        ds=myutils.load(1,folder)

        colour=colours[j]
        nsmooth=nsmooth_orig

        sinkfile=sinks.Sinkfile(folders[j],create_file=create_file,ds=ds)
        x_data=sinkfile.getattr('time')

        if nsmooth_orig>1:
            nsmooth=nsmooth_orig
            while len(x_data)/nsmooth > 500000:
                nsmooth=nsmooth*2
            if len(x_data) < 5000:
                nsmooth=1

        x_smooth=sinks.smooth(x_data,nsmooth)/get_time_unit(folder)
        xsink=get_parameters(folder)['position']

        
        for i,(field,ax) in enumerate(zip(fields[:nfields],axes)):
            print("Plotting",field)
            if j==0:
                ax.axhline(1.0,linestyle=':',color='k')
            if field=='BHmass':
                masses=sinkfile.getattr('BHmass')
                ax.plot(x_smooth,sinks.smooth(masses,nsmooth)/masses[0],color=colour,linestyle='-')
            if field=='accretion':
                bondi_analytic=sinks.smooth(sinkfile.getattr('BHmass'),nsmooth)**2*4*np.pi*rho/(cs**3*(1+mach**2)**(3./2))
                ax.plot(x_smooth,sinks.smooth(sinkfile.getattr('dotMacc'),nsmooth)/bondi_analytic,color=colour,linestyle='-')
                ratios=sinks.smooth(sinkfile.getattr('dotMacc'),nsmooth)/bondi_analytic
                print("Ratios min,max,median,means,final",ratios.min(),ratios.max(),np.median(ratios), ratios.mean(),ratios[-1])
                print("Mean last 20",(sinks.smooth(sinkfile.getattr('dotMacc'),nsmooth)/bondi_analytic)[-20:].mean())
                ax.set_yscale('log')

                dotM.append(time_average(sinks.smooth(sinkfile.getattr('dotMacc'),nsmooth)/bondi_analytic,x_smooth,mach,parameters['nres']).mean())
                nres.append(parameters['nres'])
                print("Value for dotM_deltaX",dotM[-1])

            if field=='velocities':
                ax.plot(x_smooth,sinks.smooth(sinkfile.getattr('vrel_mag')/(mach*cs),nsmooth),color=colour,linestyle='-')
                ax.plot(x_smooth,sinks.smooth(sinkfile.getattr('mean_cs')/cs,nsmooth),color=colour,linestyle='--')
                ax.set_yscale('log')
            if field=='rho':
                ax.plot(x_smooth,sinks.smooth(sinkfile.getattr('mean_density'),nsmooth)/rho,color=colour,linestyle='-')
            if field=='mach':
                ax.plot(x_smooth,sinks.smooth(sinkfile.getattr('vrel_mag')/sinkfile.getattr('mean_cs')/mach,nsmooth),color=colour,linestyle='-')
                ax.set_yscale('log')
            if field=='R':
                ax.plot(x_smooth,sinks.smooth(sinkfile.getattr('r_bondi'),nsmooth),color=colour,linestyle='-')


            if j==len(folders)-1:
                ax.set_ylabel(field_labels[field])
                y_limits=ax.get_ylim()
                if(y_limits[1]>70):
                    ax.set_yscale('log')
                if(y_limits[0]<0):
                    ax.set_yscale('symlog')
                #ax.set_ylim(max(y_limits[0]*0.8,1E-1),min(y_limits[1]*1.2,1000))
                if i==0:
                    ax.set_ylim(0.01,10)
                if limits[1]:
                    ax.set_xlim(limits[0],limits[1])
            if i==legend_panel and len(folders)>1:
                legend_folders(ax,folders,colours,name=name,loc=legend_loc,columns=legend_columns)

            if field=='velocities':
                if parameters['gamma']<1.5:
                    ax.set_ylim(1E-2,30)
                    ax.set_yscale('log')
                else:
                    ylims=ax.get_ylim()
                    ax.set_ylim(1E-1,ylims[1])
                    if ylims[1]>40:
                        ax.set_yscale('log')
                
                #Add legend to velocities panel
                legend_lines=[mlines.Line2D([],[],label=r'$c_{s,\bullet}/c_{s,\infty}$',linestyle='--',color='k')]
                if not(mach0):
                    legend_lines+=[mlines.Line2D([],[],label=r'$v_\bullet/v_\infty$',linestyle='-',color='k')]
                    
                    #[mlines.Line2D([], [], color='k', label=label,linestyle=linestyle) for label,linestyle in zip([r'$v_\bullet/v_\infty$',r'$c_\bullet/c_\infty$'],['-','--'])]
                if velocities_legend:
                    legend=ax.legend(handles=legend_lines,frameon=False,loc=velocities_legend,ncol=2)
                else:
                    legend=ax.legend(handles=legend_lines,frameon=False,loc='lower right',bbox_to_anchor=[1.0,-0.0],ncol=2)

                ax.add_artist(legend)


        del sinkfile

    axes[-1].set_xlabel(xlabel)
    plt.tight_layout(h_pad=0.00)
    #Fix location of axis labels
    for ax in axes:
        ax.yaxis.set_label_coords(-0.1,0.5)

    #Save figure
    plotname="profiles"
    if len(folders)==1:
        plotname+="_mach{0}".format(mach)
    else:
        plotname+="_n{0}".format(len(folders))
    if addendum:
        plotname+="_{0}".format(addendum)
    if pdf:
        plotname+='.pdf'
    else:
        plotname+='.png'
    print("Saving as",plotname)
    plt.savefig(plotname)
    plt.close()
    return

def legend_folders(ax,folders,colours,loc=1,name=True,anchor=None,columns=1,fontsize=font_size,bbox_transform=None):
    if name:
        labels=[make_name(folder) for folder in folders]
    else:
        labels=folders
    legend_lines=[mlines.Line2D([], [], color=colour, label=label) for label,colour in zip(labels,colours)]
    legend=ax.legend(handles=legend_lines,frameon=False,loc=loc,bbox_to_anchor=anchor,ncol=columns,fontsize=fontsize-2,bbox_transform=bbox_transform)
    ax.add_artist(legend)
    return ax

#########################################
#Derived fields for drag calculation

def slice_drag(outputs=None,limits=None,radius=True,folders=['./'],plot_steps=False,red_blue=[0,0],addendum=None,axis_limits=None,fontsize=font_size):
    if plot_steps:
        fig,(ax1,ax2,ax3,ax4)=plt.subplots(4,1,sharex=True,figsize=(6,10))
        axes=[ax1,ax2,ax2,ax4]
    else:
        fig,(ax1,ax2)=plt.subplots(2,1,sharex=True,figsize=(8,8))
        axes=[ax1,ax2]

    if list(folders)==all_machs:
        red_blue=all_colours
    for j,folder in enumerate(folders):
        print("::::::::::::::::::::::::")
        colours=myutils.get_colours(red_blue[0],red_blue[1])
        run_parameters=get_parameters(folder) 
    #Set scale radius                                                                            
        scale_radius=get_time_unit(folder)
        mach=run_parameters['mach']
        xlabel=r's [$R^S_\infty$]'

        fields=['mass','fdrag','velocity_x']
        
        if not outputs:
            outputs_loc=[get_output_time(time=25,folder=folder,real_time=True)]
        else:
            outputs_loc=outputs
        for k,output in enumerate(outputs_loc):
            sink=sinks.read(output,folder=folder)[0]
            if len(folders)>1:
                k=0
            x,profiles,steps,integrals,box=slice_profile(output=output,fields=fields,folder=folder)
            if radius:
                x=np.array(x)/scale_radius
            else:
                xlabel='s [boxlength]'

            for i,ax in enumerate([ax1,ax2]):
                ax.plot(x,profiles[fields[i]],linestyle='-',label='{0}'.format(output),color=colours[j+k])

            print('--------------------------------')
            print("Total force", integrals['fdrag'])
            print("Total mass of the wake",integrals['mass'])
            print("Total mass of the box",(box['overdensity']*box['dx']**3).sum()/run_parameters['alpha'])
            print("Sink mass",sink.mass.in_units('code_mass')/run_parameters['alpha'])
            print("By cell x",box['fdrag_x'].sum()/run_parameters['alpha']/4/np.pi)
            print("By cell 3d magnitude",np.sqrt((box['fdrag_x'].sum()**2+box['fdrag_y'].sum()**2+box['fdrag_z'].sum()**2))/run_parameters['alpha']/4/np.pi)
            print('--------------------------------'        )

            if plot_steps:
                ax3.plot(x,profiles['velocity_x'],linestyle='-',label='{0}'.format(output))
                ax4.plot(x,steps,marker='x',color='k')

        for i,ax in enumerate(axes):
            ax.axvline(0,linestyle='--',color='k')
            ax.axvline(1,linestyle=':',color='k')
            ax.axvline(-1,linestyle=':',color='k')
            ax.axhline(0,linestyle=':',color='k')
    #Set yscale and limits

    ax1.set_yscale('log')
    if not axis_limits:
        ylims1=ax1.get_ylim()
        ax1.set_ylim(1E-10,ylims1[1])
    else:
        ax1.set_ylim(axis_limits[0],axis_limits[1])

    ax2.set_yscale('symlog',linscaley=1E-1,linthreshy=2E-2,subsy=[2,4,6,8])
    #Change both if changing!
    ylims2=ax2.get_ylim()
    force_limit=int(math.ceil(ylims2[1] / 10.0)) 
    ax2.set_ylim(-1*force_limit,force_limit)
    force_ticks=np.array([10**tick for tick in range(-1,int(np.log10(force_limit))+1)])
    ax2.set_yticks(list(-1*force_ticks)[::-1]+[0]+list(force_ticks))

    ax1.set_ylabel(r'$dm_{wake}/ds$  [$M_{sink}/R^S_\infty$]',fontsize=fontsize)
    ax2.set_ylabel(r'$dF^D_\diamond/ds$  [$4 \pi \rho_\infty (GM_{sink})^2 /(R^S_\infty c_{s,\infty}^2)$]',fontsize=fontsize)

    if plot_steps:
        ax3.set_ylabel('sum(velocity_x)/dx')
        ax4.set_ylabel('cell size')
        
    #Sort out x-axis and legend
    axes[-1].set_xlabel(xlabel,fontsize=fontsize)
    if limits:
        ax1.set_xlim(limits[0],limits[1])
    legend_folders(ax2,folders=folders,colours=colours,loc=4,columns=2)

    #Save plot
    plt.tight_layout(h_pad=0)
    if outputs:
        n_out=len(outputs)
    else:
        n_out=1
    plotname='slice_profiles_o{0}_f{1}'.format(n_out,len(folders))
    if plot_steps:
        plotname+='_{0}'.format('steps')
    if addendum:
        plotname+='_{0}'.format(addendum)
    plotname+='.pdf'
    print("Saving profile as {0}".format(plotname))
    plt.savefig(plotname)
    plt.close()
    return


def analytic_bondi_profile(gamma=4.0/3):

    #Data containers
    data={}
    gamma=float(gamma)

    #Constants
    rho_inf=1.
    G=1.
    Mbh=1.0   #Is that correct?
    c_inf=1.0
    p_inf=1.0/gamma
    
    #Calculated constants
    scaling=0.5**((gamma+1.)/(2*(gamma-1.))) * ((5-3*gamma)/4.)**((3*gamma-5)/(2*(gamma-1)))
    alpha=4*np.pi*scaling*G**2*Mbh**2*rho_inf/c_inf**3
    beta=gamma/(gamma-1)*p_inf/rho_inf

    rho_B= lambda rho,r: 0.5*(alpha/(4*np.pi*rho*r**2))**2+beta*((rho/rho_inf)**(gamma-1)-1)-G*Mbh/r
    results=[1]  #First guess
    radii=np.logspace(-2,3,1000)
    for radius in radii:
        results.append(float(fsolve(rho_B,results[-1],args=(radius))))

    data['radii']=radii
    data['density']=np.array(results[1:])
    data['velocity_magnitude']=alpha/(4*np.pi*radii**2*data['density'])
    data['pressure']=data['density']**gamma    #*p_inf
    data['temperature']=data['pressure']/data['density']     #*p_inf
    return data
    
#This function draws comparative profiles of a variety of hydrodynamical fields
def radial_profiles(time=25,folders=['.'],addendum=None,fields=['density'],rb=[0,0],rmax=500):

    fig,axes=plt.subplots(len(fields),1,sharex=True,figsize=(8.27,2.7*len(fields)))
    if len(fields)==1:
        axes=[axes]
    colours=myutils.get_colours(rb[0],rb[1])

    gamma=4.0/3
    analytic=analytic_bondi_profile(gamma=gamma)

    units={'density':'code_density','pressure':'code_pressure','temperature':'code_temperature','velocity_magnitude':'code_velocity'}
    for i,(folder,colour) in enumerate(zip(folders,colours)):
        output=get_output_time(time=time,folder=folder,real_time=True)
        ds=myutils.load(output,folder=folder)
        run_parameters=get_parameters(folder)
        sink=sinks.read(output,folder=folder)[0]
        scale_radius=get_time_unit(folder)

        #Use ray along x=axis
        ray=ds.ray([0.5,0.5,0.5],[1.0,0.5,0.5])
        add_drag_fields(ray,sink)
        ray_sort=np.argsort(ray['t'])
        radii=ray['r_tosink'][ray_sort].in_units('code_length')
        radii[0]=radii[0]*0
        radii=radii/scale_radius

        #Use profile plot on the sphere
        #sphere=ds.sphere([0.5,0.5,0.5],ds.quan(0.5,'code_length'))
        #add_drag_fields(sphere,sink)
        #profile=yt.create_profile(sphere,['radius'],fields=fields,weight_field='cell_volume',accumulation=False,n_bins=100)
        #sphere_radii=profile.x.in_units('code_length')/scale_radius

        #Plot data
        for field,ax in zip(fields,axes):
            if field=='pressure':
                normalisation=1./gamma
            elif field=='temperature':
                normalisation=1./gamma**2
            else:
                normalisation=1.
            data_ray=ray[field][ray_sort].in_units(units[field])/normalisation
            data_ray=data_ray*(data_ray>0)   #Sanity check mostly useful for x=velocity

            #Plot markers for low res runs
            if run_parameters['ncells']<0.0:
                marker='x'
            else:
                marker=''
            ax.plot(radii,data_ray,color=colour,label=make_name(folder),linestyle='-',marker=marker)
            #ax.plot(sphere_radii,profile[field].in_units(units[field])/normalisation,color=colour,linestyle='--') #Sphere doesn't look good...

    labels={'density':r'$\rho/\rho_\infty$','temperature':r'T/T$_\infty$','pressure':'p/p$_\infty$','velocity_magnitude':r'$v_r / c_{s,\infty}$'}
    #Set up individual plots
    for field,ax in zip(fields,axes):
        ax.plot(analytic['radii'],analytic[field],color='k',linestyle=':',label='analytic')
        try:
            ax.set_ylabel(labels[field])        
        except:
            ax.set_ylabel(field)
        ax.set_yscale('log')
        ax.axvline(1.0,color='grey',alpha=0.5)
        if field=='density':
            ax.set_ylim(7E-1,1E3)
        if field=='temperature':
            ax.set_yscale('linear')
            ax.set_ylim(0,15)
        if field=='pressure':
            ax.set_ylim(7E-1,1E4)
        if field=='velocity_magnitude':
            ax.set_ylim(1E-6,5E1)

    #Set up quantities that affect whole plot
    axes[-1].set_xscale('log')
    axes[-1].set_xlim(1E-2,rmax)
    axes[-1].set_xlabel('R/R$^S_\infty$')
    axes[-2].legend(frameon=False,ncol=2)

    figname='radial_profiles'
    for field in fields:
        figname+='-{0}'.format(field)
    myutils.saveas(fig,figname,addendum=addendum,pdf=True)

#Thermal and gravitational pressure profiles
def bondi_profiles(output,folders=['.'],addendum=None,colours=[0,0],fontsize=font_size):
    '''
    This function creates the pressure profiles that prove that gravitational pressure dominates
    over thermal pressure into the accretion region.'''

    rmax=2
    nplots=1
    fig,ax=plt.subplots(nplots,1,sharex=True,figsize=(8.27,2.7*nplots))
    colours=myutils.get_colours(colours[0]+2,colours[1])

    #Add gravitaitonal potential                                                                                                                                                                                                            
    #grav_radii=np.linspace(0,rmax,1000)
    #g=1/grav_radii**2
    #g=scale_radius/(grav_radii*scale_radius)**2
    #axes[2].plot(grav_radii,g,color='k',linestyle='--')
    #axes[2].axhline(0.0,linestyle=":",color='grey')

    marker=''
    for i,folder in enumerate(folders):
        colour=colours[i+2]

        ds=myutils.load(output,folder=folder)
        sink=sinks.read(output,folder=folder)[0]
        scale_radius=get_time_unit(folder)

        ray=ds.ray([0.5,0.5,0.5],[1.0,0.5,0.5])
        add_drag_fields(ray,sink)

        ray_sort=np.argsort(ray['t'])
        radii=ray['r_tosink'][ray_sort].in_units('code_length')
        radii[0]=radii[0]*0            

        pressure=ray['pressure'][ray_sort]
        density=ray['density'][ray_sort]

        F_press=(pressure[1:]-pressure[:-1])/(radii[1:]-radii[:-1])/density[1:]*scale_radius
        F_press=np.array(F_press.in_units('code_length/code_time**2'))

        #Plot everything
        radii=radii/scale_radius
        g_tosink=1/radii**2
        g_tosink=np.array(g_tosink.in_units('1/code_length**2'))
        
        #axes[0].plot(radii,density.in_units('code_density'),label=folder,color=colour,marker=marker)
        #axes[1].plot(radii,pressure.in_units('code_pressure'),color=colour,marker=marker)      
        #axes[2].plot(radii[1:],F_press.in_units('code_length/code_time**2'),color=colour,marker=marker)
        #axes[2].plot(radii[1:],g_tosink[1:],color=colour,marker='o')
        #ax.plot(radii[1:],(g_tosink[1:]+F_press)/g_tosink[1:],color=colour,marker=marker,label=make_name(folder))
        ax.plot(radii[1:],-1*(F_press)/g_tosink[1:],color=colour,marker=marker,label=make_name(folder)) 
        ax.axvline(radii[pressure==pressure.max()],color=colour,alpha=0.5,linestyle='--')


    ax.axhline(1.0,color='grey',linestyle=':')
    ax.axhline(-1.0,color='grey',linestyle=':')
    ax.axhline(0.0,color='grey',linestyle='-')

    #ax.set_yscale('log')

    #xlims=ax.get_xlim()
    #ax.set_xlim(xlims[0],rmax)
    ax.set_xlim(0,1.0)
    #ax.set_ylim(-0.2,2.2)
    ax.set_ylim(-1.15,1.15)
    ax.legend(loc=0,frameon=False,ncol=1,fontsize=fontsize-4)
    ax.set_xlabel('radius $[R^S_\infty]$')
    #ax.set_ylabel(r"$(\nabla \phi_g -  \frac{1}{\rho} abs( \nabla P))/\nabla \phi_{g} $")
    ax.set_ylabel(r"$ -\frac{1}{\rho} \nabla P/\nabla \phi_{g} $")

    plotname='Bondi_pressure_profiles'
    myutils.saveas(fig,plotname,addendum=addendum,pdf=True)

#Creates a profile from averaging over slices along the wake etc
def slice_profile(output,rho0=1,fields=['fdrag'],folder='./'):
    sink=None

    while not(sink):
        try:
            sink=sinks.read(output,folder=folder)[0]
        except:
            if output<0:
                print("Negative output!",output)
                break
            sink=None            
            output=output-1

    ad=sink.ds.all_data()
    box=add_drag_fields(ad,sink)
    params=get_parameters(folder)
    msink=params['alpha']
    mach=params['mach']
    r_scale=get_scale_radius(folder)

    #Get step sizes based on smallest grid
    ray = sink.ds.ortho_ray(0,(0.5,0.5))
    ray_sort = np.argsort(ray["x"])
    steps=ray['dx'][ray_sort] 

    msink=get_parameters(folder)['alpha']

    sink_pos=sink.ds.index.get_smallest_dx()/2+sink.ds.quan(0.5,'code_length')
    x=ray['x'][ray_sort]
    x_tosinks=x.in_units('code_length')-sink_pos
    x_tosinks[abs(x_tosinks)<sink.ds.index.get_smallest_dx()/2]=0
    
    #retrieve spatial cell information
    dxs=box['dx'].in_units('code_length')
    x_mins=box['x']-dxs/2
    x_maxs=box['x']+dxs/2

    profiles={}; indices=[]; integrals={}
    for x_loc in x:
        #identify all cells that cross the slice
        low=x_mins<=x_loc
        high=x_maxs>x_loc
        indices.append(low*high)       #indices of all cell that cover x_slice

    for i,field in enumerate(fields):
        field_profile=[]
        for j,x_loc in enumerate(x_tosinks):
            if field=='mass':
                slice_value=box['overdensity'][indices[j]]*dxs[indices[j]]**2     #m/dx for each cell
                slice_value=slice_value*r_scale/msink                 #Do I need factor of r_scale or is 1/msink enough?

            elif field=='fdrag':
                #Has to be calculated from the cells, not the mass slices because the radial extent matters. The slices can NOT be treated as point masses
                slice_value=box['fdrag_x'][indices[j]]/dxs[indices[j]]
                slice_value=slice_value*r_scale/msink/4/np.pi
            else:
                slice_value=box[field][indices[j]]*dxs[indices[j]]**2
            field_profile.append(slice_value.sum())
        profiles[field]=np.array(field_profile)
        f_integrates=profiles[field]*steps/r_scale
        f_integrates=f_integrates[np.logical_not(np.isnan(f_integrates))]
        integrals[field]=f_integrates.sum()

    #profiles['fdrag']=[mass*abs(x_tosink)/x_tosink**3 for mass,x_tosink in zip(profiles['mass'],x_tosinks)]
    return np.array(x_tosinks),profiles,np.array(steps),integrals,box

#Add the fields used to calculate the drag force from the wake
def add_drag_fields(myobject,sink,rho0=1,mach=1):
    #Calculate drag force direction                                                                                                                                                                                              
    #for coordinate in ['x','y','z']:        
    myobject.set_field_parameter('mach',mach)                                                                                                                                                                                    
    myobject.ds.add_field('x_tosink',function=x_tosink,units='code_length',force_override=True)
    myobject.ds.add_field('y_tosink',function=y_tosink,units='code_length',force_override=True)
    myobject.ds.add_field('z_tosink',function=z_tosink,units='code_length',force_override=True)

    #Calculate various parameters for drag force 
    myobject.set_field_parameter('rho0',sink.ds.quan(rho0,'code_density'))
    myobject.ds.add_field('overdensity',function=overdensity_field,units='code_density',validators=[ValidateParameter('rho0')],particle_type=False)
    myobject.ds.add_field('overmass',function=overmass_field,units='code_mass',validators=[ValidateParameter('rho0')],particle_type=False)
    myobject.set_field_parameter('sink',sink)
    myobject.ds.add_field('r_tosink',function=r_tosink,units='code_length',validators=[ValidateParameter('sink')],particle_type=False,force_override=True)
    myobject.ds.add_field('g_tosink',function=g_tosink,units='code_mass/code_length**2',validators=[ValidateParameter('sink')],particle_type=False,force_override=True)

    #Calculate the drag force
    myobject.ds.add_field('fdrag_tot',function=fdrag_tot,units='code_mass/code_length**2',force_override=True)
    myobject.ds.add_field('fdrag_x',function=fdrag_x,units='code_mass/code_length**2',force_override=True)
    myobject.ds.add_field('fdrag_y',function=fdrag_y,units='code_mass/code_length**2',force_override=True)
    myobject.ds.add_field('fdrag_z',function=fdrag_z,units='code_mass/code_length**2',force_override=True)
    

    return myobject

###############################

def fdrag_x(field,data):
    Fdrag_cell=data['fdrag_tot']*data['x_tosink']/data['r_tosink']
    dx_min=data.ds.index.get_smallest_dx()
    Fdrag_cell[abs(data['x_tosink'])<dx_min/2]=0
    return Fdrag_cell

def x_tosink(field,data):
    sink=data.get_field_parameter('sink')
    try:
        sinkpos=getattr(sink,'x')
    except:
        sinkpos=data.ds.quan(0.00,'code_length')
    dist=data['x']-sinkpos
    dx_min=data.ds.index.get_smallest_dx()
    dist[abs(dist)<dx_min/2]=0
    return dist

def fdrag_y(field,data):
    Fdrag_cell=data['fdrag_tot']*data['y_tosink']/data['r_tosink']
    dx_min=data.ds.index.get_smallest_dx()
    Fdrag_cell[abs(data['y_tosink'])<dx_min/2]=0
    return Fdrag_cell

def y_tosink(field,data):
    sink=data.get_field_parameter('sink')
    try:
        sinkpos=getattr(sink,'y')
    except:
        sinkpos=data.ds.quan(0.0,'code_length')
    dist=data['y']-sinkpos
    return dist

def fdrag_z(field,data):
    Fdrag_cell=data['fdrag_tot']*data['z_tosink']/data['r_tosink']
    dx_min=data.ds.index.get_smallest_dx()
    Fdrag_cell[abs(data['z_tosink'])<dx_min/2]=0
    return Fdrag_cell

def z_tosink(field,data):
    sink=data.get_field_parameter('sink')
    try:
        sinkpos=getattr(sink,'z')
    except:
        sinkpos=data.ds.quan(0.0,'code_length')
    dist=data['z']-sinkpos
    return dist

def fdrag_tot(field,data):
    '''calculate the magnitude of the local drag force'''
    Fdrag=data['overdensity']*data[('ramses','cell_volume')]/(data['r_tosink']**2)  # in units of [1/M_sink]
    dx_min=data.ds.index.get_smallest_dx()
    Fdrag[abs(data['x_tosink'])<dx_min]=0
    return Fdrag
    
def r_tosink(field,data):
    sink=data.get_field_parameter('sink')
    try:
        sinkpos=sink.pos
    except:
        sinkpos=data.ds.arr(np.zeros(3),'code_length')
    r=(data['x']-sinkpos[0])**2+(data['y']-sinkpos[1])**2+(data['z']-sinkpos[2])**2
    dx_min=data.ds.index.get_smallest_dx()**2
    r[r<dx_min]=dx_min
    return np.sqrt(r)

def g_tosink(field,data):
    sink=data.get_field_parameter('sink')
    try:
        msink=sink.mass
    except:
        msink=data.ds.quan(0.001,'code_mass')
    g=msink/data['r_tosink']**2
    return g


def overdensity_field(field,data):     #Code field that has rho=rho-rho_0
    rho=data['density']-data.get_field_parameter('rho0')
    #rho[rho.in_units('code_density')<1E-1*(data['x_tosink']<data.ds.quan(0,'code_length'))]=0
    rho[rho<0]=0
    return rho

def overmass_field(field,data):     #Code field that has rho=rho-rho_0                                                                                                   
    mass=data['overdensity']*data[('ramses','cell_volume')]
    return mass

#Cutregion including all cells with a certain overdensity

def overdensity_region(box,density_cut):   #density_cut is in % over/under density
    cut=box.cut_region(["abs(obj['overdensity'])>{0}".format(density_cut)])
    return cut

def create_fdrag_file(folder='.',output_min=None):
    outputs=np.array([int(name[-5:]) for name in os.listdir(folder) if name[:6]=='output'])
    outputs.sort()
    print("Found outputs",outputs)
    foldername=folder+"/Data"

    if not os.path.exists(foldername):
        os.makedirs(foldername)

    filename=foldername+"/fdrag.csv"

    if output_min:
        f=open(filename,'a')
        outputs=outputs[outputs>=output_min]
    else:
        f=open(filename,'wb')


    writer=csv.writer(f)
    if not output_min:
        writer.writerow(['output','time','Fx','Fy','Fz'])
    for output in outputs:
        sink=sinks.read(output,folder=folder)[0]
        ad=sink.ds.all_data()
        box=add_drag_fields(ad,sink)
        row=[output,sink.ds.current_time,box['fdrag_x'].sum(),box['fdrag_y'].sum(),box['fdrag_z'].sum()]
        row[1:]=[float(value) for value in row[1:]]
        writer.writerow(row)
    return

class Fdrag(object):
    def __init__(self,folder,create_file=False,real_time=False):
        outputs=np.array([int(name[-5:]) for name in os.listdir(folder) if name[:6]=='output'])
        filename=folder+"/Data/fdrag.csv"
        if not(os.path.exists(filename)) or create_file:
            print("I need to create the fdrag file")
            create_fdrag_file(folder)
        print("Opening",filename)
        with open(filename) as f:
            reader=csv.reader(f)
            next(reader)
            rows=[row for row in reader]

        self.parameters=get_parameters(folder)
        self.ostriker_scaling=4*np.pi*self.parameters['alpha']
        self.radius=get_time_unit(folder,real_time=real_time)

        self.outputs=np.array([int(row[0]) for row in rows])
        self.times=np.array([float(row[1]) for row in rows])/self.radius
        self.forces=np.array([[float(dim) for dim in row[2:]] for row in rows])/self.ostriker_scaling

        self.fdrag_mag=np.array([np.linalg.norm(force) for force in self.forces])
        
        if self.outputs.max()<outputs.max():
            print("Recreate the file for next time")
            create_fdrag_file(folder,output_min=self.outputs.max()+1)


    def at_time(self,target_time):
        forces=self.forces[self.times>target_time]
        if len(forces)==0:
            forces=self.forces[-1]
            print("Returning final available value at time",self.times[-1])
        return forces[0]
