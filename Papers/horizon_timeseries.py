import horizon as h
import marta_horizon as m
import numpy as np
import matplotlib as mpl
import csv
import matplotlib.pyplot as plt
import collections as clct
from astropy.cosmology import WMAP9 as cosmo

mpl.rc('font', family='serif', size=16)
mpl.rcParams['lines.linewidth']=h.linewidth


all_outputs=[43,70,90,125,197,343,519]
few_outputs=[70,125,343,519]
local_outputs=[470,486,503,519,535,552,570]

#Plotting functions

def overplot_ratios(redshifts=h.all_redshifts):
    outputs=few_outputs
    BHs=present_in_all(increasing=True,outputs=outputs)
#    fig,ax=plt.subplots()
    ax=h.ratios_mass(stars=True,saveit=False,quartile=True)
#    print BHs
    j=0
    for i,BH in enumerate(BHs[:10]):    #5 random objects
        masses=timeseries(BH,field='Mstar',AGN=True,outputs=outputs)
        ratio=timeseries(BH,field='Mstar',AGN=True,outputs=outputs)/timeseries(BH,field='Mstar',AGN=False,outputs=outputs)
        ax.plot(masses,ratio,color=pick_colour_rb(i),marker='o',markeredgewidth=0.0,markersize=5)
    plt.show()
    

def BH_progenitors(redshifts=h.all_redshifts,quartile=False,nbins=h.nbins_glob,marta=True,progenitors=True):
    'Overplot only objects with progenitors on BHmass plot'
    ax=h.binned_field_mass(quartile=quartile,xlabel='Stellar masss [Msun]',saveit=False,redshifts=redshifts,nbins=nbins,BHy=True,linestyle=":",field='mass')
    BHs=read_file(local=False)
    for redshift in redshifts:
        output=h.find_output_for_redshift(redshift,AGN=True)
        colour=h.pick_colour(redshift,redshifts)
        BHs_z=extract_output(output,BHs)
        masses=np.array([BH.AGN.Mstar for BH in BHs_z])
        BHmasses=np.array([BH.AGN.Mbh for BH in BHs_z])
        if progenitors:   #overplot the progenitors to 1E11 Msun galaxies
            ax=h.plot_vs_mass(ax,masses=masses,data=BHmasses,linestyle='-',colour=colour,quartile=quartile,nbins=nbins)

        if marta:    #Overplot data from Martas bookshop
            BHs_marta=m.read_file(redshift)
            masses_marta=np.array([BH.Mstar for BH in BHs_marta])
            BHmasses_marta=np.array([BH.Mbh for BH in BHs_marta])
            ax=h.plot_vs_mass(ax,masses=masses_marta,data=BHmasses_marta,linestyle='--',colour=colour,quartile=quartile,nbins=nbins)
            
    if marta:
        h.add_categories_to_legend(ax,categories={"Ricarda":":","Marta":"--"},loc=4)
    if progenitors:
        h.add_categories_to_legend(ax,categories={"All objects":":","Progenitors":"-"},loc=4)

    foldername="./for_Marta/"
    filename=foldername+"BHmass_mass_nbins{0}_{1}".format(nbins,len(redshifts))
    if marta:
        filename+="_marta"
    if progenitors:
        filename+="_progens"
    h.saveas(ax,filename)

def duty_cycle(Nobjects=10,Mlimits=[5E10,2E11],objects=True,quartile=True,time=False):
    ''' plot the net outflows due to AGN around redshift 0.5'''

    fig,ax=plt.subplots()

    
    print "Plot vs time, not redshift"
    local=True
    BHs=read_file(local=local)
    field='R20flow'


    BH_timeseries=np.array([timeseries(BHs[BHid],field=field,AGN=True) for BHid in BHs])-np.array([timeseries(BHs[BHid],field=field,AGN=False) for BHid in BHs])
    redshifts=np.array([h.load(output).z for output in local_outputs])
    times=[cosmo.age(redshift).value for redshift in redshifts]
    if time:
        xvalues=times
        xlabel="Age of the universe [Gyr]"
    else:
        xvalues=redshifts
        xlabel="Redshift"
    Mstar_zMin=np.array([BHs[BHid][max(local_outputs)].AGN.Mstar for BHid in BHs])

    print Mstar_zMin
    small_galaxies=BH_timeseries[Mstar_zMin<Mlimits[0]]
    big_galaxies=BH_timeseries[Mstar_zMin>Mlimits[1]]

    print "There are",len(small_galaxies),"small galaxies and",len(big_galaxies),"big galaxies"

    for I,galaxies in enumerate([small_galaxies,big_galaxies]):
        ax=plt.subplot(1,2,I+1)
        medians=np.array([np.median(galaxies[:,i]) for i in range(len(xvalues))])
        lower_quartiles=np.array([np.nanpercentile(galaxies[:,i],20) for i in range(len(xvalues))])
        upper_quartiles=np.array([np.nanpercentile(galaxies[:,i],80) for i in range(len(xvalues))])

        if quartile:
            ax.fill_between(xvalues,lower_quartiles,upper_quartiles,color=h.colour_1(),alpha=h.alpha_region)

        if objects:
            for j,BH in enumerate(galaxies[Nobjects:2*Nobjects-1]):  #Used first 10 originally?
                ax.plot(xvalues,BH,linestyle=":",color=pick_colour_rb(j),marker='o',markeredgewidth=0.0,markersize=2*h.linewidth)

        if quartile:
            ax.plot(xvalues,medians,color=h.colour_1(),linestyle='-',marker='o',markeredgewidth=0.0,markersize=2*h.linewidth,label="Sample median",alpha=h.alpha_region)

        ax.axvline(xvalues[3],color='gray',linestyle='-.')#,label="z=0.5")
        if I==0:
            title=r"Small galaxies: $M_{*}^{\rm H-AGN} < 5 \times 10^{10}$ M$_\odot$"
        else:
            title=r"Large galaxies:  $ M_{*}^{\rm H-AGN} > 2 \times 10^{11}$ M$_\odot$"
        ax.set_title(title)
        ax.set_yscale('symlog')
#        plt.gca().invert_xaxis()
        ax.set_xlabel(xlabel)
        ax.set_ylabel(r'Residual outflow [$\rm  M_\odot yr^{-1}$]')
        ax.set_ylim(-1E4,1E4)
        ax.legend(loc=3,frameon=False)
    figname="dutycycle_flows_{0}".format(Nobjects)
    if time:
        figname+="_time"
    else:
        figname+="_redshift"
    fig.set_size_inches(14,6)
    plt.tight_layout()
    h.saveas(fig,figname)
    return times

###############################################
# Helper functions


def extract_output(output,BHs=None):
    ''' return list of BH_twins at ooutput'''
    if not output in all_outputs:
        print "output needs to be one of",all_outputs,"or",local_outputs
        return

    if not BHs:
        BHs=read_file()
    BHs=np.array([BHs[id][output] for id in BHs if output in BHs[id].keys()])
    return BHs


def strictly_increasing(L):
    return all(x<y for x, y in zip(L, L[1:]))

def present_in_all(outputs=all_outputs,BHs=None,increasing=False):
    ''' This function assumes all outputs, as the local ones are automatically all present in all'''
    if not BHs:
        BHs=read_file()
    new_BHs=[BHs[id] for id in BHs.keys() if all([output in BHs[id].keys() for output in outputs])]
    if increasing:
        new_BHs=np.array([BH for BH in new_BHs if strictly_increasing(timeseries(BH,'Mstar'))])
    print "There are now",len(new_BHs),"out of originally",len(BHs)
    return new_BHs

def pick_colour_rb(i):
    colours= ['#67001f','#b2182b','#d6604d','#f4a582','#fddbc7','#d1e5f0','#92c5de','#4393c3','#2166ac','#053061']
    while i > len(colours)-1:
        i=i-len(colours)
    return colours[i]

def extract_twins(twins,BHs=None,BH_ids=None):
    ''' Find the set of twins from the BHs or BHids'''
    if BH_ids:
        target_ids=BH_ids
    else:
        if not BHs:
            print "Please provide either BHs or BH_ids"
        target_ids=np.array([BH.IDbh for BH in BHs])
    source_ids=np.array([id for twin in twins for id in twin.AGN.mark.sink.all_ids ])  #load all ids and flattend the list
    BHtwins=np.array([find_host(id,twins) for id in target_ids if id in source_ids])
    return BHtwins

def read_file(local=False,debug=False):
    filename="./matchedBHs/"
    simulations=['AGN','noAGN']
    if local:
        outputs=local_outputs
    elif debug:
        outputs=[43,70]
    else:
        outputs=all_outputs
        
    AGNlines={}
    noAGNlines={}
    for output in outputs:
        for simulation in simulations:
            filename="./matchedBHs/{0}/BHtwins_{1}".format(simulation,str(output).zfill(3))
            if simulation=='noAGN':
                filename="./matchedBHs/{0}/BHtwins_{1}".format(simulation,str(h.pairup_output(output,DM=False)).zfill(3))
            if local:
                filename+="_local"
            filename+=".csv"
            print "Opening",filename

            with open(filename) as f:
                reader=csv.reader(f)
                reader.next()     #skip header
                if simulation=='AGN':
                    AGNlines[output]=[row for row in reader]
                else:
                    noAGNlines[output]=[row for row in reader]

    #Data has the structure data[BHid][output] to call the value for one output
    #Not all objects have all outputs in them

    data=clct.defaultdict(dict)
    for output in outputs:
        print output,": There are",len(AGNlines[output]),"objects"
        for i,line in enumerate(AGNlines[output]):
            id=int(line[2])
            data[id][output]=BH_twin(AGNlines[output][i],noAGNlines[output][i])  #Append new redshift information to id entry
    return dict(data)

def timeseries(BH,field='IDbh',AGN=True,outputs=None):
    ''' returns a list of field value for all objects in BH'''
    if AGN:
        simulation='AGN'
    else:
        simulation='noAGN'
    if outputs:
        return  np.array([getattr(getattr(BH[output],simulation),field) for output in outputs])
    else:
        return np.array([getattr(getattr(BH[output],simulation),field) for output in sorted(BH.keys())])
            
class BH_twin(object):
    def __init__(self,AGNline,noAGNline=None):
        self.IDbh=int(AGNline[2])
        self.AGN=DataPoint(AGNline,AGN=True)
        if noAGNline:
            self.noAGN=DataPoint(noAGNline,AGN=False)

class DataPoint(object):
    def __init__(self,line,AGN):
        self.AGN=AGN
        self.IDdm=int(line[0])
        self.IDgal=int(line[1])
        self.IDbh=int(line[2])
        self.Mdm=float(line[3])
        self.Mstar=float(line[4])
        self.Mbh=float(line[5])
        self.Mbaryon=float(line[6])
        self.R20flow=float(line[7])

#############################################################################
#Write files

def identify_objects(outputs,local=False):
    source_twins=h.Pair(min(outputs)).get_sink_twins(local=local,flows=True,accretion=False)   #load the original list of BHs
    source_ids=[twin.AGN.mark.sink.id for twin in source_twins]
    print "There are",len(source_ids),"objects to start with"
    targets={}
    print '################################'
    for i,output in enumerate(outputs):
        #Load in twins for this output
        if i == 0:
            targets[output]=source_twins
        else:
            targets[output]=h.Pair(output).get_sink_twins(local=local,flows=True,accretion=False)

        tmp_ids=[]

        #For each BH, keep it if it is present in exactly one halo at this output
        for id in source_ids:
            hosts = np.array([twin.AGN.mark.sink.in_halo(id) for twin in targets[output]])  
            if sum(hosts) == 1:
                tmp_ids.append(id)
        source_ids=tmp_ids            #Update the set of BHs to reflect output just checked
        print "There are",len(source_ids),"objects left"
        print '################################'
    return source_ids,targets  #return a list of BH ids present in all "outputs", as well as a list of pairs already loaded
 
def find_host(id,twins):
    hosts=np.array([twin.AGN.mark.sink.in_halo(id) for twin in twins])
    return twins[hosts][0]
       
def load_BHs(output):
    filename="./SINKS/sinks_{0}.txt".format(str(output).zfill(5))
    print "Opening",filename
    BHmasses={}
    with open(filename) as f:
        reader=csv.reader(f)
        next(reader)
        all_BHs=np.array([row[0].split() for row in reader])
        for BH in all_BHs:
            BHmasses[int(BH[0])]=float(BH[1])
    return BHmasses

def write_objects_to_file(local=False):
    if local:
        outputs=[470,486,503,519,535,552,570]
        identifying_outputs=outputs
    else:
        outputs=[43,70,90,125,197,343,519]
        identifying_outputs=[43,519]
    BHids,targets=identify_objects(outputs=identifying_outputs,local=local)  #identify BHs present in all listed outputs
    simulations=["AGN","noAGN"]

    #Load all remaining outputs
    for output in outputs:
        if not output in targets.keys():
            targets[output]=h.Pair(output).get_sink_twins(local=local,flows=True,accretion=False)

    print ";;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;"

    for i,output in enumerate(outputs):         #For each output in the list
        foldername="./matchedBHs/"
        host_twins=extract_twins(twins=targets[output],BH_ids=BHids)
        print "There are",len(host_twins),"black holes at output ",output,"out of",len(BHids)," in total "
        for simulation in simulations:
            if simulation=="noAGN":
                output=h.pairup_output(output,DM=False)
                BHmasses=clct.defaultdict(int)
            else:
                #retrieve a dict containing all BHs in this output. Can't use twin.AGN.mark.sink.mass as BH not always the first one in halo
                BHmasses=load_BHs(output) 

            filename=foldername+"{0}/".format(simulation)+"BHtwins_{0}".format(str(output).zfill(3))
            if local:
                filename+="_local.csv"
            else:
                filename+=".csv"

            with open(filename,'wb') as f:
                writer=csv.writer(f)
                writer.writerow(['DMhalo id','galaxy id','BH id','M_dm',"Mstar","Mbh","Mbaryon","R20 Baryon outflow [MSun yr^{-1}]"])
                for id,host_twin in zip(BHids,host_twins):     #Use BH ids and BHmasses as BH might not be first in halo
                    twin=getattr(host_twin,simulation)
                    writer.writerow([twin.halo.host.id,twin.halo.id,id,twin.halo.host.mvir,twin.halo.mvir,BHmasses[id],twin.mark.Mbaryon,twin.mark.R20.baryon.outflow])
                
