import numpy as np
import matplotlib.lines as mlines
import matplotlib.pyplot as plt
import matplotlib as mpl
import collections as clct
import csv
from matplotlib.ticker import MultipleLocator, FormatStrFormatter
import os
import sys
from scipy import stats
from matplotlib.transforms import offset_copy
from random import randint
from matplotlib.colors import LogNorm
import weakref
#import timeseries_horizon as t
#import marta_horizon as m

#reload(m)
#reload(t)

alpha_region=0.6
linewidth=4

mpl.rc('font', family='serif', size=14)
mpl.rcParams['lines.linewidth']=linewidth

pc=3.0857E18
kpc=3.0857E21
Mpc=3.0#3.0857E224
Msun=1.988E33
year=3.154E7
Gyr=3.154E16

npart=10
nbins_glob=20
MpcBox=142. #1.#78409   # in Mpc

omega_b     =  0.454999990761280E-01
omega_m     =  0.272000014781952E+00

label_ratio="M$_{H-AGN}$ / M$_{H-noAGN}$"
all_redshifts=[0.01,1,3,5]   #0.52

#######################################################################
#Specific plotting functions

def fit_transition_mass(z_in,data,ax,exponential=False):
    # z_in : log10(1+z)
    # data : log10(Mstar)



    z_in=np.array(z_in)
    data=np.array(data)


    #beta*(1+z)^-alpha
    from scipy import optimize
    fitfunc=lambda p, x:p[0] + (x)*p[1]
    errfunc = lambda p, x, y: (y - fitfunc(p, x)) 
    pinit = [10.0, -0.2]
    power,tmp = optimize.leastsq(func=errfunc, x0=pinit,args=(z_in, data))
    z_plot=np.linspace(0.001,1,100)   
    label=r"M_*^{{quench}}=10^{{{0}}}(1+z)^{{{1}}}".format(round(power[0],2),round(power[1],2))
    ax.plot(z_plot,fitfunc(power,z_plot),label=r"${0}$".format(label),color=colour_1())

    #beta*exp(-alpha(1+z))
    z_lin=10**(z_in)-1
    lnData=np.log(10**data)
    fitfunc=lambda p,x: p[0] + p[1]*x
    errfunc = lambda p, x, y: (y - fitfunc(p, x))
    exp,tmp = optimize.leastsq(func=errfunc, x0=pinit,args=(z_lin, lnData))
    M=np.log10(np.exp(exp[0]))
    label=r"M_*=10^{{{0}}}e^{{({1}z)}}".format(round(M,2),round(exp[1],2))
    if exponential:
        ax.plot(z_plot,np.log10(np.exp(fitfunc(exp,10**(z_plot)-1))),label=r"${0}$".format(label),color='r')


    z_plot=np.linspace(0.001,1,100)

    label_exp=np.log10(np.exp(exp[0]))
    label=r"$M_*=10^{{{0}}}\times \exp({1}(1+z))$".format(round(label_exp,2),round(exp[1],3))
    
    #ax.plot(z_plot,np.log10(np.exp(fitfunc(exp,10**(z_plot)))),label=label,color='g')
    return ax

def transition_mass(redshifts=all_redshifts,perc=0.85,DM=False,exponential=False):
    plt.clf()
    markersize=10
    mpl.rcParams['lines.linewidth']=linewidth-2
    fig,ax=plt.subplots()
    plot_z=(np.linspace(0,0.8,100))
    ones_z=np.ones(len(plot_z))
    peng_z=np.linspace(0,0.51)
    peng=10.6-(peng_z)*0.6/0.51

    #Simulations
    pontzen_z=np.linspace(1.85,6,100)
    pontzen_z=np.log10(1+pontzen_z)

    if DM:
        ax.plot(np.log10(5+1),12.0,label='DiMatteo2011 (MassiveBlackII)',color='grey',markersize=markersize,marker='o',linestyle='',markeredgewidth=0)
        ax.plot(pontzen_z,np.ones(len(pontzen_z))*11.8,linestyle=':',label='Pontzen2016',color='grey')
    else:
        ax.plot(pontzen_z,np.ones(len(pontzen_z))*10.4,linestyle=':',label='Pontzen2016',color='grey')
        ax.plot(np.log10(1+0.01),11,marker='p',label='Vogelsberger2014 (Illustris)',color='grey',markersize=markersize+2,markeredgewidth=0,linestyle='')
        ax.plot(np.log10(1+0.01),10.6,marker='s',label='Keller2016',color='grey',markersize=markersize,markeredgewidth=0,linestyle='')
        ax.plot(np.log10(1+1),10.47,marker='D',label='Bower2017 (EAGLE)',color='grey',markeredgewidth=0,markersize=markersize,linestyle='')
        ax.plot(np.log10(5+1),10.0,label='DiMatteo2011 (MassiveBlackII)',color='grey',markersize=markersize,marker='o',linestyle='',markeredgewidth=0)

        #Observations
        ax.plot(peng_z,peng,linestyle='--',label='Peng2010 (SDSS+zCOSMOS)',color='k')
        ax.plot(np.log10(1+0.01),10.3,label='Baldry2004 (SDSS)',color='k',markersize=markersize,linestyle='',marker='<',markeredgewidth=0)

    data_z=[]
    data_M=[]

    for redshift in redshifts:
        _separator_2()
        
        #Pick colour                                                                                                                                                                 
        colour=pick_colour(redshift,redshifts)
        
        #Load data
        output=find_output_for_redshift(redshift,AGN=True)
        pair=Pair(output,DM=False)

        ydata=pair.get_ratios(name='AGN.halo.mvir')
        if DM:
            xdata=pair.get_data(name='AGN.halo.host.mvir')
        else:
            xdata=pair.get_data(name='AGN.halo.mvir')

        #Make bins
        bins=make_bins(xdata,nbins=30,min_bin=min_limit(DM=DM)*2)
        plot_bins=np.array([(bins[i]+bins[i-1])/2 for i in range(1,len(bins))])
        indices=np.digitize(xdata,bins)

        medians=[]
        for i in range(1,len(bins)):
            bin_data=ydata[indices==i]
            bin_data= bin_data[np.logical_not(np.isnan(bin_data))]
            bin_data=bin_data[bin_data<1E308]
            bin_data=bin_data[0<bin_data]
            medians.append(np.median(bin_data))

        medians=np.array(medians)
        lower=np.log10(plot_bins[medians<(perc+0.05)][0])
        upper=np.log10(plot_bins[medians<(perc-0.05)][0])
        value=np.log10(plot_bins[medians<perc][0])

        print("VALUES",pair.AGN.ds.z,value,upper-value,value-lower)

        label=None

        data_z.append(np.log10(pair.AGN.ds.z+1))
        data_M.append(value)
        ax.errorbar(data_z[-1],value,yerr=[[value-lower],[upper-value]],marker='o',color=colour,markersize=10,linestyle='',markeredgewidth=0,elinewidth=2)
        
    ax.errorbar(100,10,0.01,label='This work',color='k',marker='o',linestyle='',markersize=8,elinewidth=2,markeredgewidth=0)
    ax=fit_transition_mass(data_z,data_M,ax,exponential=exponential)

    plt.legend(frameon=False,fontsize=12,loc=3,numpoints=1)      # ,loc='upper right',bbox_to_anchor=(0.99,0.91))
    ax.set_xlabel(r"log$_{10}(1+z)$")
    if DM:
        ax.set_ylabel(r"log$_{10}$( $ M^{\rm quench}_{\rm vir}$)   [M$_\odot$]")
    else:
        ax.set_ylabel(r"log$_{10}$($ M^{{\rm quench}}_{*}$)   [M$_\odot$]")
    ax.set_xlim(0,max(plot_z))
    #ax.set_ylim(8.5,11.1)
    figname='transition_mass'
    if DM:
        figname+='_DM'
    if exponential:
        figname+='_exp'
    figname+='.pdf'
    plt.savefig(figname)
    mpl.rcParams['lines.linewidth']=linewidth
    plt.close()
    return

def baryon_conversion_efficiency(redshifts=[0.01,1,3,5],simulation='AGN',addendum=None):
    fig,ax=plt.subplots()

    _separator_2()
    for redshift in redshifts:
        colour=pick_colour(redshift,redshifts)
        pair=Pair(find_output_for_redshift(redshift))
        twins=pair.get_marks_twins(noAGN=True,galaxy=False)
        x_masses=np.array([getattr(twin,simulation).halo.host.mvir + getattr(twin,simulation).halo.mvir for twin in twins])
        y_masses=np.array([getattr(twin,simulation).halo.mvir for twin in twins])/(x_masses*omega_b/omega_m)
        ax=plot_vs_mass(ax,masses=x_masses,data=y_masses,quartile='True',DM=True,limits=[1E11,1E40],colour=colour)
        _separator_2()

    foldername='./Datasets/BaryonEfficiency_Kravtsov/'
    files=os.listdir(foldername)
    galaxy_type='Total'
    for file in files:
        if file[:len(galaxy_type)]==galaxy_type and file[-4:]=='.csv':
            with open(foldername+file) as f:
                reader=csv.reader(f)
                next(reader)
                next(reader)
                next(reader)
                data=list(zip(*reader))
            ax.plot(np.array(data[0]),data[1],linestyle='-',label=file)

    ax.set_xscale('log')
    ax.set_yscale('log')
    ax.set_xlabel('{0} M200'.format(simulation))
    ax.set_ylabel('f_star: M*/(M200 * Omega_b/Omega_m)')
    ax.set_xlim(1E11,1E15)

    add_redshifts_to_legend(ax,redshifts)

    ax.legend(loc=4,frameon=False)
    filename='f_star_mass_{0}_{1}'.format(simulation,len(redshifts))
    saveas(ax,filename,addendum=addendum)
    
######################################################################## 
#Composite plotting functions

def four_GMF(nbins=20,sugata_comparison=False):
    if sugata_comparison:
        redshifts=[5,2.7,1,0.52][::-1]
    else:
        redshifts=all_redshifts
    mpl.rcParams['lines.linewidth']=1
    mpl.rcParams['lines.markeredgewidth']=1
    mpl.rc('font', family='serif', size=10)
    fig,((ax1,ax2),(ax3,ax4))=plt.subplots(2,2,sharex=True,sharey=True)
    fig.subplots_adjust(wspace=0,hspace=0)   
    redshift_labels=redshift_legend_entries(redshifts).labels
    for ax,redshift,label in zip([ax4,ax3,ax2,ax1],redshifts,redshift_labels):
        ax=GMF(find_output_for_redshift(redshift),ax=ax,saveit=False,nbins=nbins,sugata_comparison=sugata_comparison)
        ax.annotate(label,xy=(0.55,0.8),xycoords='axes fraction',size='large')   
        ax.set_xlim(9,14.9)
        ax.set_ylim(-6.9,0)
    #add_redshifts_to_legend(ax,all_redshifts[::-1],loc=4)

    xlabel='log$_{10}$($M$)[M$_\odot$]'
    ylabel='log$_{10}$($\Phi$)[Mpc$^{-3} $dex$^{-1}$]'

    handles=[];labels=[]
    #Add object types to legend
    objects={'halos':GMF_linestyle(DM=True)}
    for item in objects:
        line=mlines.Line2D([],[],color='k',linestyle=objects[item],label=item)
        handles.append(line)
        labels.append(item)
    #add simulations to legend
    simulations={'H-AGN galaxies':'k','H-noAGN galaxies':'gray'}
    for item in simulations:
        line=mlines.Line2D([],[],color=simulations[item],linestyle='-',label=item)
        handles.append(line)
        labels.append(item)
    new_legend=plt.legend(handles,labels,loc='upper center', bbox_to_anchor=(0.51, 0.95),shadow=False, ncol=4,bbox_transform=plt.gcf().transFigure,fancybox=True)
    #add_simulations_to_legend(ax4,categories={'H-AGN':'k','H-noAGN':'gray'},linestyle='-')
    fig.text(0.5, 0.03, xlabel, ha='center')
    fig.text(0.065, 0.5, ylabel, va='center', rotation='vertical')
    ax4.add_artist(new_legend)
    #plt.tight_layout()
    fig.subplots_adjust(top=0.85)
    figname='GMF_four_{0}'.format(nbins)
    if sugata_comparison:
        figname+='_sugata'
    saveas(fig,figname)

    mpl.rc('font', family='serif', size=16)
    mpl.rcParams['lines.linewidth']=linewidth
    mpl.rcParams['lines.markeredgewidth']=linewidth
    return

def four_scatter(y,xlabel='Stellar mass M* [Msun]',ylabel=None,x='AGN.halo.mvir',ratios=False,z=None):
    fig,((ax1,ax2),(ax3,ax4))=plt.subplots(2,2,sharex=True,sharey=True)
    fig.subplots_adjust(wspace=0,hspace=0)   
    for ax,redshift in zip([ax4,ax3,ax2,ax1],all_redshifts):
        output=find_output_for_redshift(redshift)
        colour=pick_colour(redshift,all_redshifts)
        im,ax=binned_field_mass(y=y,x=x,scatter=True,saveit=False,redshifts=[redshift],set_colour=colour,ax=ax,ratios=ratios,z=None)
        ax.annotate("z = {0}".format(redshift_for_plot(output=output,sf=1)),xy=(0.6,0.1),xycoords='axes fraction',size='large')

    cbar_ax = fig.add_axes([0.9, 0.1, 0.05, 0.8])
    cb=fig.colorbar(im, cax=cbar_ax)
    cb.set_ticks(np.linspace(im.get_array().min(), im.get_array().max(), 6))
    cb.set_ticklabels(np.linspace(0, 1., 6))
    #add_redshifts_to_legend(ax,all_redshifts[::-1],loc=4)
    fig.text(0.5, 0.01, xlabel, ha='center')
    fig.text(0.01, 0.5, ylabel, va='center', rotation='vertical')
    y_split=y.split('.')
    figname='scatter_4_{0}_{1}_4'.format(y.split('.')[-1],x.split('.')[-1])
    saveas(fig,figname)

def stellar_metallicity_plots(redshift):
    fig,(row1,row2)=plt.subplots(3,2,sharex=True,sharey=False)
    #fig.subplots_adjust(wspace=0,hspace=0) 
    for ax,row in zip(['mass','SFR'],[row1,row2]):
        colour=pick_colour(redshift,all_redshifts)
        im,ax=binned_field_mass(y='mark.Zstar.Zave',scatter=True,x='halo.mvir',saveit=False,redshifts=[redshift],set_colour=colour,ax=ax)
        ax.annotate("z = {0}".format(redshift),xy=(0.6,0.1),xycoords='axes fraction',size='large')

    cbar_ax = fig.add_axes([0.9, 0.1, 0.05, 0.8])
    cb=fig.colorbar(im, cax=cbar_ax)
    cb.set_ticks(np.linspace(im.get_array().min(), im.get_array().max(), 6))
    cb.set_ticklabels(np.linspace(0, 1., 6))
    #add_redshifts_to_legend(ax,all_redshifts[::-1],loc=4)
    fig.text(0.5, 0.01, xlabel, ha='center')
    fig.text(0.01, 0.5, ylabel, va='center', rotation='vertical')
    figname='scatter_4_{0}_mass_4'.format(field)
    saveas(fig,figname)

######################################################################## 
#Plotting functions

def numbers_redshift():
    fig,ax=plt.subplots()

    outputs=get_outputs(DM=True)[0][:]

    AGNhalos=np.array([len(get_output_halos(output,AGN=True,DM=False)) for output in outputs])
    DMhalos=np.array([len(get_output_halos(output,AGN=True,DM=True)) for output in outputs])

    AGNpairs=[]
    DMpairs=[]
    for output in outputs:
        pair=Pair(output,DM=False)
        AGNpairs.append(len([twin for twin in pair.twins if twin.is_halo]))

        pair=Pair(output,DM=True)
        DMpairs.append(len([twin for twin in pair.twins if twin.is_halo]))

    redshifts=[load(output).z for output in outputs]
    
    ax.plot(redshifts,np.array(AGNpairs)*1.0/AGNhalos,color=blue(1),label='galaxies')
    ax.plot(redshifts,np.array(DMpairs)*1.0/DMhalos,color=mono(1),label='DM')

    plt.xlabel('redshift')
    plt.ylabel('N$_{matched}$/N$_{halos}$')
    
    plt.legend(loc=0)
    history(ax)
    saveas(ax,"matched_redshift")


def binned_field_mass(redshifts=all_redshifts,nbins=nbins_glob,quartile=True,apply_cut=True,addendum=None,xlabel=None,saveit=True,y='AGN.halo.mvir',x='AGN.halo.mvir',linestyle='-',BHx=False,scatter=False,ylabel=None,ax=None,set_colour=None,limits=[0,1E40],title=None,ratios=False,z=None,r10=False,preloaded_pair=None):
    ''' This function can be used to plot any of marks mass quantities. BH mass via BH=True, and any of the following: [Mstar,Mdm,Mbh,Mgas,Mbaryon]
    BH=True plots the mass of the central BH, while Mbh plots the total black hole mass within the halo '''

    if not ax:
        fig,ax=plt.subplots()
    if scatter:
        quartile=False

    for redshift in redshifts:
        _separator_2()

        #Pick colour
        if not set_colour:
            colour=pick_colour(redshift,redshifts)
        else:
            colour=set_colour

        #Load data
        output=find_output_for_redshift(redshift,AGN=True)
        if preloaded_pair:
            pair=preloaded_pair
        else:
            pair=Pair(output,DM=False,r10=r10)
        if apply_cut: 
            pair.apply_selection_cut()

        #y data
        if not ylabel:
            ylabel=y
            if ratios:
                ylabel='ratios:'+ylabel
        if ratios:
            ydata=pair.get_ratios(name=y)
        else:
            ydata=pair.get_data(name=y)
            
        if z:
            zdata=pair.get_data(name=z)
        else:
            zdata=None
        split_y=y.split('.')

        #x data
        if not xlabel:
            xlabel=x
        xdata=pair.get_data(name=x,other_name=y)

        while not(len(ydata)==len(xdata)):
            #xdata criteria is more restrictive than y data one, call y data again
            print("Samples uneven, reload")
            print(len(ydata),len(xdata))
            if ratios:
                ydata=pair.get_ratios(name=y,other_name=x)
            else:
                ydata=pair.get_data(name=y,other_name=x)

            xdata=pair.get_data(name=x)
            
        split_x=x.split('.')

        #Sanitise data, removing 0 and NaN                                                                                                                        
        xdata=xdata[ydata>0]
        ydata=ydata[ydata>0]

        ydata=ydata[xdata>0]
        xdata=xdata[xdata>0]

        if ratios:
            xdata=xdata[ydata>0.01]
            ydata=ydata[ydata>0.01]
            if z:
                zdata=zdata[ydata>0.01]

        if scatter:
            if 'mass' in split_x or 'mvir' in split_x or 'mstar' in split_x:
                extent=[8,12]
                if 'sink' in split_x:
                    extent=[6,10]
            else:
                print("Setting automatic extent for x: ",x)
                extent=[np.log10(xdata[xdata>0].min()),np.log10(xdata.max())]

            if split_y[-1]=='eddington_ratio':
                extent+=[-8, 0.1]
            elif 'sink' in split_y and 'mass' in split_y:
                extent+=[6,10]
            elif 'sfr' in split_y or 'sfr1' in split_y or 'sfr2' in split_y:
                extent=[8,13,-4,2]
            elif 'AGN_power' in split_y:
                extent+=[-10,-5]
            elif 'Zave' in split_y:
                extent+=[-4,-1]
            elif 'ZcellMAX' in split_y:
                extent+=[0,3]
            elif ratios:
                extent+=[0,2]
            else:
                print("Setting automatic extent for y: ",y)
                extent+=[np.log10(ydata[ydata>0].min()),np.log10(ydata.max())]


            if ratios:
                #Linear y axis
                im=ax.hexbin(xdata, ydata, C=zdata, bins='log',gridsize=20,xscale='log',yscale='lin',extent=extent,cmap=plt.cm.Greys)#,norm=mpl.colors.LogNorm())
                #im=ax.hexbin(xdata, ydata, bins='log',gridsize=20,xscale='log',yscale='lin',extent=extent,cmap=plt.cm.Greys)
            else:
                #Log on y axis
                im=ax.hexbin(xdata, ydata, bins='log',gridsize=20,xscale='log',yscale='log',extent=extent,cmap=plt.cm.Greys)
            print("The median",y,"for the entire sample is",np.median(ydata))
            
        ax=plot_vs_mass(ax,masses=xdata,data=ydata,x_split=split_x,colour=colour,quartile=quartile,nbins=nbins,linestyle=linestyle,BH=('sink' in split_x),limits=limits)

    #if field in ['GALf_gas'] and 1.0 in redshifts:
    #    mass_schreiber=[9.7,10.2,10.7,11.2]
    #    mass_schreiber=[10**mass for mass in mass_schreiber]
    #    f_gas=[0.58,0.48,0.27,0.24]
    #    ax.plot(mass_schreiber,f_gas,linestyle=" ",marker='o',color='red')

    if 'sink' in split_y and 'mass' in split_y:
        ax.set_ylim(1E6,9E9)
        print("WARNING: I am setting a limit on the y-axis")
    elif 'eddington_ratio' in split_y:
        ax=add_hline(ax,value=0.01)
        ax.set_ylim(1.1E-8,1.1)
    if 'sink' in split_x and 'mass' in split_x:
        ax.set_xlim(1E6,9E9)
    elif 'mass' in split_x or 'mvir' in split_x:
        if not 'host' in split_x:
            annotate_mass_types(ax,redshifts)    

    if ratios:
        add_hline(ax)
        ylims=ax.get_ylim()
        ax.set_ylim(0,max(1.2,ylims[1]))

    if not xlabel:
        xlabel="{1}: H-{0} mass [M$_\odot$]".format(x_split[0],figname_type(DM=False))
 
    if not(scatter):
        ax.set_xscale('log')
        if not(ratios):
            ax.set_yscale('log')
    else:
        if saveit:
            cb=fig.colorbar(im,ax=ax)

    if not saveit:
        if scatter:
            return im,ax
        else:
            return ax
    else:
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        if title:
            plt.title(title)
        #if ysimulation=='noAGN':
        #    plt.ylabel("{2}:{0} {1}".format(ylabel,field,ysimulation))
        #else:
        #    plt.ylabel("{0} {1}".format(ylabel,field))
        loc=4
        add_redshifts_to_legend(ax,redshifts,loc=loc)
        if ratios:
            figname="ratios_"
        elif scatter:
            figname="scatter_"
        else:
            figname="binned_"
        figname+="{0}_{1}".format(x,y)
        if quartile:
            figname+="_quartile"
        figname+="_{0}_nbins{1}".format(len(redshifts),nbins)
        saveas(ax,figname,addendum)


def log_mean(data):
    #return np.mean(data)
    return np.log10(np.mean(data))


def flow_flow(redshift=6,R95=True,outflow=True,colour_coding=True):
    fig,ax=plt.subplots()
    output=find_output_for_redshift(redshift,AGN=True)
    pair=Pair(output,DM=False)
    twins=pair.get_marks_twins(flows=True,noAGN=True)
    
    if R95:
        radius="R95"
    else:
        radius="R20"

    if outflow:
        flowtype="outflow"
    else:
        flowtype="inflow"


    AGN_flows=np.array([getattr(getattr(twin.AGN.mark,radius).all_species,flowtype) for twin in twins])
    noAGN_flows=np.array([getattr(getattr(twin.noAGN.mark,radius).all_species,flowtype) for twin in twins])
    

    masses=np.array([twin.AGN.halo.mvir for twin in twins])
    masses=np.log10(masses)
    cm = plt.cm.get_cmap('RdYlBu')
    sc=ax.scatter(AGN_flows,noAGN_flows,c=masses,s=10,alpha=alpha_region,vmin=masses.min()-1,vmax=masses.max()+1,cmap=cm)
    
    line=np.logspace(0,np.log10(AGN_flows.max()),100)
    ax.plot(line,line,color=colour_1())

    #ax.scatter(AGN_flows,noAGN_flows)
    plt.colorbar(sc)
    plt.xlabel('AGN {0}'.format(flowtype))
    plt.ylabel('noAGN {0}'.format(flowtype))

    #ax.set_xlim(0,AGN_flows.max()+100)
    #ax.set_ylim(0,noAGN_flows.max()+100)

    plt.xscale('log')
    plt.yscale('log')

    figname="flow_flow_{0}".format(output_for_pair(pair))
    saveas(ax,figname)

def scatter_x_y(output,apply_cut=False,show_cut=False,x_AGN=True,y_AGN=True,addendum=None,saveit=True,x='Star',y='DM',x_halo=False,y_halo=False,ax=None,limits=[0,1E40],colour=None,show_selection=False,ylabel=None,xlabel=None):
    '''Plot scatter plots. Options for axes are Mstar, Mdm, Mbh, Mbaryon, Mgas npart'''

    print("REWRITE MODERN WAY!!!")

    _separator_2()
    if not ax:
        fig,ax=plt.subplots()

    if not colour:
        colour="DarkBlue"

    if x==y:
        y_AGN=False
        print("Plotting noAGN on y-axis")

    #load data
    pair=Pair(output)
    x_sink=False
    y_sink=False
    if x in ['Mbaryon','Mgas','npart','sfr','sfe']:
        x_halo=True
    if y in ['Mbaryon','Mgas','npart','sfr','sfe']:
        y_halo=True
    if x in ['accretion','bondi','eddington','age','eddington_ratio']:
        x_sink=True
    if y in ['accretion','bondi','eddington','age','eddington_ratio']:
        y_sink=True

    if apply_cut:
        pair.apply_selection_cut(AGN)
    if 'Mdm' in [x,y]:
        pair.add_hosts(AGN=True)
        pair.add_hosts(AGN=False)

    #Load the twins for x values
    if 'Mbh' in [x,y]:
        twins=pair.get_sink_twins()
    elif x_halo or y_halo:
        twins=pair.get_marks_twins(noAGN=y_halo)
    elif x_sink or y_sink:
        twins=pair.get_sink_twins(accretion=True)
    else:
        twins=pair.twins

    if x_AGN:
        sim_twins=np.array([twin.AGN for twin in twins])
        x_sim='AGN'
    else:
        sim_twins=np.array([twin.noAGN for twin in twins])
        x_sim='noAGN'

    if x_halo:
        x_masses=np.array([getattr(twin.mark,x) for twin in sim_twins])
    elif x_sink:
        x_masses=np.array([getattr(twin.mark.sink,x) for twin in sim_twins])
    elif x=='Mbh':
        x_masses=np.array([twin.mark.sink.mass for twin in sim_twins])
    elif x=='Mdm':
        x_masses=np.array([twin.halo.host.mvir for twin in sim_twins])
    else:
        x_masses=np.array([twin.halo.mvir for twin in sim_twins])

    if y_AGN:
        sim_twins=np.array([twin.AGN for twin in twins])
        y_sim='AGN'
    else:
        sim_twins=np.array([twin.noAGN for twin in twins])
        y_sim='noAGN'

    if y_halo:
        y_masses=np.array([getattr(twin.mark,y) for twin in sim_twins])
    elif y_sink:
        y_masses=np.array([getattr(twin.mark.sink,y) for twin in sim_twins])
    elif y=='Mbh':
        y_masses=np.array([twin.mark.sink.mass for twin in sim_twins])
    elif y=='Mdm':
        y_masses=np.array([twin.halo.host.mvir for twin in sim_twins])
    else:
        y_masses=np.array([twin.halo.mvir for twin in sim_twins])

    #plot the actual values
    ax.scatter(x_masses,y_masses,marker='x',color=colour)

    if x==y:
    #add exclusion lines
        line=np.logspace(0,np.log10(x_masses.max())+10)
        ax.plot(line,line,color=colour_1())
        ax.plot(line,line*0.1,color=colour,linestyle='--')
        ax.plot(line,line*10.0,color=colour,linestyle='--')
    
    if xlabel:
        plt.xlabel(xlabel)
    else:
        if not x=='npart':
            if x_halo:
                plt.xlabel("{1}: Halo {0} [Msun]".format(x,x_sim))
            else:
                plt.xlabel("{1}: {0} [Msun]".format(x,x_sim))
        else:
            plt.xlabel('Number of particles in {0} halo'.format(x_sim))
            
    if ylabel:
        plt.ylabel(ylabel)
    else:
        if not y=='npart':
            if y_halo:
                plt.ylabel("{1}: Halo {0} [Msun]".format(y,y_sim))
            else:
                plt.ylabel("{1}: {0} [Msun]".format(y,y_sim))
        else:
            plt.ylabel('Number of particles in {0} halo'.format(y_sim)) 

    if limits:
        plt.xlim([x_masses[x_masses>x_masses.min()].min(),x_masses.max()])
        plt.ylim([y_masses[y_masses>y_masses.min()].min(),y_masses.max()])

    if show_cut:
        pair=Pair(output)
        pair.apply_selection_cut(AGN)
        if AGN:
            ax.axvline(pair.selection_cut,color='r')
        else:
            ax.axhline(pair.selection_cut,color='r')

    plt.xscale('log')
    plt.yscale('log')

    #plt.title('Masses at z={0}'.format(redshift_for_plot(pair)))
    ax.legend(loc=0,frameon=False)

    if saveit:
        figname="scatter_{1}_{2}_{0}".format(output_for_pair(pair),x,y)
        
        if show_selection:
            figname+="_select"
        if show_cut:
            figname+='_Dcut'
        if apply_cut:
            figname+='_Acut'
        saveas(ax,figname,addendum,png=True)
    else:
        return ax

# This routine plots the ratio of objects in H-AGN to the objects in H-noAGN, for small masses
def object_numbers_mass(redshifts=[6,4,2,1,0],DM=False,nbins=nbins_glob,addendum=None):
    fig,ax1=plt.subplots()

    types=['H-noAGN','H-AGN']
   
    names=['halos','subhalos']
    categories={names[0]:'-',names[1]:'--'}

    for redshift in redshifts:
        out_AGN=find_output_for_redshift(redshift,AGN=True)
        out_noAGN=pairup_output(out_AGN,DM=DM)

        numbers=[]
        
        for subhalos in [False,True]:
    #AGN data
            halos=get_output_halos(out_AGN,DM=DM,AGN=True,subhalos=subhalos)
            halo_masses=np.array([halo.mvir for halo in halos])

            bins=np.logspace(np.log10(halo_masses.min()),13,nbins)
            halo_indices=np.digitize(halo_masses,bins)
            AGN_numbers=np.array([sum(halo_indices==bin) for bin in range(len(bins))])

    #noAGN data
            halos=get_output_halos(out_noAGN,DM=DM,AGN=False,subhalos=subhalos)
            halo_masses=np.array([halo.mvir for halo in halos])
            halo_indices=np.digitize(halo_masses,bins)
            noAGN_numbers=np.array([sum(halo_indices==bin) for bin in range(len(bins))])
            
            ax1.plot(bins,AGN_numbers*1.0/noAGN_numbers,color=pick_colour(redshift,redshifts),linestyle=categories[names[int(subhalos)]])


    ax1=add_hline(ax1)

    ax1.set_xlabel('mass [Msun]')
    ax1.set_xscale('log')
    ax1.set_xlim(1E7,1E10)
    ax1.set_ylim(0,2)
    ax1.set_ylabel("{0}: ".format(axislabel_type(DM))+"N$_{H-AGN}$ / N$_{H-noAGN}$")

    add_redshifts_to_legend(ax1,redshifts)
    add_categories_to_legend(ax1,categories=categories,loc=4)

    figname="object_numbers_mass_{2}_{0}_nbins{1}".format(len(redshifts),nbins,figname_type(DM))
    saveas(ax1,figname,addendum)

def matched_numbers_mass(redshifts=all_redshifts,nbins=nbins_glob,AGN=True,xlabel=None,ylabel=None,r10=False,ax=None):
    if not ax:
        fig,ax=plt.subplots()

    type=simulation_type(AGN)
    ax=add_hline(ax)

    for redshift in redshifts:
        _separator()
        
        for DM in [False,True]:
            output=find_output_for_redshift(redshift,AGN=AGN)
            halos=get_output_halos(output,DM=DM,AGN=AGN,nmin=500*int(not(DM)))
            halo_masses=np.array([halo.mvir for halo in halos])
            bins=make_bins(halo_masses,nbins=nbins,min_bin=min_limit(DM))

            halo_indices=np.digitize(halo_masses,bins)
            halo_numbers=np.array([sum(halo_indices==bin) for bin in range(len(bins))])

            output=find_output_for_redshift(redshift,AGN=True)
            pair=Pair(output,DM=DM,r10=r10)
            if DM:
                twins=pair.twins
            else:
                twins=pair.get_marks_twins()
            pair_masses=np.array([getattr(twin,type).halo.mvir for twin in twins])
            pair_indices=np.digitize(pair_masses,bins)
            pair_numbers=np.array([sum(pair_indices==bin) for bin in range(len(bins))])
            perc_matched=pair_numbers*1.0/halo_numbers

            print("# Halos",len(halo_masses))
            print("# Twins",len(pair_masses))
            ax.plot(bins,perc_matched,color=pick_colour(redshift,redshifts),linestyle=GMF_linestyle(not(DM)))

    plt.xscale('log')
    ax.set_ylim(0,1.09)
    if not xlabel:
        xlabel='H-{0} mass [Msun]'.format(type)
    if not ylabel:
        ylabel="{0}: ".format(type)+'N$_{twins}$ / N$_{halos}$'
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    add_categories_to_legend(ax,categories={'halos':GMF_linestyle(DM=False),'galaxies':GMF_linestyle(DM=True)},loc="lower center")  #Careful, I swapped the linestyles
    add_redshifts_to_legend(ax,redshifts)

    figname='matched_numbers_mass_{2}_{0}_nbins{1}'.format(len(redshifts),nbins,type)
    if r10:
        figname+="_r10"
    else:
        figname+="_r05"
    saveas(ax,figname)

def numbers_mass(redshifts=all_redshifts,mark=False,DM=False,nbins=30):
    fig,ax=plt.subplots()
    for redshift in sort_redshifts(redshifts):
        _separator_2()
        colour=pick_colour(redshift,sort_redshifts(redshifts))
        output=find_output_for_redshift(redshift,AGN=True)
        pair=Pair(output,DM=DM)

        if mark:
            twins=pair.get_marks_twins(noAGN=True)
        else:
            twins=pair.twins

        masses=np.array([twin.AGN.halo.mvir for twin in twins])
        plot_vs_mass(ax,masses,masses,colour=colour,quartile=False,nbins=nbins,numbers=True,DM=DM)

    ylabel="Number of objects in mass bin"
    xlabel="galaxy mass [Msun]"
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.xscale('log')   
    plt.yscale('log')
    add_redshifts_to_legend(ax,redshifts)
    figname="numbers_mass".format(nbins)
    if mark:
        figname+="_mark"
    if DM:
        figname+="_DM"
    figname+="_nbins{0}_{1}".format(nbins,len(redshifts))
    saveas(ax,figname)
    
def flowratios_mass(redshifts=all_redshifts,nbins=nbins_glob,quartile=False,addendum=None,xlabel="Galaxy mass: $M_{H-AGN}$ [M$_{\odot}$]",ylabel=None):

    flowtypes={"outflow":"-","inflow":"--","net_inflow":":"}
    radii=["R95","R20"]
    simulations=["AGN","noAGN"]

    nplots=4
    pad=10

    x='AGN.halo.mvir'
    x_split=x.split('.')

    for redshift in sort_redshifts(redshifts):
        _separator_2()
        colour = pick_colour(redshift,redshifts)
        output = find_output_for_redshift(redshift,AGN=True)
        pair=Pair(output,DM=False)
        pair.apply_selection_cut()
        #twins=pair.get_marks_twins(noAGN=True,flows=True)
        #masses=np.array([twin.AGN.halo.mvir for twin in twins])

        ylabel=clct.defaultdict(str)

        for i,radius in enumerate(radii):  #picks the radius
            #plot types j: 0= 2 flow plots   1=ratio plots     3 = excess flow plot  4 = metallicity plot
            #Plot numeber I:    R95    1=flow   2=ratio   3=excess 4=metallicity
            #                   R20    5=flow   6=ratio   7=excess 8=metallicity
            for j in range(nplots):
                I=nplots*i+j+1
                fig=plt.figure(I)

                if j==0:    #flow plots, two horizontal
                    print("Flow plots")
                    for k,simulation in enumerate(simulations):    #picks the simulation
                        ax=plt.subplot(1,2,k+1)
                        for flow in ["outflow","inflow"]:
                            data=pair.get_data('{0}.mark.{1}.gas.{2}'.format(simulation,radius,flow))
                            masses=pair.get_data(x)
                            #data=np.array([getattr(getattr(getattr(twin,simulation).mark,radii[i]).gas,flow) for twin in twins])
                            plot_vs_mass(ax,data,masses,colour=colour,quartile=quartile,nbins=nbins,linestyle=flowtypes[flow],x_split=x_split)
                        ylabel[I]=r"Gas mass flow [$\rm M_\odot yr^{-1}$]"
                   #     if redshift==min(redshifts) and i==0:
                            
                            #ax.set_title(simulation,size=14,fontweight='bold')#,xy=(0.85,0.93),xycoords='axes fraction',size='x-large')

                if j==1:     #ratio plots
                    print("Ratio plots")
                    ax=plt.subplot(1,1,1)
                    for flow in ["net_inflow"]:
                        name='AGN.mark.{0}.gas.{1}'.format(radius,flow)
                        data=pair.get_ratios(name)
                        masses=pair.get_data(x)
                        #data=np.array([getattr(getattr(twin.AGN.mark,radii[i]).gas,flow) for twin in twins])/np.array([getattr(getattr(twin.noAGN.mark,radii[i]).gas,flow) for twin in twins])
                        data,masses_clean=sanitize_ratios(data,masses,min_ratio=0.0,max_ratio=1E5)
                        plot_vs_mass(ax,data,masses_clean,colour=colour,quartile=True,nbins=nbins,linestyle=flowtypes[flow],x_split=x_split)
                        ylabel[I]=r'Gas net flow ratio $ \dot{M}_{\rm H-AGN}/\dot{M}_{\rm H-noAGN}$'
                    ax=add_hline(ax)
                    ax.set_ylim(0,3)

                if j==2:   #excess plots
                    print("Residual plots")
                    ax=plt.subplot(1,1,1)
                    for flow in ["outflow","inflow"]:
                        name='AGN.mark.{0}.gas.{1}'.format(radius,flow)
                        data=pair.get_data(name)-pair.get_data('no'+name)
                        masses=pair.get_data('AGN.halo.mvir')
                        #data=np.array([getattr(getattr(twin.AGN.mark,radii[i]).gas,flow) for twin in twins])-np.array([getattr(getattr(twin.noAGN.mark,radii[i]).gas,flow) for twin in twins])
                        plot_vs_mass(ax,data,masses,colour=colour,quartile=quartile,nbins=nbins,linestyle=flowtypes[flow])
                    ylabel[I]=r"Residual gas flow [$\rm M_\odot yr^{-1}$]"
                    ax=add_hline(ax,value=0.0)
                    ax.set_ylim(-1E3,1E3)

                if j==3: #outflow metallicity ratio plots
                    print("Metallicity plots")
                    ax=plt.subplot(1,1,1)
                    for flow in ["inflow"]:
                        name='AGN.mark.{0}.gas.{1}'.format(radius,flow)
                        data=pair.get_ratios(name)
                        masses=pair.get_data('AGN.halo.mvir')
                        #data=np.array([getattr(getattr(twin.AGN.mark,radii[i]).z,flow) for twin in twins])/np.array([getattr(getattr(twin.noAGN.mark,radii[i]).z,flow) for twin in twins])
                        plot_vs_mass(ax,data,masses,colour=colour,quartile=True,nbins=nbins,linestyle=flowtypes[flow])
                    ylabel[I]="outflow metallicity ratio - {0}: AGN/noAGN".format(radii[i])
                    ax=add_hline(ax)

    radii_titles={'R20':r'$0.20 \times R_{\rm vir}$: Galaxy scale','R95':r'$0.95 \times R_{\rm vir}$: Halo scale'}

    for i in range(len(radii)):
        for j in range(nplots):
            I=nplots*i+j+1
            fig=plt.figure(I)
            for ax in fig.axes:
                ax.set_ylabel(ylabel[I])
                ax.set_xlabel(xlabel)
                if j in [0,2,3]:
                    ax.set_yscale('symlog')
                ax.set_xscale('log')
                annotate_mass_types(ax,redshifts)
                if i==1:
                    add_redshifts_to_legend(ax,redshifts)
                else:
                    if j in [0,2]:
                        add_categories_to_legend(ax,{key:flowtypes[key] for key in ['outflow','inflow']},loc=2)
            fig.set_size_inches(8,6)
            plt.tight_layout()
            if j==0:
                for loc_ax, simulation in zip(fig.axes, simulations):
                    loc_ax.annotate(simulation, xy=(0.5, 1), xytext=(0, pad),xycoords='axes fraction', textcoords='offset points', size='x-large', ha='center', va='baseline')
                fig.axes[0].annotate(radii[i], xy=(-0.1, 0.5), xytext=(-ax.yaxis.labelpad - pad, 0),xycoords='axes fraction', textcoords='offset points',size='x-large', ha='right', va='center',rotation=90)
                figname="flows"
                fig.set_size_inches(16,7)
                plt.tight_layout()
                fig.subplots_adjust(top=0.90,left=0.10)
            elif j==1:
                plt.title(radii_titles[radii[i]])
                figname="flowratios"
            elif j==2:
                plt.title(radii_titles[radii[i]])
                figname="residual_flows"
            elif j==3:
                plt.title(radii_titles[radii[i]])
                figname="metallicity_ratios"
                    
            figname+="_masses"
            if quartile or j==1:
                figname+="_quartile"
            figname+="_{0}_{1}".format(radii[i],len(redshifts))
            saveas(fig,figname,addendum)


#This plots the median ratio vs mass for various redshifts
def ratios_mass(redshifts=all_redshifts,DM=False,nbins=nbins_glob,quartile=False,apply_cut=True,stars=False,addendum=None,ylabel=None,xlabel=None,number=False,saveit=True,linestyle='-',ax=None,x='AGN.halo.mvir',y='AGN.halo.mvir',log=True,limits=[None,None]):

    if not ax:
        fig,ax=plt.subplots()

    categories={}
    x_split=x.split('.')
    for redshift in sort_redshifts(redshifts):
        _separator_2()
        colour=pick_colour(redshift,redshifts)
        output=find_output_for_redshift(redshift,AGN=True)
        pair=Pair(output,DM=DM)    #Used to have r10=r10. r10 is the percentage virial radius criteria used to match galaxies to halos

        if apply_cut and x_split[0]=='AGN' and not(DM):
            print("Only applying the mass cut if plotting against M_H-AGN")
            pair.apply_selection_cut()
        
        masses=pair.get_data(x)
        ratios=pair.get_ratios(y)
        plot_vs_mass(ax,ratios,masses,x_split=x_split,colour=colour,quartile=quartile,nbins=nbins,linestyle=linestyle,limits=[masses.min(),masses.max()])
        #if SF:
        #    AGN_efficiency=np.array([twin.AGN.mark.Mstar for twin in twins])/np.array([twin.AGN.mark.Mbaryon for twin in twins])
        #    noAGN_efficiency=np.array([twin.noAGN.mark.Mstar for twin in twins])/np.array([twin.noAGN.mark.Mbaryon for twin in twins])
        #    data=AGN_efficiency/noAGN_efficiency
        #    ylabel_loc="AGN efficiency / noAGN efficiency"
        #    linestyle='-'
        #    categories[ylabel_loc]=linestyle 
        #    plot_vs_mass(ax,data,masses,colour=colour,quartile=quartile,nbins=nbins,linestyle=linestyle,BH=BHmass)
        
    if not DM and 'mvir' in x_split:
        print("Adding mass types")
        ax=annotate_mass_types(ax,redshifts)
    ax=add_hline(ax)

    if not ylabel:
        ylabel="median ratio of {0}".format(y)
    if not xlabel:
        xlabel=x

    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    if log:
        plt.xscale('log')
    ylims=ax.get_ylim()
    if not DM:
        ax.set_ylim(0,max(1.1,ylims[1]))
    else:
        ax.set_ylim(0.8,1.04)

    add_redshifts_to_legend(ax,redshifts)
    figname="ratios_{0}_{1}".format(x,y)
    if quartile:
        figname+="_quartile"
    figname+="_{1}_{0}_nbins{2}".format(len(redshifts),x_split[0],nbins)
    if saveit:
        saveas(ax,figname,addendum)
        return
    else:
        return ax

#This function make a history of the fractions for particular mass bins.
def ratios_z(nhists=4,kwargs={},mean=False,median=True,DM=False,fixed=False,addendum=None,quartile=True,apply_cut=False,total_mass=False,ylabel=None):
    AGN,noAGN=get_outputs(DM=True)
    #AGN=[43,70,90]#,125,197,343,519,752]

    if fixed:
        nhists=4
        print("There are always 4 fixed bins")

    means=np.zeros((len(AGN),nhists))
    medians=np.zeros((len(AGN),nhists))
    lower_quartiles=np.zeros((len(AGN),nhists))
    upper_quartiles=np.zeros((len(AGN),nhists))
    mass_cuts=np.zeros((len(AGN),nhists+1))
    z=np.zeros(len(AGN))
    
    for i in range(len(AGN[:])):

        _separator_2()
        out_AGN=AGN[i]
        pair=Pair(out_AGN,DM=DM)
        if apply_cut:
            pair.apply_selection_cut()
        data=masscut_histogram(out_AGN,pair=pair,nhists=nhists,mean=True,median=True,plotit=False,fixed=fixed,total_mass=total_mass,**kwargs)
        means[i,:]=data.means[:]
        medians[i,:]=data.medians[:]
        lower_quartiles[i,:]=data.lower_quartiles[:]
        upper_quartiles[i,:]=data.upper_quartiles[:]
        mass_cuts[i,:]=data.mass_cuts[:]
        z[i]=pair.AGN.ds.z

    fig, ax1 = plt.subplots()
    ax2=ax1.twinx()
    for i in range(1,nhists):
        ax2.plot(z,mass_cuts[:,i],color=mono(i-1),linestyle=':')

    linestyle='-'

    for i in range(nhists)[::-1]:
        if mean:
            ax1.plot(z,means[:,i],color=ygb_small(i),linestyle=mean_linestyle())
            ylabel_loc='mean'
        if median and mean:
            ax1.plot(z,medians[:,i],color=ygb_small(i),linestyle=median_linestyle())
            ylabel_loc=""
        if median:
            if quartile:
                ax1.fill_between(z,lower_quartiles[:,i],upper_quartiles[:,i],color=ygb_small(i),alpha=alpha_region)
                ax1.plot(z,lower_quartiles[:,i],color=ygb_small(i),linewidth=1)
                ax1.plot(z,upper_quartiles[:,i],color=ygb_small(i),linewidth=1)
                linestyle='-'
            ax1.plot(z,medians[:,i],color=ygb_small(i),linestyle=linestyle)
            ylabel_loc='median'

    if not DM:
        ax=add_hline(ax)
    add_galaxies_to_legend(ax1=ax1,add_labels=True,ax2=ax2,nhists=nhists,outside=False,loc=3)
    if mean and median:
        add_measures_to_legend(ax1,mean=mean,median=median)
    
    if total_mass:
        masstype="total mass"
    else:
        masstype="virial mass"

    if not ylabel:
        ylabel=ylabel_loc+" {0}: {1} , {2}".format(axislabel_type(DM),label_ratio,masstype)
    ax1.set_ylabel(ylabel)
    ax2.set_ylabel('Masscuts [M$_{\odot}$]',color=mono(nhists-3))

    history(ax1)
    if fixed:
        cut_type='fixed'
    else:
        cut_type='perc'
    figname="ratios_z_{1}_{0}_{2}".format(nhists,figname_type(DM),cut_type)
    if not apply_cut:
        figname+="_Ncut"
    if total_mass:
        figname+='_mh'
    if quartile:
        figname+="_quartile"

    saveas(ax1,figname,addendum)
    
#This makes mass histograms split into mass bins. It returns data that can be used for histories over time
def masscut_histogram(out_AGN,plotit=False,nhists=3,nbins=100,normed=False,DM=False,mean=False,median=True,pair=None,fixed=False,total_mass=False,addendum=None):
    hist_lims=[0.1,2.0]   #For the plot axes
    
    #Read the data
    if not pair:
        pair=Pair(out_AGN,DM=DM)

    if total_mass:
        AGNmasses=pair.get_total_halo_mass(AGN=True,mh=True)
        noAGNmasses=pair.get_total_halo_mass(AGN=False,mh=True)
        ratios=AGNmasses/noAGNmasses
    else:
        AGNmasses=np.array([twin.AGN.halo.mvir for twin in pair.twins])
        noAGNmasses=np.array([twin.noAGN.halo.mvir for twin in pair.twins])
        ratios=np.array([twin.ratio for twin in pair.twins])


    AGNmasses=np.log10(AGNmasses)

    #Make the mass_cuts
    if nhists>1:
        mass_cuts=find_masscuts(pair,nhists=nhists,fixed=fixed,total_mass=total_mass)
    else:
        mass_cuts=[0,AGNmasses.max()]
    mass_cuts[-1]+=1   #To make sure the most massive galaxy is actually included
    print("The mass bins for output",out_AGN,"are",mass_cuts)

    #Set up the figure
    if plotit:
        fig=plt.figure()
        ax=plt.subplot()

    #Collect means and medians & plot histograms
    means=[]
    medians=[]
    lower_quartiles=[]
    upper_quartiles=[]
    for i in range(len(mass_cuts)-1):
        bin_ratios=ratios[np.array([mass_cuts[i]<mass<=mass_cuts[i+1] for mass in AGNmasses])]
        means.append(np.mean(bin_ratios))
        medians.append(np.median(bin_ratios))
        lower_quartiles.append(np.nanpercentile(bin_ratios,25))
        upper_quartiles.append(np.nanpercentile(bin_ratios,75))

        print("In the mass bin {0} < mass <= {1} there are".format("%0.2g"%mass_cuts[i],"%0.2g"%mass_cuts[i+1]),len(bin_ratios),"objects with a median of",medians[i])

        if plotit:
            colour=pick_colour(i,range(len(mass_cuts)-1))
            data,bins=make_histogram(bin_ratios,nbins=nbins)
            ax.hist(bin_ratios,bins=bins,label=labels(i),normed=normed,color=colour,histtype='step')
            
            if mean:
                ax.axvline(means[i],color=colour,linestyle=mean_linestyle(),label=labels(i))

            if median:
                ax.axvline(medians[i],color=colour,linestyle=median_linestyle(),label=labels(i))


    #Style the plot accordingly and save it
    if plotit:
        ax.axvline(1,color=colour_1(),linestyle=median_linestyle())
        if nhists>1:
            add_galaxies_to_legend(ax1=ax,add_labels=True,nhists=nhists,outside=True)
        histogram(ax=ax,normed=normed,title='Mass fractions at z={0}'.format(redshift_for_plot(pair)))
        #add_measures_to_legend(ax,mean=mean,median=median)
        plt.xlim(hist_lims[0],hist_lims[1])
        figname="masscut_histogram"
        if fixed:
            figname+="_fixed"
        else:
            figname+="_perc"
        if normed:
            figname+="_n"
        figname+="_{0}_{1}".format(output_for_pair(pair),nhists)
        saveas(ax,figname,addendum)

    #Return the means, medians etc for use in histories
    print("Returning the mass_cuts, means and medians")
    DataTuple=clct.namedtuple('DataTuple',['mass_cuts','means','medians','lower_quartiles','upper_quartiles'])

    data=DataTuple(mass_cuts,means,medians,lower_quartiles,upper_quartiles)
    return data

def GMF(output,nbins=22,ax=None,saveit=True,observations=True,sugata_comparison=False,twinned=False):
    _separator()
    if not ax:
        fig,ax=plt.subplots()
        
    if observations:
        ax=overplot_observations(ax=ax,output=output)
    for DM in [True,False]:
        for AGN in [True,False]:
            _separator_2()
            #Load in halos and make bins
            out=pairup_output(output,AGN=AGN,DM=DM)
            halos=get_output_halos(out,AGN=AGN,DM=DM)
            masses=np.array([halo.mh for halo in halos])
            print("Mass extrema",np.log10(masses.min()),np.log10(masses.max()))
            ax=draw_GMF(ax,masses,color=mono(int(AGN)+1),linestyle=GMF_linestyle(DM),DM=DM,nbins=nbins,errors=not(DM),sugata_bins=not(DM))
            
            if AGN and DM:
                baryon_fraction=0.165
                masses=masses*baryon_fraction
                ax=draw_GMF(ax,masses,errors=False,color=colour_1(),linestyle='-.',min_bin=1E9,nbins=nbins,sugata_bins=False)
                
                if sugata_comparison:
                    if output==70:
                        AGN_x=[9.31578947368421,9.894736842105262, 10.491228070175438]
                        AGN_y=[-2.856920077972709,-3.6526315789473687, -4.907472384665367]
                        noAGN_x=[9.280701754385964, 9.894736842105262,10.502923976608187, 11.064327485380115]
                        noAGN_y=[ -2.871994801819363, -3.5044834307992208, -4.462941303876977, -5.955079055663851]
                    elif output==125 or output==139:
                        AGN_x=[9.269006893537556,9.755964797266424, 10.802014422790393, 11.25410731668554]
                        AGN_y=[ -1.9574468085106385, -2.2695035460992905, -3.390070921985816, -4.595744680851064]
                        noAGN_x=[9.252516050295222,9.679922305398229,10.22739422275317,10.733268587831857, 11.27077084389745,11.69908811977345]
                        noAGN_y=[ -1.9455106443323336, -2.193514563440565,-2.651947092038882, -3.1634594724674057, -3.965192130843211, -4.9128600036097065]
                    elif output==343:
                        AGN_x=[9.159309581234067,9.986885133337339, 10.374597670566223, 10.846458213528567, 11.171754971718922, 11.554987168373259, 11.833266099614622]
                        AGN_y=[-1.725686427657949,-1.9934082345569872, -2.205786677424062, -2.7072415006566026, -3.233964758087359, -4.023363001999847, -4.628466470401429]
                        noAGN_x=[9.159326747289908,9.934477164854217, 10.552746998085984, 11.276004428842407,11.763640577122796, 12.43512518346222]
                        noAGN_y=[-1.7388012943206104, -1.9537203134521794, -2.3118729025225524, -2.880550000429152, -3.434567286647385, -4.448806529967644]
                    elif output==519:
                        AGN_x=[9.164265129682997,9.752161383285303, 10.293948126801153, 10.749279538904899, 11.12391930835735, 11.504322766570606, 11.878962536023055, 12.121037463976945 ] 
                        AGN_y=[-1.6776541366036306, -1.8193483245335407, -2.034371778960432, -2.3376175350764625, -2.8312333031826493, -3.4853909421737925,-4.32937167378363,-4.823955068470097]
                        noAGN_x=[9.15850144092219, 9.752161383285303, 10.322766570605188, 10.881844380403459, 11.440922190201729, 11.965417867435159, 12.46685878962536, 12.7492795389048993]
                        noAGN_y=[ -1.6484991270325415, -1.79015124424157, -2.0487599655020086, -2.351248448642167, -2.8143208733881644, -3.3944340436273364, -4.0769052777719335, -4.527398556974273]

                    ax.plot(AGN_x,AGN_y,label='AGN')
                    ax.plot(noAGN_x,noAGN_y,label='noAGN')
                
    #ax.xaxis.set_major_locator(MultipleLocator(1))
    #ax.xaxis.set_minor_locator(MultipleLocator(0.2))
    #ax.yaxis.set_major_locator(MultipleLocator(1))
    #ax.yaxis.set_minor_locator(MultipleLocator(0.2))

    ax.set_ylim(-7,0)
    #plt.yscale('log')
    filename="GMF_{0}_nbins{1}".format(str(output).zfill(3),nbins)
    if output==713:
        filename+="_Bernardi"
    if saveit:
        #Adjust plot properties
        xlabel='log$_{10}(M)$[M$_\odot$]'
        ylabel='log$_{10}$($\Phi$)[Mpc$^{-3} $dex$^{-1}$]'
        plt.xlabel(xlabel)
        plt.ylabel(ylabel)
        add_categories_to_legend(ax,categories={'halos':GMF_linestyle(DM=True),'galaxies':GMF_linestyle(DM=False)},loc=3)
        add_simulations_to_legend(ax,categories={'H-AGN':'k','H-noAGN':'gray'},linestyle='-')
        saveas(ax,filename)
    else:
        return ax


##########################################################################  
#Plot functions

#This function draws massfunctions for the input masses
def draw_GMF(ax,masses,errors=False,color='red',linestyle=None,DM=False,nbins=50,min_bin=None,sugata_bins=False):
    if not(min_bin):
        min_bin=max(1E9,min_limit(DM))
    bins=make_bins(masses,nbins=nbins,min_bin=min_bin,maxbinNumber=1,sugata=sugata_bins)
    #plot_bins=bins[1:]
    plot_bins=(bins[1:]+bins[:-1])/2
    print("There are",len(masses),"objects")

    #Count objects and set up errors
    counts, bins=np.histogram(masses,bins)
    cum_counts=np.cumsum(counts[::-1])[::-1]
    error_values=np.array([np.sqrt(count) for count in cum_counts])
    error_factor=1

    #Conversion factor to Bernardi2013 units
    loc_width=bins[1:]-bins[:-1]
    Bernardi_conversion=1/MpcBox**3*np.log10(plot_bins)/np.log10(loc_width)
    
    #Convert all quantities to Bernardi units
    log_counts=np.log10(cum_counts*Bernardi_conversion)
    plot_bins=np.log10(plot_bins)
    upper_error=np.log10(cum_counts*Bernardi_conversion*(1+float(error_factor)/error_values))
    error_values=np.array([min(0.25,float(error_factor)/error) for error in error_values])
    lower_error=np.log10(cum_counts*Bernardi_conversion*(1-error_values))

    if not linestyle:
        linestyle=GMF_linestyle(DM)

        #[log_counts-lower_error,upper_error-log_counts]
    if errors:
        (_,caps,_)=ax.errorbar(plot_bins,log_counts,yerr=[log_counts-lower_error,upper_error-log_counts],linestyle=linestyle,color=color)
        #(_,caps,_)=ax.errorbar(plot_bins,log_counts,yerr=log_counts/error_values,linestyle=linestyle,color=color)
    else:
        ax.plot(plot_bins,log_counts,linestyle=linestyle,color=color)
    return ax

#This functions is called by ratios_masses and bins&draws the data on the plot
def plot_vs_mass(ax,data,masses,x_split='empty',nbins=nbins_glob,quartile=False,colour="Pink",linestyle='-',numbers=False,DM=False,BH=False,error=False,limits=[0,1E40],alpha_line=1.0):
    if not(len(data)==len(masses)):
        print("The two arrays are different lengths! Call get_data(y) before calling get_data(x)",len(data),len(masses))

        exit
    if DM:
        bins=make_bins(masses,nbins=nbins,min_bin=max(limits[0],min_limit(DM)-1),max_bin=min(limits[1],1E15))
    elif (('sink' in x_split) and ('mass' in x_split)):
        bins=make_bins(masses,nbins=nbins,min_bin=max(limits[0],1E6),max_bin=min(limits[1],max(masses)))
    elif 'f_gas' in x_split or 'GALf_gas' in x_split:
        bins=make_bins(masses,nbins=nbins,min_bin=limits[0],max_bin=limits[1])
    else:
        if masses.min()>1E5:
            bins=make_bins(masses,nbins=nbins,min_bin=max(limits[0],min_limit(DM)),max_bin=min(limits[1],np.sort(masses)[-20]))
        else:
            print("Using min and max of sample")
            bins=make_bins(masses,nbins=nbins,min_bin=masses.min(),max_bin=masses.max())
    indices=np.digitize(masses,bins)
    
    medians=[];
    lower_quartiles=[];
    upper_quartiles=[]
    for i in range(1,len(bins)):
        bin_data=data[indices==i]
        bin_data= bin_data[np.logical_not(np.isnan(bin_data))]
        bin_data=bin_data[bin_data<1E308]
        #bin_data=bin_data[0<bin_data]
        #        print bin_data
        medians.append(np.median(bin_data))
        lower_quartiles.append(np.nanpercentile(bin_data,25))
        upper_quartiles.append(np.nanpercentile(bin_data,75))
    
    #medians=np.array([np.median(data[indices==i]) for i in range(1,len(bins))])
    if numbers:
        medians=np.array([len(data[indices==i]) for i in range(1,len(bins))])

    medians=np.array(medians)
    lower_quartiles=np.array(lower_quartiles)
    upper_quartiles=np.array(upper_quartiles)
    plot_bins=[(bins[i]+bins[i-1])/2 for i in range(1,len(bins))]
    
    if quartile:
        ax.fill_between(plot_bins,lower_quartiles,upper_quartiles,color=colour,alpha=alpha_region)
        #ax.plot(plot_bins,lower_quartiles,color=colour,linewidth=1)
        #ax.plot(plot_bins,upper_quartiles,color=colour,linewidth=1)
        linestyle='--'

    if error:
        ax.errorbar(plot_bins,medians,yerr=[medians-lower_quartiles,upper_quartiles-medians],color=colour,linestyle=linestyle,alpha=alpha_line,elinewidth=1)
    else:
        ax.plot(plot_bins,medians,color=colour,linestyle=linestyle,alpha=alpha_line)
    return ax

def get_eddington_twins(pair,eddington):
    twins=pair.get_sink_twins(accretion=True)
    eddington_ratios=np.array([twin.AGN.mark.sink.eddington_ratio for twin in twins])
    if eddington=='quasar':
        edd_twins=twins[eddington_ratios>=0.01]
    elif eddington=='radio':
        edd_twins=twins[eddington_ratios < 0.01]
    else:
        print(eddington,"is not a valid feedback mode")
    print("In",eddington,'mode there are',len(edd_twins),'out of',len(twins))
    return edd_twins

def annotate_mass_types(ax,redshifts):
    "This function annotates dotted grey lines to split the mass bins into small, medium and large galaxies. Written for ratios_mass"
    ax.axvline(1E9,color='grey',linestyle='-',lw=linewidth-2)
    ax.axvline(1E11,color='grey',linestyle='-',lw=linewidth-2)
    if redshifts:
        if min(redshifts)<3:
            ax.annotate('small',xy=(0.01,0.01),xycoords='axes fraction')
            ax.annotate('medium',xy=(0.26,0.01),xycoords='axes fraction')
            ax.annotate('large',xy=(0.77,0.01),xycoords='axes fraction')
    return ax

def galaxy_legend_entries(nhists=3):
    loc_handles=[mlines.Line2D([],[],color=ygb_small(i),linestyle='-',label=labels(i)) for i in range(nhists)]
    loc_labels=[labels(i) for i in range(nhists)]
    DataTuple=clct.namedtuple('DataTuple',['handles','labels'])
    entry=DataTuple(loc_handles,loc_labels)
    return entry

def redshift_legend_entries(redshifts):
    outputs=[find_output_for_redshift(redshift,AGN=True) for redshift in redshifts]
    loc_handles=[mlines.Line2D([],[],color=pick_colour(redshift,redshifts)) for redshift in redshifts]
    loc_labels=["$z={0}$".format(redshift_for_plot(output=output,sf=1)) for output in outputs]
    DataTuple=clct.namedtuple('DataTuple',['handles','labels'])
    entry=DataTuple(loc_handles,loc_labels)
    return entry

def add_redshifts_to_legend(ax,redshifts,loc=0):
    ''' Add all redshifts to legend, in ax'''
    ax_handles=redshift_legend_entries(redshifts).handles
    ax_labels=redshift_legend_entries(redshifts).labels
    legend=plt.legend(ax_handles,ax_labels,loc=loc,frameon=False)
    ax=plt.gca().add_artist(legend) 
    return ax

def add_simulations_to_legend(ax,categories={'AGN':'red','noAGN':'pink'},loc=0,linestyle='-'):
    handles=[]; labels=[]
    for item in categories:
        line=mlines.Line2D([],[],color=categories[item],linestyle=linestyle,label=item)
        handles.append(line)
        labels.append(item)
    new_legend=plt.legend(handles,labels,frameon=False,loc=loc)
    ax=plt.gca().add_artist(new_legend)
    return ax

def add_categories_to_legend(ax,categories={'halos':'-','galaxies':'--'},loc=0):
    handles=[]
    labels=[]
    for item in categories:
        line=mlines.Line2D([],[],color='k',linestyle=categories[item],label=item)
        handles.append(line)
        labels.append(item)
    new_legend=plt.legend(handles,labels,frameon=False,loc=loc)
    ax=plt.gca().add_artist(new_legend)
    return ax

def histogram(ax,normed,title):
    plt.xscale('log')
    plt.xlabel(label_ratio)
    if normed:
        ylabel="Normalised number count"
    else:
        ylabel="Number count"
    plt.ylabel(ylabel)
    plt.title(title)
    return ax

def history(ax,title=None):
    plt.gca().invert_xaxis()
    ax.set_xlabel('redshift')
    if title:
        plt.title(title)
    return ax

def mean_linestyle():
    return '-'

def median_linestyle():
    return '--'

def GMF_linestyle(DM):
    if DM:
        linestyle=':'
    else:
        linestyle='-'
    return linestyle

#############################################################################   
#Data processing functions

def sanitize_ratios(ratios,masses,min_ratio=0.01,max_ratio=100):
    min_index=ratios>min_ratio
    ratios=ratios[min_index]
    masses=masses[min_index]

    max_index=ratios<max_ratio
    ratios=ratios[max_index]
    masses=masses[max_index]
    return ratios,masses

def make_bins(xdata,nbins=nbins_glob,min_bin=0,max_bin=1E20,maxbinNumber=20,sugata=False,log=True):
    xdata=np.sort(xdata)
    if log:
            bins=np.logspace(np.log10(max(xdata[xdata>0][maxbinNumber],min_bin)),np.log10(min(xdata[-1*maxbinNumber],max_bin)),nbins)
    else:
        bins=np.linspace(min_bin,max_bin,nbins)
    if sugata:
        print("Using Sugata's bins")
        bins=np.array([10**power for power in np.arange(8.5,14.00,0.5)])#np.arange(9.02,13.00,0.3)])
        #to reproduce Sugata plots, use 0.5 as step size 
    return bins

#Get masscut => careful, result is the log
def find_masscuts(pair,nhists=3,fixed=False,total_mass=False):
    _separator()
    nbins=1000

    AGN_masses=np.array([twin.AGN.halo.mh for twin in pair.twins])
    if total_mass:
        AGN_masses=np.array([twin.AGN.halo.total_mass(mh=True) for twin in pair.twins])

    if fixed:
        if pair.DM:
            mass_cuts=[0,1E12,1E13,1E14,1E20]
        else:
            mass_cuts=[0,1E10,1E11,1E12,1E20]
    else:

        #10-40-40-10 split
        mass_cuts=[0]
        for perc in [10,50,90,100]:
            mass_cuts.append(np.percentile(AGN_masses,perc))
            
    mass_cuts[-1]+=5
    return np.log10(np.array(np.array(mass_cuts)))  

#Make a histogram
def make_histogram(data,bins=[],box=MpcBox,nbins=50,Bernardi_units=False):

    if len(bins)<1:
        print("Making",nbins,"bins for histogram.")
        bins=np.logspace(np.log10(min(data)),np.log10(max(data)),nbins,endpoint=True)

    if box!=MpcBox:
        print("Not using the HORIZON box size")

    counts,bins=np.histogram(data,bins)

    plot_bins=(bins[1:]+bins[:-1])/2
    loc_width=bins[1:]-bins[:-1]

    cum_counts=np.cumsum(counts[::-1])[::-1]
    cum_counts=np.array([float(count) for count in cum_counts])

    if Bernardi_units:
        cum_counts=cum_counts/box**3 * np.log10(plot_bins)/np.log10(loc_width)
        return np.log10(cum_counts),np.log10(plot_bins)
    else:
        return cum_counts,bins

#Load a dataset
def load(output,AGN=True,DM=False):
    filename="{1}/info_{0}.txt".format(str(output).zfill(5),infoname(AGN,DM))
    with open(filename) as f:
        data=[]
        for i in range(1,19):
            data.append(f.readline().split())
    ds=Dataset(data,AGN,DM,output=output)
    return ds

#Read Bernardi data
def read_Bernardi_data():
    filename='MsF_SerExp.dat'
    print("Opening",filename)
    with open(filename) as f:
        lines=f.read().splitlines()

    cum_counts=[]
    errors=[]
    bins=[]
    for line in lines:
        data=line.split()
        bins.append(float(data[0]))
        cum_counts.append(float(data[1]))
        errors.append(float(data[2]))
    return cum_counts,errors,bins
    
#Collect all masses from the yt_halo files directly
def get_output_masses(output,DM=True,AGN=True):
    halos=np.array(get_output_halos(output,DM=DM,AGN=AGN))
    masses=np.array([halo.mvir for halo in halos])
    return masses

def get_output_halos(output,AGN,DM,pair=None,nmin=0):
    if not pair:
        filename=foldername(AGN=AGN,DM=DM)

        if DM:
            npart=100
        else:
            npart=10

        filename+="/yt_halos_{0}_{1}.txt".format(str(output).zfill(3),npart)

    else:
        if AGN:
            simulation='AGN'
        else:
            simulation='noAGN'
        
        if pair.DM:
            filename='./DM_TWINS/yt_halos_{1}_{0}.csv'.format(str(output).zfill(3),simulation)
        else:
            if pair.r10:
                radius='R10'
            else:
                radius='R05'
            filename='./STAR_TWINS_{0}/'.format(radius)
            if DM:
                filename+='yt_DM_halos'
            else:
                filename+='yt_STAR_halos'
            filename+='_{1}_{0}.csv'.format(str(output).zfill(3),simulation)
        
    print("Opening",filename)

    halo_object=Halo
    halos=[]
    with open(filename) as f:
        reader=csv.reader(f)
        if pair:
            next(reader)
        halos=[]
        for row in reader:
            if not pair:
                line='\t'.join(row[0:4])
                line=line.split()
            else:
                line=row
            halos.append(halo_object(line,DM=DM))

    if nmin>0:
        print("CAREFUL,the order of galaxies will not match twin files if nmin is used")
        print("There are",len(halos),"objects in the whole sample")
        if DM:
            halos_nmin=np.array([halo for halo in halos if halo.npart>=nmin])
            halos=halos_nmin
            print("There are",len(halos),"halos with more than",nmin,"particles.")
        else:
            halos_nmin=get_output_halos(output,AGN=AGN,DM=True)
            halos_ids_nmin=np.array([halo.id for halo in halos_nmin if halo.npart>(nmin-1)])

            filename="./AGN_VS_NOAGN/"+"halo2galaxy"
            if AGN:
                filename+='_AGN'
            else:
                filename+='_noAGN'
            filename+='_{0}.asc'.format(str(output).zfill(3))
            print("Opening",filename)
            with open(filename) as f:
                reader=csv.reader(f,delimiter='\t')
                next(reader)
                next(reader)
                rows=np.array([row for row in reader])
                halo_ids=np.array([int(row[0]) for row in rows])
                galaxy_ids=np.array([int(row[1]) for row in rows])
            
            #Find ids of all galaxies in large enough halos
            galaxy_ids_nmin=np.array([galaxy_ids[halo_ids==id][0] for id in halo_ids if id in halos_ids_nmin]) 
            print("There are",len(galaxy_ids_nmin),"galaxies in halos with more than",nmin,'Particles')
            #return halos for galaxies 
            included_nmin=np.array([halo for halo in halos if halo.id in galaxy_ids_nmin])
            halos=included_nmin

    return np.array(halos)

def write_nstepcoarse():
    outputs=get_outputs()[0]
    filename='nstepcoarse_AGN.txt'
    with open(filename,'wb') as f:
        writer=csv.writer(f)
        values=['{0}'.format(str(get_nstepcoarse(output)).zfill(5)) for output in outputs]
        writer.writerow(['{0}'.format(str(get_nstepcoarse(output)).zfill(5)) for output in outputs])



#############################################################################   
#Small utilities

def add_hline(ax,value=1.0):
    ax.axhline(value,color=colour_1(),linestyle='-.')
    return ax

def min_limit(DM):
    ''' Minimum mass bin to be plotted'''
    if DM:
        return 4E10
    else:
        return 2E8

def sci(number):
    return '{0:2.4e}'.format(number)


def saveas(plot,figname,addendum=None,png=False):
    if type(plot)==mpl.figure.Figure:
        axes=plot.axes
    else:
        plt.tight_layout()
        axes=[plot]
    for ax in axes:
        ax.tick_params(axis='x', pad=5)
    if addendum:
        figname+='_{0}'.format(addendum)
    if png:
        figname=figname+".png"
    else:
        figname=figname+".pdf"

    print("Saving figure as {0}".format(figname))
    if type(plot)==mpl.figure.Figure:
        plot.savefig(figname)
    else:
        plt.savefig(figname)
    plt.clf()

def simulation_type(AGN):
    if AGN:
        return 'AGN'
    else:
        return 'noAGN'

def figname_type(DM):
    if DM:
        return 'DM'
    else:
        return 'galaxies'

def axislabel_type(DM):
    if DM:
        return 'halos'
    else:
        return 'galaxies'

def find_output_for_redshift(z,DM=False,AGN=True):
    outputs=get_outputs(DM=True)
    simulation_outputs=outputs[int(not(AGN))]
    redshifts=np.array([load(output,AGN=AGN,DM=DM).z for output in simulation_outputs])
    redshifts=abs(redshifts-z)

    z_output=simulation_outputs[redshifts==redshifts.min()][0]
    print("Redshift z=",z,"corresponds to",simulation_type(AGN),"output",z_output)
    return z_output


def get_nstepcoarse(out_AGN):
    ds=load(out_AGN)
    return ds.nstep_coarse

def grid(x, y, resX=100, resY=100):
    "Convert 3 column data to matplotlib grid"
    xi = linspace(min(x), max(x), resX)
    yi = linspace(min(y), max(y), resY)
    Z = griddata(x, y, xi, yi)
    X, Y = meshgrid(xi, yi)
    return X, Y, Z

def sort_redshifts(redshifts):
    return np.sort(redshifts)

#output for figure name
def output_for_pair(pair):
    return str(pair.AGN.output).zfill(3)

#redshift for titles and labels
def redshift_for_plot(pair=None,sf=3,output=None):
    if pair:
        return np.round(pair.AGN.ds.z,sf)
    else:
        return np.round(load(output).z,sf)

def huge():
    return 1E32

def labels(i):
    labels=['Smallest 10 % in $M_{H-AGN}$','10 % < $M_{H-AGN}$ < 50 %','50 % < $M_{H-AGN}$ < 90 %','Largest 90 % in $M_{H-AGN}$','XX','XX3']
    return labels[i]

def blue(i):
    #colours=['LightBlue','DodgerBlue','DarkBlue','Black','Red','Orange','Yellow']
    #colours=['#bdd7e7','#6baed6','#3182bd','#08519c']
    #colours=['#bdd7e7','#3182bd','#08519c','#08306b']
    colours=['#c6dbef','#6baed6','#2171b5','#08306b']
    return colours[i]

def ygb_big(i):
    #colours=['#c7e9b4','#7fcdbb','#41b6c4','#1d91c0','#225ea8','#253494','#081d58']
    colours=['#d73027','#fc8d59','#fee090','#e0f3f8','#91bfdb','#4575b4']

    if i >= len(colours):
        colours=['#ffffd9','#edf8b1','#c7e9b4','#7fcdbb','#41b6c4','#1d91c0','#225ea8','#253494','#081d58']
    
    while i>=len(colours)-1:
        i=i-len(colours)


    colours=colours[::-1]
    return colours[i]

def ygb_small(i):
#    colours=['#ffffbf','#a1dab4','#41b6c4','#225ea8']  
    colours=['#a1dab4','#41b6c4','#2c7fb8','#253494']  
    return colours[i]

def mono(i):
    colours=['green','Grey','Black','LightBlue','DodgerBlue','Navy','Red','Orange','Green']
    return colours[i]

def colour_1():
    return "darkorange"

def pick_colour(item,all_items):
    all_items=sort_redshifts(all_items)
    i=np.where(all_items==item)[0][0]
    if len(all_items)<=4:
        return ygb_small(i)
    else:
        return ygb_big(i)

def sort_redshifts(redshifts):
    return np.sort(redshifts)

class EmptyException(Exception):
    pass

def _separator():
    print("##########################################")
    return

def _separator_2():
    print("::::::::::::::::::::::::::::::::::::::::::")
  
def boxsize(AGN=True):
    ds1=load(1,AGN=AGN)
    box=ds1.unit_l/ds1.aexp/Mpc
    return box

#Read output pairs
def get_outputs(DM=True):
    foldername="./AGN_VS_NOAGN_MASSES/"
    files=os.listdir(foldername)

    if DM:
        filename="halo2halo"
    else:
        filename="galaxy2galaxy"

    out_AGN=[]
    out_noAGN=[]

    for file in files:
        if file[0:len(filename)]==filename:
            if not file[len(filename):len(filename)+4]=="_agn":
                out_AGN.append(int(file[-11:-8]))
                out_noAGN.append(int(file[-7:-4]))

    return np.sort(out_AGN),np.sort(out_noAGN)   #They should both be chronological, so this should work

#Find the corresponding noAGN output number
def pairup_output(output,DM,AGN=False):
    AGNoutputs,noAGNoutputs=get_outputs(DM)
    if AGN:
        return output
    else:
        try:
            assert output in AGNoutputs
            return noAGNoutputs[AGNoutputs==output][0]
        except:
            raise EmptyException("There is no pair for these outputs, pick one of {0}".format(AGN))

#Find the filename for a pair
def pair_name(out_AGN,DM=True,r10=False):
    if DM:
        foldername='./DM_TWINS'
    else:
        foldername='./STAR_TWINS'
        if r10:
            foldername+='_R10'
        else:
            foldername+='_R05'

    filename=foldername+"/twins_{0}_{1}.csv".format(str(out_AGN).zfill(3),str(pairup_output(out_AGN,DM=DM)).zfill(3))
    print("Opening file",filename)
    return filename

#Find the correct folder
def foldername(AGN=True,DM=True):
    if AGN:
        filename="./AGN"
    else:
        filename="./noAGN"

    if DM:
        filename+="_DM"
    else:
        filename+="_STARS"

    return filename

#Find the name of the file
def infoname(AGN=True,DM=False):
    if AGN:
        filename="./AGN"
    else:
        filename="./noAGN"
    if DM:
        filename="./DM"
    filename+="_info"
    return filename

#############################################################################  
#GMF observational data

def overplot_observations(ax,output):
    folder='./Datasets/GMF_observations/'
    files=os.listdir(folder)
    relevant_files=[file for file in files if file[:3]=='MF_' and file[-4:]=='.dat']
    redshift=np.round(load(output).z,1)
    masses=[];lower=[];upper=[]
    for file in relevant_files:
        for z in [redshift-0.15,redshift,redshift+0.15]:
            tmp_masses,tmp_lower,tmp_upper=read_source(name=file,folder=folder,redshift=z)   #,ax=ax)
            for dex in [-0.25,0,0.25]:
                masses.extend(list(np.array(tmp_masses)+dex))
                lower.extend(tmp_lower)
                upper.extend(tmp_upper)
        
    #Make mass bins
    nbins = min([len(masses)-3, 22])
    bins = np.linspace(min(masses),max(masses)+2,nbins)
    indices = np.digitize(masses,bins)

    #Bin the actual data
    lower = np.array(lower)
    upper = np.array(upper)
    masses = np.array(masses)
    lower_binned = [lower[indices==i].min() for i in range(1,nbins) if i in indices]
    upper_binned = [upper[indices==i].max() for i in range(1,nbins) if i in indices]

    #To distort data as little as possible, take the middle of the bin to be
    #the mean of all data points in that bin
    plotbins = []
    for i in range(1,nbins):
        if i in indices:
            plotbins.append(masses[indices==i].mean())

    ax.fill_between(plotbins,lower_binned,upper_binned,color='#2166ac',alpha=alpha_region,linewidth=0)
    return ax

def read_source(name,folder,redshift,ax=None):
    '''Read each file in observational_data, and return data covering the given redshift'''
    mass=[]; phi=[]; upper_error=[]; lower_error=[]
    with open(folder+name) as f:
        print("Opening",name)
        reader=csv.reader(f)
        next(reader)
        next(reader)
        next(reader)
        raw_data=[row[0].split() for row in reader]

    for line in raw_data:
        if redshift>=float(line[0]) and redshift<float(line[1]):
            mass.append(float(line[2]))
            phi.append(float(line[3]))
            lower_error.append(float(line[4]))
            upper_error.append(float(line[5]))

    if mass:
        #To overplot data quickly, for visual inspection
        if ax:
            ax.errorbar(mass,phi,[np.array(lower_error),np.array(upper_error) ],label=name,color='grey',alpha=alpha,linewidth=1)
    return mass, np.array(phi)-np.array(lower_error),np.array(phi)+np.array(upper_error) 


#############################################################################  
#Data objects

class Pair(object):
    def __init__(self,outAGN,DM=False,add_halos=True,r10=False):
        self.AGN=Simulation_Pair(outAGN,AGN=True)
        self.noAGN=Simulation_Pair(pairup_output(outAGN,DM=False),AGN=False)
        self.DM=DM
        self.r10=r10     #USES 10% virial radius, from Elisa

        filename=pair_name(self.AGN.output,DM=self.DM,r10=r10)
        print("Opening",filename)
        with open(filename) as f:
            reader=csv.reader(f)
            next(reader)
            lines=[row for row in reader]
        self.twins=np.array([Twin(line,dsAGN=self.AGN.ds,dsnoAGN=self.noAGN.ds) for line in lines])# if 0.1<float(line[2])<10.0 ])
        print("Twins",len(self.twins))
        if add_halos:
            self.add_halos_to_twins()
            if not self.DM:
                self.add_hosts(AGN=True)
                self.add_hosts(AGN=False)
        print("Twins with halos",len(self.twins))

    def get_ratios(self,name,other_name=None,NO=False):
        data=self.get_data(name,other_name=other_name)/self.get_data('no{0}'.format(name),other_name='no{0}'.format(other_name))
        return data

    def get_data(self,name,other_name=None):
        try:
            data=self.data_twins
        except:
            data=self.get_twins(name,other_name=other_name)
            
        split_name=name.split('.')
        if len(split_name)==0:
            print("That is not a valid name:",name)
            exit

        try:
            for component in split_name:
                data=np.array([getattr(twin,component) for twin in data])
        except:
            data=self.get_twins(name,other_name=other_name)
            for component in split_name:
                data=np.array([getattr(twin,component) for twin in data])
        return data

    def get_twins(self,name,other_name=None):
        split_name=name.split('.')[1:]        
        galaxy=split_name[-1][:3]=='GAL'
        flows=('R20' in split_name) or ('R95' in split_name)
        if not galaxy and other_name:
            galaxy=other_name.split('.')[-1][:3]=='GAL'
        if 'sink' in split_name:            #Plot the mass of the central BH
            twins=self.get_sink_twins(accretion=True,galaxy=galaxy)
        elif any(['Z' in component for component in split_name]):
            twins=self.get_metal_twins(name=split_name[-2])
        elif 'mark' in split_name:
            twins=self.get_marks_twins(noAGN=True,galaxy=split_name[-1][:3]=='GAL',flows=flows)
        else:
            twins=self.twins
        self.data_twins=twins
        return twins

    def add_halos_to_twins(self):
        for simulation in ['AGN','noAGN']:
            halos=get_output_halos(getattr(self,simulation).output,AGN=(simulation=='AGN'),DM=self.DM,pair=self)
            halo_twins=[]
            for halo,twin in zip(halos,self.twins):          #This assumed they are in the correct order!!!
                getattr(twin,simulation).__add_halo__(halo)
                if not self.DM:
                    getattr(twin,simulation).halo.add_host_id(getattr(twin,simulation).host_id)
                halo_twins.append(twin)
            self.twins=halo_twins

    def apply_selection_cut(self,AGN=True):
        self.selection_cut=10**find_masscuts(self,fixed=False)[1]
        cut_twins=[twin for twin in  self.twins if getattr(twin,simulation_type(AGN)).halo.mvir > self.selection_cut]
        print("Excluding",len(self.twins)-len(cut_twins),"due to the selection cut at H-{0}".format(simulation_type(AGN)),self.selection_cut,"Msun")
        self.twins=cut_twins

    def add_hosts(self,AGN):
        DM_halos=np.array(get_output_halos(output=getattr(self,simulation_type(AGN)).output,AGN=AGN,DM=True,pair=self))
        #assumes host and halos are in the same order!
        for halo,twin in zip(DM_halos,self.twins):
            getattr(twin,simulation_type(AGN)).halo.add_host(halo)

    def add_marks_halos(self,AGN=True,flows=True,BH=True,local=False,galaxy=False):
        output_forfile=getattr(self,simulation_type(AGN)).output_forfile    #output number as it appears in file names
        if not hasattr(getattr(self.twins[0],simulation_type(AGN)).halo,"host"):
            self.add_hosts(AGN=AGN)

        foldername="./Datasets/MARK/{0}".format(simulation_type(AGN))
        if local:
            foldername+="_10000"
        filename=foldername+"/HaloMasses_{0}.csv".format(output_forfile)
        print("Opening file",filename           )
        with open(filename) as f:                     
            reader=csv.reader(f)
            next(reader)
            halo_lines=np.array([row for row in reader])                #Read the Halos from file
            halo_ids=np.array([int(line[0]) for line in halo_lines])    #Extract ids only for sorting

        if AGN and BH:         #Read the BH information as well
            filename=foldername+"/BigBH_{0}.csv".format(output_forfile)
            print("Opening file",filename)
            with open(filename) as f:
                reader=csv.reader(f)
                next(reader)
                sink_lines=np.array([row for row in reader])
                
        if flows:
            for radius in ['R20','R95']:
                filename=foldername+"/HaloMassFlux{0}_{1}.csv".format(radius,output_forfile)
                print("Opening", filename)
                with open(filename) as f:
                    reader=csv.reader(f)
                    next(reader)
                    if radius=='R20':
                        flow_linesR20=np.array([row for row in reader])
                    else:
                        flow_linesR95=np.array([row for row in reader])

        if galaxy:
            filename=foldername+"/GalaxyMasses_{0}.csv".format(output_forfile)
            print("Opening",filename)
            with open(filename) as f:
                reader=csv.reader(f)
                next(reader)
                galaxy_lines=np.array([row for row in reader])

        simulation=simulation_type(AGN)
        twins=[getattr(twin,simulation) for twin in self.twins if getattr(twin,simulation).halo.n500]  #select only twins with big host halos
        indices=[np.where(halo_ids==twin.host_id)[0][0] for twin in twins]
        [twin.__add_marks_halo__(halo_lines[i],i+3) for twin,i in zip(twins,indices)]
        if AGN and BH:
            [twin.mark.add_sink(sink_lines[i],twin.halo.mvir) for twin,i in zip(twins,indices)]
        if flows:
            [twin.mark.add_flows(flow_linesR20[i],flow_linesR95[i]) for twin,i in zip(twins,indices)]
        if galaxy:
            [twin.mark.add_galaxy(galaxy_lines[i]) for twin,i in zip(twins,indices)]

            

    def get_marks_twins(self,noAGN=False,flows=False,local=False,galaxy=False):      #always loads AGN halos, might also add noAGN halos
        self.add_marks_halos(AGN=True,flows=flows,local=local,galaxy=galaxy)
        twins=[twin for twin in self.twins if hasattr(twin.AGN,'mark')]
        if noAGN:
            self.add_marks_halos(AGN=False,flows=flows,local=local,galaxy=galaxy)
            twins=[twin for twin in twins if hasattr(twin.noAGN,'mark')]
        print("There are",len(twins),"out of",len(self.twins),"left")
        return twins


    def get_sink_twins(self,flows=False,local=False,accretion=False,galaxy=False):
        noAGN=True
        marks_twins=self.get_marks_twins(local=local,noAGN=noAGN,flows=flows,galaxy=galaxy)
        sink_twins=np.array([twin for twin in marks_twins if twin.AGN.mark.sink.id!=0])
        if accretion:
            filename="./SINKS/sinks_{0}.txt".format(str(self.AGN.output).zfill(5))
            print("Opening",filename)
            with open(filename) as f:
                reader=csv.reader(f)
                next(reader)
                BHs=np.array([row[0].split() for row in reader])
                BH_ids=np.array([int(BH[0]) for BH in BHs])
                
            for twin in sink_twins:
                i=np.where(BH_ids==twin.AGN.mark.sink.id)[0][0]
                twin.AGN.mark.sink.add_accretion(BHs[i])

            sink_twins=np.array([twin for twin in sink_twins if twin.AGN.mark.sink.eddington_ratio>0])
        return sink_twins

    def get_total_halo_mass(self,AGN,mh=False):
        if mh:
            print("Adding the halo mass mh")
            mass='mh'
        else:
            print("Adding the virial mass mvir")
            mass='mvir'

            #Function to reload existing data
        if hasattr(getattr(self.twins[0],simulation_type(AGN)).halo,"sub_{0}".format(mass)):
            return np.array([getattr(twin,simulation_type(AGN)).halo.total_mass(mh=mh) for twin in self.twins])

        subhalos=get_output_halos(output=getattr(self,simulation_type(AGN)).output,DM=self.DM,AGN=AGN,subhalos=True)
        subhalo_parent=np.array([subhalo.parent for subhalo in subhalos])
        subhalo_mass=np.array([getattr(subhalo,mass) for subhalo in subhalos])

        halo_ids=np.array([getattr(twin,simulation_type(AGN)).id for twin in self.twins])
        halo_mass=np.array([getattr(getattr(twin,simulation_type(AGN)).halo,mass) for twin in self.twins])

        total_subhalo_mass=np.array([sum(subhalo_mass[subhalo_parent==id]) for id in halo_ids])
        total_mass=total_subhalo_mass+halo_mass
        for i in range(len(self.twins)):
            getattr(self.twins[i],simulation_type(AGN)).halo.add_subhalo_mass(mass=total_subhalo_mass[i],mh=mh)
        return total_mass

    def get_metal_twins(self,name):
        try:
            self.data_twins[0].AGN.mark
            twins=self.data_twins
        except:
            twins=self.get_marks_twins(noAGN=True)

        if len(name.split('.'))>1:
            name=name.split('.')[-2]
        try:
            getattr(self.data_twins[0].AGN.mark,name)
            return twins
        except:
            twins=[twin for twin in twins if getattr(twin,'AGN').halo.npart>200 and getattr(twin,'noAGN').halo.npart>200]
            print("There are",len(twins),"large enough galaxies in the sample" )
            for combination in ['AGN','noAGN','AGN_noAGN']:
                simulation=combination.split('_')[0]
                print(simulation)
                output_forfile=getattr(self,simulation).output_forfile
                foldername="./Datasets/MARK/{0}".format(combination)
                if name[1:5]=='star':
                    filename=foldername+"/VirialStars_{0}.csv".format(output_forfile)
                elif name[1:4]=='gas':
                    filename=foldername+"/VirialMetals_{0}.csv".format(output_forfile)
                else:
                    filename=foldername+"/OverMetals_{0}_{1}.csv".format(name[1:],output_forfile)
                print("Opening",filename)
                with open(filename) as f:
                    reader=csv.reader(f)
                    next(reader)
                    metal_lines=np.array([row for row in reader])
                    metal_ids=np.array([int(row[0]) for row in metal_lines])
            
                print("Galaxies in file",len(metal_ids))
                if combination=='AGN_noAGN':
                    name=name+'NO'
                indices=[np.where(metal_ids==getattr(twin,simulation).id)[0][0] for twin in twins]
                [getattr(twin,simulation).mark.add_metals(metal_lines[i],name=name) for twin,i in zip(twins,indices)]

            print(len(twins)," twins left after adding metals")
            self.data_twins=twins
            return twins

class Twin(object):
    def __init__(self,line,dsAGN,dsnoAGN):
        if len(line)>2:
        #Galaxies also have host ids
            self.AGN=Simulation_Twin(object_id=int(line[0]),simulation='AGN',host_id=int(line[2]),ds=dsAGN)
            self.noAGN=Simulation_Twin(object_id=int(line[1]),simulation='noAGN',host_id=int(line[3]),ds=dsnoAGN)
        else:
        #DM halos do not
            self.AGN=Simulation_Twin(object_id=int(line[0]),simulation='AGN',ds=dsAGN)
            self.noAGN=Simulation_Twin(object_id=int(line[1]),simulation='noAGN',ds=dsnoAGN)

class Sink(object):
    def __init__(self,line,parent):
        self.id=int(float(line[0]))
        self.mass=float(line[1])
        self.separation=float(line[2])
        self.line=line
        self.all_ids=[int(line[3*i]) for i in range(5)]
        self.parent=weakref.proxy(parent)

    def in_halo(self,id):
        return id in self.all_ids

    def add_accretion(self,line):
        self.age=-1*float(line[8])     #yr
        self.accretion=float(line[9])     #Msun/yr
        self.bondi=float(line[10])        #Msun/yr
        self.eddington=float(line[11])    #Msun/yr
        if self.eddington==0:
            self.eddington_ratio=0.
        else:
            self.eddington_ratio=self.accretion/self.eddington     #Msun/yr
        self.eddington_mbh=self.eddington_ratio/self.mass
        if self.eddington_ratio > 0.01:
            self.feedback_efficiency=0.15
        else:
            self.feedback_efficiency=1.0
        self.AGN_power=self.feedback_efficiency*0.1*self.accretion #Units of [MSun* c^2 / yr]
        self.AGNpowerMdm=self.AGN_power/self.parent.Mdm

        self.mass2=self.mass**2

class Halo(object):
    def __init__(self,line,DM,npart_load=0):
        self.id=int(float(line[0]))
        self.pos=np.array([float(line[i]) for i in range(1,4)])
        self.rvir=float(line[4])   #New files: in kpc, old in code units
        self.mvir=float(line[5])*1E11    # in Msun
        if DM:  #These two columns are consistently swapped
            self.npart=int(float(line[7]))
            self.mh=float(line[6])*1E11 # in Msun
        else:
            self.npart=int(float(line[6]))
            self.mh=float(line[7])*1E11 # in Msun
        self.DM=DM
        self.line=line
        self.npart_load=npart_load
        
    def add_host_id(self,host_id):
        self.host_id=host_id

    def add_host(self,host):
        self.host=host
        self.n500=host.npart>500
        return self

    def add_subhalo_mass(self,mass,mh):
        if mh:
            self.sub_mh=mass
        else:
            self.sub_mvir=mass

    def total_mass(self,mh):
        if mh:
            return self.mh+self.sub_mh
        else:
            return self.mvir+self.sub_mvir

class Subhalo(Halo):
    def __init__(self,line,DM):
        parent=int(float(line[1]))
        line.remove(line[1])
        Halo.__init__(self,line,DM)
        self.parent=parent

class Halo_Mark(object):
    def __init__(self,line,order,parent=None):

        if parent:
            self.parent=weakref.proxy(parent)
        else:
            self.parent=None

        self.order=order     #row in file with this object (counting from 3!!)

        self.id=int(float(line[0]))
        self.pos=np.array([float(line[i]) for i in range(1,4)])
        self.r=float(line[4])
        if parent:
            self.r=self.r*parent.ds.kpc   #in code units
        self.Mgas=float(line[5])
        self.Mstar=float(line[6])
        self.Mbh=float(line[7])
        self.Mdm=float(line[8])
        self.Nbh=int(line[10])
        if len(line)>11:
            self.sfr=float(line[11])+1E-11  #To avoid issues with 0.0
        self.Mbaryon=self.Mgas+self.Mstar+self.Mbh
        self.Mall=self.Mbaryon+self.Mdm
        self.Mbaryon_vol=self.Mbaryon/(self.r**3)
        if (self.Mgas+self.Mstar)>0:
            self.f_gas=self.Mgas/(self.Mgas+self.Mstar)
        else:
            self.f_gas=np.nan
        self.rho_gas=self.Mgas/(4*np.pi*self.r**3 /3)


    def add_sink(self,line):
        #while mstar<float(line[1])*80:
        #    line=list(line[3:])+[0,0.0,0.0]

        #This only adds the first sink at the minute!!
        self.sink=Sink(line,self)
        if self.parent:
            self.MbhMdm=self.sink.mass/self.Mdm
  
    def add_flows(self,lineR20,lineR95):
        self.R20=RadiusFlows(lineR20)
        self.R95=RadiusFlows(lineR95)
        #self.sfr=self.R20.sfr
    
    def add_galaxy(self,line):
        self.GALstar=float(line[1])
        self.GALgas=float(line[2])
        self.GALbh=float(line[3])
        self.GALbaryon=self.GALstar+self.GALgas#+self.GALbh
        self.GALf_gas=np.nan
        self.GALf_star=np.nan
        self.GALsfe=np.nan
        if self.GALbaryon>0:
            self.GALf_gas=self.GALgas/self.GALbaryon
            self.GALf_star=self.GALstar/self.GALbaryon
            if hasattr(self,'sfr') and self.GALgas>0:
                self.GALsfe=self.sfr/self.GALgas*1E9   #Matching Schreiber2016, SFR/Mgas [1/Gyr]              
        self.GALrho_gas=self.GALgas/self.r**3   # r is in code units


    def add_metals(self,line,name):
        if name[1:5]=='star':
            setattr(self,name,StellarMetallicity(line))
        else:
            setattr(self,name,GasMetallicity(line))

    def add_profiles(self,simulation=None,output=None):
        if not simulation:
            simulation=self.parent.simulation
        if not output:
            output=self.parent.ds.output
        
        filepath='./Datasets/MARK/{0}/profiles/'.format(simulation)
        filename='All_RadialProfile_{1}_H{0}.csv'.format(self.order,str(output).zfill(5))
        with open(filepath+filename) as f:
            reader=csv.reader(f)
            keys=next(reader)
            lines=[row for row in reader]
            lines=np.array(lines)
            print("Number of data points per profile",np.shape(lines)[0])

            data_dict={}
            for i,key in enumerate(keys):
                data_dict[key.strip()]=np.array([float(value) for value in lines[:,i]])

        self.profiles=data_dict
        return

class GasMetallicity(object):
    def __init__(self,line):
        self.id=int(line[0])

        if len(line)>6:    #OverMetals file
            self.Zave=float(line[2])    #in units of overmetallicity!
            self.Ztot=float(line[3])    #in units of metallicity!
            self.ZcellMAX=float(line[4])   # in units of overmetallicity
            self.r_grid=float(line[5])
            self.r_kpc=float(line[6])
        else:              #VirialMetals file
            self.Zave=float(line[1])    #in units of overmetallicity!
            self.Ztot=float(line[2])    #in units of metallicity!
            self.ZcellMAX=float(line[3])   # in units of overmetallicity
            self.rvir_grid=float(line[4])
            self.rvir_kpc=float(line[5])

        if self.ZcellMAX==0:   #leftover radii from higher threshold
            self.rvir_grid=0.0
            self.rvir_kpc=0.0

class StellarMetallicity(object):
    def __init__(self,line):
        self.id=int(line[0])
        self.mstar=float(line[1])
        self.Ztot=float(line[2]) #in units of metallicity!
        self.sfr1=float(line[3])
        self.sfr2=float(line[4])
        [setattr(self,spec,float(value)) for spec,value in zip(['H','O','Fe','C','N','Mg','S'],line[5:])]

class Simulation_Twin(object):
    def __init__(self,object_id,simulation,host_id=None,ds=None):
        self.id=object_id
        self.simulation=simulation
        if host_id:
            self.host_id=host_id
        if ds:
            self.ds=ds

    def __add_halo__(self,halo,host_id=None):
        self.halo=halo

    def __add_marks_halo__(self,line,order):
        self.mark=Halo_Mark(line,order=order,parent=self)


class Simulation_Pair(object):
    def __init__(self,output,AGN):
        self.output=output
        self.output_forfile=str(self.output).zfill(3)                            
        self.ds=load(output,AGN=AGN,DM=False)

        if AGN:
            floors={761:7.655265E-04,343:5.244030E-04,197: 2.959E-004,125:1.310583E-04}
        else:
            floors={299:9.782093E-04,145:7.160897E-04,99:3.830396E-04,69:1.596806E-04}
        if output in floors.keys():
            self.Zbox=floors[output]

class RadiusFlows(object):    #Collects all information about various flows at a particular radius
    def __init__(self,line):
        self.sfr=float(line[1])
        self.all_species=Flow(line[2:4])
        self.gas=Flow(line[4:6])
        self.cold=Flow(line[6:8])
        self.hot=Flow(line[8:10])
        self.z=Flow(line[10:12])
        self.star=Flow(line[12:14])
        self.bh=Flow(line[14:16])
        self.dm=Flow(line[16:18])
        self.baryon=Flow([self.gas.inflow+self.star.inflow+self.bh.inflow,self.gas.outflow+self.star.outflow+self.bh.outflow])

class Flow(object):
    def __init__(self,flows):
        self.inflow=float(flows[0])
        self.outflow=float(flows[1])
        self.net_inflow=self.inflow-self.outflow

class Dataset(object):
    def __init__(self,data,AGN,DM,output):
        self.output=output
        self.AGN=AGN
        self.DM=DM

        self.ncpu=int(data[0][2])
        self.ndim=int(data[1][2])
        self.levelmin=int(data[2][2])
        self.levelmax=int(data[3][2])
        self.ngridmax=int(data[4][2])
        self.nstep_coarse=int(data[5][1])

        self.boxlen=float(data[7][2])
        self.time=float(data[8][2])
        self.aexp=float(data[9][2])
        self.H0=float(data[10][2])
        self.omega_m=float(data[11][2])
        self.omega_l=float(data[12][2])
        self.omega_k=float(data[13][2])
        self.omega_b=float(data[14][2])
        self.unit_l=float(data[15][2])
        self.unit_d=float(data[16][2])
        self.unit_t=float(data[17][2])
        self.z=1/self.aexp-1

        self.pc=self.unit_l/3.0864E18
        self.kpc=self.pc/1E3
        self.Mpc=self.kpc/1E3
        self.Mpc_h=self.Mpc*self.H0/100

       
class Units(object):
    def __init__(self):
        self.pc=3.086E18
        self.kpc=3.086E21
        self.Mpc=3.086E24
        self.Msun=1.988E33
        self.year=3.154E7
        self.Gyr=3.154E16
