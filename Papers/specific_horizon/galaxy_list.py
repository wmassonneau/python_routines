import horizon

output=761

#pair=horizon.Pair(output)
#pair.add_hosts(AGN=True)
#filename="twinned_galaxies_{0}.csv".format(str(output).zfill(3))
#print "Opening",filename
#with open(filename,'wb') as f:
#    writer=horizon.csv.writer(f)
#    writer.writerow(['id','x','y','z','rvir','Mdm','Mstar'])
#    for twin in pair.twins:
#        writer.writerow([twin.AGN.halo.id]+list(twin.AGN.halo.pos)+[twin.AGN.halo.host.rvir]+[twin.AGN.halo.host.mvir]+[twin.AGN.halo.mvir])

halo_list=horizon.get_output_halos(output,AGN=True,DM=True)
halos={}
for halo in halo_list:
    halos[halo.id]=halo

#get all galaxies
galaxies=horizon.get_output_halos(output,AGN=True,DM=False)
filename="all_galaxies_{0}.csv".format(str(output).zfill(3))
print "Opening",filename
with open(filename,'wb') as f:
    writer=horizon.csv.writer(f)
    writer.writerow(['id','x','y','z','rvir','Mstar'])
    for galaxy in galaxies:
        writer.writerow([galaxy.id]+list(galaxy.pos)+[galaxy.rvir,galaxy.mvir])

#Also print halo information
galaxy_list=galaxies
galaxies={}
for galaxy in galaxy_list:
    galaxies[galaxy.id]=galaxy

filename="./AGN_VS_NOAGN/"+'halo2galaxy_AGN_{0}.asc'.format(str(output).zfill(3))
with open(filename) as f:
    reader=horizon.csv.reader(f,delimiter='\t')
    next(reader)
    next(reader)
    rows=[row for row in reader]
    halo_ids=[int(row[0]) for row in rows]
    galaxy_ids=[int(row[1]) for row in rows]

final_galaxies=horizon.np.array([galaxies[gal_id].add_host(halos[halo_id]) for gal_id,halo_id in zip(galaxy_ids,halo_ids)])
print "There are",len(final_galaxies),"final galaxies to go into the file"

filename='galaxies_with_halos_{0}.csv'.format(str(output).zfill(3))
print "Opening",filename
with open(filename,'wb') as f:
    writer=horizon.csv.writer(f)
    writer.writerow(['Galaxy id','Halo id','x','y','z','rvir star','Mstar','Mdm','rvir DM'])
    for galaxy in final_galaxies:
        writer.writerow([galaxy.id,galaxy.host.id]+list(galaxy.pos)+[galaxy.rvir,galaxy.mvir,galaxy.host.mvir,galaxy.host.rvir])
