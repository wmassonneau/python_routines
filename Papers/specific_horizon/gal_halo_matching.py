import horizon
import matplotlib.pyplot as plt
import numpy as np
import matplotlib as mpl

output=125

def make_histogram(masses,nbins=20,min_bin=2E8):
    sorted_masses=sorted(masses)
    min_bin=np.log10(min_bin)
    bins=np.logspace(max(min_bin,np.log10(masses.min())),np.log10(sorted_masses[-10]+1),nbins)
    print bins[0],bins[-1]
    print bins
    counts,bins=np.histogram(masses,bins)
    print counts
    print "Total number",sum(counts)
    plot_bins=(bins[1:]+bins[:-1])/2
    return counts,plot_bins


##########################
mpl.rcParams['lines.linewidth']=2
fig,(ax1,ax2,ax3)=plt.subplots(3,1,sharex=True)

for r10,colour in zip([False,True],['#43a2ca','#2ca25f']):
    print "USING",r10
    if r10:
        label='0.1rvir'
    else:
        label='0.05rvir'
    pair=horizon.Pair(output,r10=r10,DM=False)
    pair.get_data('AGN.mark.Mdm')  #Ensuring number of twins is reduced to ones in sufficiently massive DM halos only
    print len(pair.get_data('AGN.mark.Mdm'))
    counts,bins=make_histogram(pair.get_data('AGN.halo.mvir'),min_bin=2E8)
    ax1.plot(bins,counts,label=label,color=colour,linestyle='-')

    ax2=horizon.binned_field_mass(x='AGN.halo.mvir',y='AGN.halo.host.mvir',ax=ax2,set_colour=colour,quartile=True,redshifts=[3],r10=r10,saveit=False,xlabel=r"M_* [Msun]",ylabel=r"M_{DM} [Msun]",preloaded_pair=pair)
    ax3=horizon.binned_field_mass(x='AGN.halo.mvir',y='AGN.mark.sink.mass',ax=ax3,set_colour=colour,quartile=True,redshifts=[3],r10=r10,saveit=False,xlabel=r"M_* [Msun]",ylabel=r"M_{SMBH} [Msun]",preloaded_pair=pair)

ax1.set_ylabel("Number")
ax2.set_ylabel("M$_{DM}$ [M$_\odot$]")
ax3.set_ylabel("M$_{SMBH}$ [M$_\odot$]")
ax3.set_xlabel(r"M$_*$ [M$_\odot$]")
ax1.legend(frameon=False)
ax3.set_ylim(1E6,1E9)
#ax1.set_yscale('log')

fig.set_size_inches(6,9)
plt.tight_layout()

figname="sample_rvir_{0}.pdf".format(output)
fig.savefig(figname)
