import numpy as np
import myutils
import csv
import os
import sinks
import time
import matplotlib.pyplot as plt
from astropy.cosmology import WMAP9 as cosmo
import matplotlib.lines as mlines
from mpl_toolkits.mplot3d import Axes3D
from horizon import Halo
from yt.fields.api import ValidateParameter

#################
#Plotting functions

def profile_lineplot(x='Mstar',y='Msink',outputs=None,folder='.',addendum=None,fit_redshifts=None,extrapolate=False,annotate_outputs=None,annotate_colours=None,fit_colours=None,profiles=None,extrapolate_fits=False):
    fig,ax=plt.subplots()


    labels={'Mstar':r'M_*','Msink':r'M_{\rm BH}','Mgas':r'M_{\rm gas}'}

    try:
        #If profiles are given
        outputs=np.array([profile.output for profile in profiles])
    except:
        #If outputs are given
        try:
            profiles=[Profile(output=output,folder=folder) for output in outputs]
            outputs=np.array(outputs)
        except:
            #Find both
            profiles,outputs=find_existing_profiles(folder=folder)

    #Annotate M-sigma relation from observation
    if fit_redshifts:
        if not fit_colours:
            fit_colours=myutils.get_colours(0,len(fit_redshifts),)
        for z,z_colour in zip(fit_redshifts,fit_colours):
            ax=Mstar_Mbh(ax,z=z,color=z_colour)

    #Plot BH evolution
    x_data=np.array([getattr(profile,x) for profile in profiles])
    y_data=np.array([getattr(profile,y) for profile in profiles])
    times=np.array([profile.time for profile in profiles])
    ax.plot(x_data,y_data,color='k',zorder=10,marker='x',label='from simulation')
    
    #Extrapolate stellar mass growth and BH mass growth
    if extrapolate:
        #x_extra,t=extrapolate_property(profiles,x=x,timerange=1E9)#,timerange=max(z_times))
        #y_extra,t=extrapolate_property(profiles,x=y,timerange=1E9)#max(z_times))

        x_extra=np.logspace(np.log10(x_data[-1]),9.5)
        nextrapolate=-90    #-83
        print(y_data[-1]-y_data[nextrapolate],x_data[-1]-x_data[nextrapolate])
        slope=8E-4/0.016
        #slope=(y_data[-1]-y_data[nextrapolate])/(x_data[-1]-x_data[nextrapolate])
        print(slope)
        y_extra=slope*(x_extra-x_extra[0])+y_data[-1]
        ax.plot(x_extra,y_extra,linestyle=':',color='k',label='extrapolated')
        ax.legend(loc=4,frameon=False)


    z_times=[cosmo.age(z).to('Myr').value for z in fit_redshifts]

    #Overplot markers for extrapolated values at fit_redshifts
    if extrapolate_fits:
        for z_time,z,z_colour in zip(z_times,fit_redshifts,fit_colours):
            x_z=x_extra[find_time_index(z_time,t)]
            y_z=y_extra[find_time_index(z_time,t)]
            #print(z_time,t,find_time_index(z_time,t))
            #print(z,x_z,y_z,t[find_time_index(z_time,t)])
            ax.scatter(x_z,y_z,color=z_colour,marker='o',zorder=10,edgecolor='k')
    
    if fit_redshifts:
        if extrapolate_fits:
            marker='o'
        else:
            marker=''
        redshift_lines=[]
        for z_time,z,z_colour in zip(z_times,fit_redshifts,fit_colours):
            redshift_lines.append(mlines.Line2D([],[],label='z={0}'.format(z),color=z_colour,marker=marker,linestyle='--',markeredgecolor='k'))
        fitted_legend=ax.legend(handles=redshift_lines,frameon=False,loc=2)
        ax.add_artist(fitted_legend)

    #Overplot annotate_times
    if annotate_outputs:
        if not(annotate_colours):
            annotate_colours=myutils.get_colours()[:len(annotate_outputs)]
        annotated_lines=[]
        for output,t_colour in zip(annotate_outputs,annotate_colours):
            ax.scatter(x_data[outputs==output],y_data[outputs==output],marker='x',color=t_colour,zorder=20,label='z = {0}'.format(round(myutils.load(output,folder=folder).current_redshift)))

        ax.legend(loc=2)
            
    #print(x_extra,y_extra)
    #ax.plot(x_extra,y_extra,color='k',linestyle=':',zorder=20)

    #Setup plot
    ax.set_xscale('log')
    ax.set_yscale('log')

    ax.set_xlabel(r"$M_*$  [$\rm M_\odot$]")
    ax.set_ylabel(r"$M_{\rm BH}$  [$\rm M_\odot$]")

    ax.set_xlim(1E4,1E9)
    ylims=ax.get_ylim()
    ax.set_ylim(1E2,1E9)

    filename='{0}-{1}'.format(x,y)
    myutils.saveas(fig,filename,pdf=True,addendum=addendum)

#
def extrapolate_property(profiles,timeaverage=100,x='Mstar',timerange=10):
    #Use average over timeverage to extrapolate x for timerange years based on value in profiles
    times=np.array([float(profile.ds.current_time.in_units('Myr')) for profile in profiles])
    x_data=np.array([getattr(profile,x) for profile in profiles])

    t_100Myr=find_time_index(times[:-1],times[-1]-timeaverage)  #Find index for time closest to max(time)-timerange Myr 
    dotx=(x_data[-1]-x_data[t_100Myr])/(times[-1]-times[t_100Myr])   #Calculate slope
    t=np.linspace(times[-1],times[-1]+timerange,100)   #Time array used for extrapolation
    x=dotx*t+x_data[-1]
    return x,t    #Return extrapolated values and times


#Plots 3d path of sink relative to the stellar halo center
def sink3D(outputs=None,folder='.',addendum=None):
    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    if not outputs:
        if haloCoM:
            outputs=myutils.find_outputs(folder)
        else:
            profiles,outputs=find_existing_profiles(folder=folder)

    print("OUTPUTS",outputs)
    all_sinks=[sinks.read(output,folder)[0] for output in outputs]
    pc_scale=np.array([float(sink.ds.length_unit.in_units('pc')) for sink in all_sinks])

    x=np.array([float(sink.pos[0]) for sink in all_sinks])
    y=np.array([float(sink.pos[1]) for sink in all_sinks])
    z=np.array([float(sink.pos[2]) for sink in all_sinks])


    cell_pos=np.array([profile.rhoMAX_loc for profile in profiles])
    print(cell_pos)
    x=x-cell_pos[:,0]
    y=y-cell_pos[:,1]
    z=z-cell_pos[:,2]
        
    print(pc_scale)
    ax.plot(x*pc_scale,y*pc_scale,z*pc_scale)
    
    figname='3D_sink_position'
    myutils.saveas(fig,figname,addendum=addendum)
    
    #plt.show()
    
def plot_profiles(outputs,fields=['Msink','Mstar','Mgas','Mdm'],folder='.',addendum=None,linestyles=None,colours=None,limits=[None,None],sink_center=True):
    fig,ax=plt.subplots()
    
    linewidth=2

    labels={'Mstar':r'$M_*$','Msink':r'$M_{\rm BH}$','Mgas':r'$M_{\rm gas}$','Mdm':r'$M_{\rm DM}$'}

    if not linestyles:
        linestyles=['-','--',':','-.','-','--',':','-.','-','--',':','-.','-','--',':','-.'][:len(fields)]
        
        if not colours:
            colours=myutils.get_colours()[:len(outputs)]
            
    #Load data and plot
    profiles=[Profile(output,folder=folder,sink_center=sink_center) for output in outputs]
    for profile,colour in zip(profiles,colours):
        radii=profile.get_attribute('radius')
        for field,linestyle in zip(fields,linestyles):
            ax.plot(radii,profile.get_attribute(field),color=colour,linestyle=linestyle,linewidth=linewidth)
        
    #Make legend
    field_lines=[mlines.Line2D([],[],color='k',linestyle=linestyle,label=labels[field],linewidth=linewidth) for linestyle,field in zip(linestyles,fields)]
    output_lines=[mlines.Line2D([],[],color=colour,label='z = {0}'.format(round(profile.ds.current_redshift,1)),linewidth=linewidth) for colour,profile in zip(colours,profiles)]

    if len(fields)>1:
        field_legend=ax.legend(handles=field_lines,frameon=False,loc=8)
        ax.add_artist(field_legend)
    else:
        ax.set_ylabel(fields[0])

    output_legend=ax.legend(handles=output_lines,frameon=False,loc=4)
    ax.add_artist(output_legend)
    
    #Set axis scales, limits and labels
    ax.set_yscale('log')
    ax.set_xscale('log')

    ax.set_xlabel('distance from black hole [pc]')
    ax.set_xlim(4E-1,1E3)
    ax.set_ylim(limits[0],limits[1])
    ax.set_ylabel(r'cumulative mass [$\rm M_\odot$]')

    filename='profile_n{0}'.format(len(outputs))
    for field in fields:
        filename+='_{0}'.format(field)
    myutils.saveas(fig,filename,addendum=addendum,pdf=True)
    #plt.show()

def plot_timeseries(outputs=[],fields=['Mgas'],folder='.',radii=[1,10,100,1000],Msink=True,Mstar=True,annotate_outputs=None,annotate_colours=None,addendum=None,SFR=True,profiles=[],Mdm=False):
    #Load profiles or outputs or both
    if len(profiles)>0:
        print("load outputs")
        #If profiles are given  
        outputs=np.array([profile.output for profile in profiles])
    elif len(outputs)>0:
        #If outputs are given  
        print("load profiles")
        profiles=[Profile(output=output,folder=folder) for output in outputs]
        outputs=np.array(outputs)
    else:
        profiles,outputs=find_existing_profiles(folder=folder)

    fig1,(ax1,ax2)=plt.subplots(2,1,gridspec_kw = {'height_ratios':[1,3]},sharex=True,figsize=(7,7.5))

    fig2,(ax3,ax4)=plt.subplots(2,1,gridspec_kw = {'height_ratios':[2,3]},sharex=True,figsize=(7,6))
    axes=[ax1,ax2,ax3,ax4]
    
    colours=myutils.get_colours(0,6)
    linestyles=[':','-.','-','--',':','-.'][:len(fields)]

    #Load data from files
    profile_radii=profiles[0].get_attribute('radius')
    if not radii:
        radii=profile_radii
        radii_indices=range(len(radii))
    else:
        profile_radii=profiles[0].get_attribute('radius')
        radii_indices=[np.where(np.absolute(profile_radii-radius)==np.absolute(profile_radii-radius).min()) for radius in radii]
        radii_indices=[int(radius[0]) for radius in radii_indices]


    if annotate_outputs:
        if not annotate_colours:
            annotate_colours=myutils.get_colours()
        for colour,output in zip(annotate_colours,annotate_outputs):
            ds=myutils.load(output,folder=folder)
            for ax in axes:
                ax.axvline(ds.current_time.in_units('Myr').v,color=colour,linewidth=1)

    ###
    #Plot Bondiplot accretion rate in top plot
    sinkfile=sinks.Sinkfile(folder=folder,create_file=False)
    nsmooth=100

    sinkfile_times=sinks.smooth(sinkfile.getattr('time'),nsmooth)
    ax1.plot(sinkfile_times,sinks.smooth(sinkfile.getattr('dotMedd'),nsmooth),color='grey',linestyle='--',zorder=2,label=r'$\dot{M}_{\rm edd}$')
    ax1.plot(sinkfile_times,sinks.smooth(sinkfile.getattr('dotMacc'),nsmooth),color='k',linestyle='-',zorder=1,label=r'$\dot{M}_{\rm BH}$')

    ####
    #Plot mass timeseries from sink_profile plots in bottom plot

    times=np.array([profile.time for profile in profiles])

    #Empty array to hold data before plotting
    data=np.zeros([len(outputs),len(fields),len(profile_radii)])
    
    #Populate the array
    for i,profile in enumerate(profiles):
        for j,field in enumerate(fields):
            data[i,j,:]=profile.get_attribute(field)
    #print("Times",times)

    #Plot mass timeseries
    r_min=0
    for i,(field,linestyle) in enumerate(zip(fields,linestyles[:len(fields)])):
        for j,color in zip(radii_indices,colours[:len(radii)]):
            if j>r_min:
                ax2.plot(times,data[:,i,j],color=color,linestyle=linestyle)

    #Add stellar and sink mass
    if Mstar or SFR:
        Mstar_data=np.array([profile.Mstar for profile in profiles])
        print(Mstar_data)
        galaxies=Halos(folder=folder,galaxy=True)
        star_line='--'
        star_colour=myutils.get_colours(1,0)
        if Mstar:
            ax2.plot(times,Mstar_data,color=star_colour,linestyle=star_line)
            ax2.plot(times,np.array([profile.Mgas for profile in profiles]),color='k',linestyle=linestyles[0])
        #    ax2.plot(galaxies.times,galaxies.mvir,color='r')
            
    if Msink:
        Msink_data=sinks.smooth(sinkfile.getattr('BHmass'),nsmooth)   #np.array([profile.Msink for profile in profiles])
        sink_line='-'
        sink_colour='k'
        ax2.plot(sinkfile_times,Msink_data,color=sink_colour,linestyle=sink_line)

    DMhalos=Halos(folder,galaxy=False)
    if Mdm:
        DM_line='-.'
        DM_colour=sink_colour
        ax2.plot(DMhalos.times,DMhalos.mh,color=DM_colour,linestyle=DM_line)
        ax2.plot(times,np.array([profile.Mdm for profile in profiles]),color='g')
    #Add SFR to top plot
    sfr_colour=star_colour

        #Use stars younger than 1 Myr from file
    galaxy_profiles,tmp=find_existing_profiles(folder=folder,sink_center=False)
    galaxy_times=[galaxy.time for galaxy in galaxy_profiles]
    ax1.plot(galaxy_times,[galaxy.sfr for galaxy in galaxy_profiles],color=sfr_colour,label='SFR',linestyle=star_line)

    #Plot R* and distance to densest cell on bottom plot
    ax3.plot(times,np.array([profile.Rstar for profile in profiles]),color=sfr_colour,linestyle='--',label=r'$R_*$')
    #ax3.plot(times,np.array([profile.rhoMAX_dist for profile in profiles]),color='k',linestyle=':',label=r'$d_{\rm BH-gas}$')
    ax3.plot(galaxy_times,[galaxy.CoMstar_dist for galaxy in galaxy_profiles],linestyle='-',color='k',label=r'$d_{\rm com}$')
    if Mdm:
        ax3.plot(DMhalos.times,DMhalos.rvir,linestyle=DM_line,color=DM_colour,label=r'$R_{\rm vir}$')   
    #Plot distance between stellar CoM and gas CoM
    distances=np.array([np.linalg.norm(profile.rhoMAX_loc-profile.CoMstar)*float(profile.ds.length_unit.in_units('pc').v) for profile in galaxy_profiles])
    #ax3.plot(galaxy_times,distances,color='grey',linestyle=':',label=r'$d_{gas-star}$')

    
    #Plot sound speed and relative bulk velocity of gas
    for radius,colour in zip([0.1,0.5,3],myutils.get_colours(0,4)):
        ax4.plot(times,[profile.get_attribute_at_radius('v_rel',radius) for profile in profiles],linestyle='-',color=colour,label=r'$v_{{\rm rel}}({0} \ \rm pc)$'.format(round(radius,1)))
    ax4.plot(galaxy_times,[profile.CSave for profile in galaxy_profiles],linestyle='--',label=r'$c_s(3 \ \rm pc)$',color='k')
    ax4.plot(galaxy_times,[profile.CoMgas_vrel_mag for profile in galaxy_profiles],linestyle='-',label=r'$v_{\rm com}$',color='k')

    #Setup legend for mass plot
    field_lines=[]#[mlines.Line2D([],[],color='k',linestyle=linestyle,label=field) for linestyle,field in zip(linestyles,fields)]
    if Mstar:
        field_lines+=[mlines.Line2D([],[],color=star_colour,linestyle=star_line,label=r"$M_*$")]
    if Msink:
        field_lines+=[mlines.Line2D([],[],color=sink_colour,linestyle=sink_line,label=r"$M_{\rm BH}$")]
    if Mdm:
        field_lines+=[mlines.Line2D([],[],color=DM_colour,linestyle=DM_line,label=r"$M_{\rm DM}$")]
    radii_lines=[mlines.Line2D([],[],color=colour,label=r'$M_{{\rm gas}}$({0} pc)'.format(round(radius,2)),linestyle=linestyles[0]) for colour,radius in zip(colours,radii)]
    field_lines+=[mlines.Line2D([],[],color='k',label=r'$M_{\rm gas}(R_*)$',linestyle=linestyles[0])]

    if len(fields)>1 or Mstar or Msink:
        field_legend=ax2.legend(handles=field_lines,frameon=False,loc=4,ncol=1)
        ax2.add_artist(field_legend)
        ax2.set_ylabel(r'[$\rm M_\odot$]') 
    else:
        ax2.set_ylabel(fields[0])
    radii_legend=ax2.legend(handles=radii_lines[::-1],frameon=False,loc=3,ncol=1)
    ax2.add_artist(radii_legend)
    ax2.set_ylim(1E-2,2E8)

    #Legend for accretion rate plot
    ax1.set_ylim(9E-12,9E1)
    ax1.legend(loc=3,ncol=3,frameon=False)

    #Legend for radii plot
    ylims=ax3.get_ylim()
    print(ylims)
    ax3.set_ylim(1E0,2E3)
    ax3.legend(ncol=2,loc=2,frameon=False)

    #Legend for velocity plot
    ax4.legend(loc=2,ncol=3,frameon=False)
    ax4.set_ylabel('[km/s]')
    ax4.set_ylim(5E-2,5E3)

    #Set yscale
    for ax in [ax1,ax2,ax3,ax4]:
        ax.set_yscale('log')

    ax1.set_ylabel(r'[$\rm M_\odot$/yr]')
    ax3.set_ylabel(r'[pc]')
    for ax in [ax2,ax4]:
        ax.set_xlabel('time [Myr]')

    #Add redshift to top axis

    tick_redshifts=range(19,10,-1)
    print(tick_redshifts)
    tick_z_ages=[cosmo.age(z).to('Myr').value for z in tick_redshifts]
    for i,ax in enumerate(axes):
        zax=ax.twiny()
        zax.set_xticks(tick_z_ages)
        xlim=ax.get_xlim()
        zax.set_xlim(xlim[0],xlim[1])
        zax.set_xticklabels(['' for z in tick_redshifts])
        if i==0 or i==2:
            zax.set_xticklabels(['{:g}'.format(z) for z in tick_redshifts])
            zax.set_xlabel('redshift')

    for fig in [fig1,fig2]:
        fig.tight_layout(h_pad=0.1)

    filename1='profile1_timeseries_r{0}'.format(len(radii))
    filename2='profile2_timeseries_r{0}'.format(len(radii))

    myutils.saveas(fig1,filename1,addendum=addendum,pdf=True)
    myutils.saveas(fig2,filename2,addendum=addendum,pdf=True)
    #plt.show()


###############
# Write files
    

def write_halo_files(folder='.',galaxy=False):
    if galaxy:
        filename='yt_galaxies'
    else:
        filename='yt_halos'
    foldername='./'+folder+'/Data/'
    print(foldername)
    outputs=np.array([int(name[len(filename)+1:len(filename)+4]) for name in os.listdir(foldername) if name[:len(filename)]==filename])
    outputs.sort()
    
    file_out=foldername+filename[3:]+'.csv'
    print(file_out)
    with open(file_out,'w') as f:
        writer=csv.writer(f)
        writer.writerow(['output','id','x','y','z','rvir','mvir','mh/npart'])
        for output in outputs:
            ds=myutils.load(output,folder)
            halo=find_halo(output,folder,galaxy=galaxy)
            data=[output,float(ds.current_time.in_units('Myr').v)]+list(halo.line)
            writer.writerow(data)
    return

def write_gas_profile(output,folder='.',radius=1E4,ds=None):
    ''' write density, temperature and rotational velocity profiles to file'''
    lastsave=time.time()

    #Load data
    profile=Profile(output,folder=folder,sink_center=False)
    center=profile.CoMstar
    Lnormal=profile.Lgas_norm
    if not ds:
        ds=profile.sink.ds
    myutils.add_filters(ds)

    #Create data object
    sp=ds.sphere(center,ds.quan(radius,'pc'))
    sp.set_field_parameter('normal',Lnormal)

    #Set up new file
    foldername=folder+"/Data"
    if not os.path.exists(foldername):
        os.makedirs(foldername)
    filename=foldername+'/gas_profiles_{0}.csv'.format(str(output).zfill(5))

    #Make filename
    grid_profile=myutils.yt.create_profile(sp,"radius",
                                           fields=["density","cell_mass",'temperature', ('gas','cylindrical_tangential_velocity'),('cylindrical_radial_velocity')],
                                           weight_field='density',
                                           accumulation=False,
                                           n_bins=200,
                                           logs={'radius':True})

    #part_profile=myutils.yt.create_profile(sp, ["particle_radius"],
    #                                       fields=([
    #            ('stars','particle_mass'), #Stellar mass       
    #            ('DM','particle_mass'),   #DM mass        
    #            ('stars','particle_velocity_cylindrical_theta')
    #            ]),
    #                                       weight_field=None,
    #                                       accumulation=False,
    #                                       n_bins=200)


    with open(filename,'w') as f:
        writer=csv.writer(f)
        writer.writerow(['density [Hcc]','T [K]','vROTgas [km/s]','vROTstar [km/s]','Mgas [Msun]','Mstar [Msun]','Mdm [Msun]'])
        
        for data in [
                grid_profile.x.in_units('pc').v,
                grid_profile['density'].in_units('amu/cm**3').v,
                grid_profile['temperature'].in_units('K').v,
                grid_profile[('gas','cylindrical_tangential_velocity')].in_units('km/s').v,
                grid_profile[('gas','cylindrical_radial_velocity')].in_units('km/s').v
                #part_profile[('stars','cylindrical_tangential_velocity')].in_units('km/s').v,
                #part_profile[('cell_mass')].in_units('Msun').v,
                #part_profile[ ('stars','particle_mass')].in_units('Msun').v,
                #part_profile[ ('DM','particle_mass')].in_units('Msun').v
                ]:
            writer.writerow(data)
        print(grid_profile[('gas','cylindrical_tangential_velocity')].in_units('km/s').v)

        print("Saving as",filename)

    return

def write_manual_profile(output,folder='.',sink_center=True,sink=None):
    lastsave=time.time()
    
    if not sink:
        sink=sinks.read(output,folder=folder)[0]
    ds=sink.ds
    if not(sink_center):
        rho_loc=Profile(output,folder=folder,sink_center=True).rhoMAX_loc
        
    myutils.add_filters(ds)
        
    foldername=folder+"/Data"
    if not os.path.exists(foldername):
        os.makedirs(foldername)

    if sink_center:
        filename=foldername+'/profile_sink_{0}.csv'.format(str(output).zfill(5))
    else:
        filename=foldername+'/profile_galaxy_{0}.csv'.format(str(output).zfill(5))

    with open(filename,'w') as f:
        writer=csv.writer(f)
        titles=[
                'radius [pc]',
                'dx_min [pc]',
                'Msink [Msun]',
                'Mgas [Msun]',
                'M* [Msun]',
                'Mdm [Msun]',
                'CoMgas [3] [code_length]',
                'CoMpart [3] [code_length]',
                'Lgas [3] [cm^2/s]',
                'Lpart [3] [cm^2/s]',
                'bulkVgas [3] [km/s]',
                'bulkVpart [3]  [km/s]',
                'rhoMAX_val [Hcc]',
                'rhoMax_loc [3] [code_length]',
                'spinparameter'
                ]
        if not(sink_center):
            titles+=[
                'SFR [Msun/yr]',
                'meanT_weighted [K]',
                'varT_weighted [k]',
                'Mstar_young [Msun]',
                'CoMstar [3] [code_length]',
                ]
        writer.writerow(titles)

        for radius in np.logspace(-1,3.5,100):
            if sink_center:
                sp=ds.sphere(sink.pos,ds.quan(radius,'pc'))
            else:
                sp=ds.sphere(rho_loc,ds.quan(radius,'pc'))
        
            total_mass=sp.quantities.total_mass()
            quan=sp.quantities
            max_rho=quan.max_location(['density'])

            data=([
                    radius,
                    float(ds.index.get_smallest_dx().in_units('pc').v),
                    float(sink.mass.v),
                    float(quan.total_quantity(['cell_mass']).in_units('Msun')),
                    float(quan.total_quantity([('stars','particle_mass')]).in_units('Msun')),
                    float(quan.total_quantity([('DM','particle_mass')]).in_units('Msun'))
                    ] 
                  + list(quan.center_of_mass(use_gas=True,use_particles=False).v)
                  + list(quan.center_of_mass(use_gas=False,use_particles=True).v)
                  + list(quan.angular_momentum_vector(use_gas=True,use_particles=False).v)
                  + list(quan.angular_momentum_vector(use_gas=False,use_particles=True).v)
                  + list(quan.bulk_velocity(use_gas=True,use_particles=False).in_units('km/s').v)
                  + list(quan.bulk_velocity(use_gas=False,use_particles=True).in_units('km/s').v)
                  + [float(max_rho[0].in_units('amu/cm**3').v)]
                  + [float(max_rho[i].v) for i in range(1,4)]
                  + [float(quan.spin_parameter(use_gas=True,use_particles=False).v)]
                  #+ list(quan.spin_parameter(use_gas=False,use_particles=True).v)
                  )
            if not(sink_center):
                Mstar_young=(sp['myr1_stars','particle_mass'].in_units('Msun')).sum()
                SFR=float(Mstar_young/1E6)
                data+=([
                        SFR,
                        float(quan.weighted_average_quantity('temperature', 'cell_mass').in_units('K')),
                        float(quan.weighted_variance('temperature', 'cell_mass').in_units('K')[1]),
                        float(Mstar_young)
                        ]
                       +
                       center_of_mass(sp,particles='stars')
                       )
                
            writer.writerow(data)


    print("This took",(time.time()-lastsave)/60,"minutes")
    print("::::::::::::::::::::::::")
    return

def center_of_mass(myobject,particles='stars'):
    masses=myobject[particles,'particle_mass']
    CoM=[float((masses*myobject[particles,'particle_position_{0}'.format(dimension)]).sum()/masses.sum()) for dimension in ['x','y','z']]
    return CoM

################
def find_halo(output,folder='.',galaxy=False):
    '''Open yt_halos file and find closest halo/galaxy to sink particle'''
    sink=sinks.read(output,folder=folder)[0]

    if galaxy:
        filename=folder+'/Data/yt_galaxies_{0}_10.txt'.format(str(output).zfill(3))
    else:
        filename=folder+'/Data/yt_halos_{0}_10.txt'.format(str(output).zfill(3))
    print("Opening",filename)


    with open(filename) as f:
        reader=csv.reader(f,delimiter='\t')
        data=np.array([row[0].split() for row in reader])
            
        positions=np.array([np.array([float(row[i]) for i in [1,2,3]]) for row in data])
        radii=np.array([float(row[4]) for row in data])
        distances=np.array([np.linalg.norm(position-sink.pos.v) for position in positions])
        ids=np.array([int(float(row[0])) for row in data])
        nparts=np.array([int(float(row[-1])) for row in data])


        pos=positions[distances==distances.min()][0]
        rad=radii[distances==distances.min()][0]
        
        print("Host id",ids[distances==distances.min()])
        print("Halo position",pos)
        print("Sink position",sink.pos)
        print("All info",data[distances==distances.min()])
        print(distances.min()*sink.ds.length_unit.in_units('pc'))
    
        print("Next distance",distances[distances>distances.min()].min()*sink.ds.length_unit.in_units('pc'))
    
        print("Radius",rad,'kpc')   #*sink.ds.length_unit.in_units('pc'))
        print("Npart",nparts[distances==distances.min()])
        
        #print("./getgalstats -inp output_{0}".format(str(output).zfill(5)),"-xc",pos[0],"-yc",pos[1],"-zc",pos[2],"-rvir",rad,"-isfr 5 -comp 1 -cold 200 -rgal",0.2,"-fld 7 -spec 0 -dopc 0 -wout 2000 -Rsol 1 -prof -32 -pid 1 > test.csv")
        halo=Halo(data[distances==distances.min()][0],DM=not(galaxy),npart_load=10)
    return halo

#################################
#Custom fields

def field_vrel_x(field,data):
    if data.has_field_parameter('vcom_x'):
        vcom=data.get_field_parameter('vcom_x')
        relative_velocity=data['velocity_x']-vcom
        return relative_velocity
    else:
        return data['zeros']

def field_vrel_y(field,data):
    if data.has_field_parameter('vcom_y'):
        vcom=data.get_field_parameter('vcom_y')
        relative_velocity=data['velocity_y']-vcom
        return relative_velocity
    else:
        return data['zeros']

def field_vrel_z(field,data):
    if data.has_field_parameter('vcom_z'):
        vcom=data.get_field_parameter('vcom_z')
        relative_velocity=data['velocity_z']-vcom
        return relative_velocity
    else:
        return data['zeros']


##################################
# Helper functions

def find_aligned_axis(L):
    L=np.abs(L/np.linalg.norm(L))
    los=['x','y','z'][np.argmax(L)]
    return los

#Find index of a given time:
def find_time_index(time,times):
    times=np.array(times)
    index=np.argmin((times-time)**2)
    return index

#Load all outputs with profiles
def find_existing_profiles(folder='.',sink_center=True):
    outputs=myutils.find_outputs(folder=folder)
    profiles=[]
    found=[]
    for output in outputs:
        try:
            prof=Profile(output,folder=folder,sink_center=sink_center)
            profiles.append(prof)
            found.append(output)
        except:
            continue
    return np.array(profiles),np.array(found)

#Plot Decarli2010 M-sigma relation at a given redshift
def Mstar_Mbh(ax,z=6,color='k'):
    Mstar=np.logspace(4,10)
    data=[Mstar*10**ratio for ratio in Decarli2010(z=z)]   #value, lower, upper                                                                                                                                                                         
    ax.plot(Mstar,data[0],linestyle='--',color=color)
    ax.fill_between(Mstar,data[1],data[2],color=color,alpha=0.3,linewidth=0.0)

    #ax.plot(Mstar,data[1],color=color,alpha=0.5,linewidth=1)
    #ax.plot(Mstar,data[2],color=color,alpha=0.5,linewidth=1)

    return ax

def Decarli2010(z):
    ratio=(0.28)*z-2.91
    low_error=(0.28-0.06)*z-2.91-0.06
    high_error=(0.28+0.06)*z-(2.91+0.06)

    return [ratio,low_error,high_error]

##################################
# Classes

class Halos(object):
    def __init__(self,folder='.',galaxy=False):
        if galaxy:
            filename='./{0}/Data/galaxies.csv'.format(folder)
        else:
            filename='./{0}/Data/halos.csv'.format(folder)
        with open(filename) as f:
            reader=csv.reader(f)
            next(reader)
            rows=[row for row in reader]

        self.outputs=np.array([int(row[0]) for row in rows])
        self.times=np.array([float(row[1]) for row in rows])
        self.halos=np.array([Halo(row[2:],DM=True) for row in rows])
        
        self.positions=np.array([halo.pos for halo in self.halos])
        self.rvir=np.array([halo.rvir for halo in self.halos])*1E3
        self.mvir=np.array([halo.mvir for halo in self.halos])
        self.mh=np.array([halo.mh for halo in self.halos])
        #self.position=np.array([[float(dim) for dim in row[2:5]] for row in rows] )
        #self.rvir=np.array([float(row[5]) for row in rows])
        #self.mvir=np.array([float(


class Profile(object):
    def __init__(self,output,folder='.',sink_center=True):
        if sink_center:
            filename='./{0}/Data/profile_sink_{1}.csv'.format(folder,str(output).zfill(5))
        else:
            filename='./{0}/Data/profile_galaxy_{1}.csv'.format(folder,str(output).zfill(5))

        #Read data from file 
        with open(filename) as f:
            print("Opening",filename)
            reader=csv.reader(f)
            next(reader)
            self.data=np.array([Radius(line) for line in reader])
            if not sink_center:
                self.data=self.data[:-11]

        self.output=output
        self.sink=sinks.read(output,folder=folder)[0]
        self.ds=self.sink.ds

        #Calculate time in Myr
        self.time=float(self.ds.current_time.in_units('Myr'))


        #Calculate distance of Center of Mass from sink position
        [radius.sink_displacement(self.sink) for radius in self.data]

        #Calculate stellar mass
        self.Mstar=self.get_attribute('Mstar').max()
        self.Rstar=self.get_attribute('radius')[np.where(self.get_attribute('Mstar')>self.Mstar*0.99)].min()
        self.Mgas=self.get_attribute('Mgas')[np.where(self.get_attribute('radius')==self.Rstar)][0]

        #Calculate the rotation axis within R* and set CoM velocity
        self.Lgas=self.get_attribute('Lgas')[np.where(self.get_attribute('radius')==self.Rstar)][0]
        self.Lgas_norm=self.Lgas/np.linalg.norm(self.Lgas)

        #Calculate the centre of mass velocity within R*
        self.Vgas=self.ds.arr(self.get_attribute('Vgas')[np.where(self.get_attribute('radius')==self.Rstar)][0],'km/s')  #with units, to define the field
        self.CoMgas_vel=self.get_attribute('Vgas')[np.where(self.get_attribute('radius')==self.Rstar)][0] #without units for I can't remember what

        self.CoMgas_vrel=self.ds.arr(self.CoMgas_vel,'km/s')-self.sink.v
        self.CoMgas_vrel_mag=np.linalg.norm(self.CoMgas_vrel.in_units('km/s'))

        #Distance to densest cell
        self.rhoMAX_dist=self.get_attribute_at_radius('rhoMAX_dist',50)
        self.rhoMAX_loc=self.get_attribute_at_radius('rhoMAX_loc',50)

        #Load sink mass for easier plotting
        self.Msink=self.sink.mass

        #Load maximum DM mass
        self.Mdm=self.get_attribute('Mdm').max()

        if not(sink_center):
            self.CoMstar=self.get_attribute('CoMstar')[np.where(self.get_attribute('radius')==self.Rstar)][0]
            self.CoMstar_dist=self.get_attribute_at_radius('CoMstar_dist',50)
            self.sfr=self.get_attribute('sfr').max()
            self.Tave=self.get_attribute('Tave')[np.where(self.get_attribute('radius')==self.Rstar)][0]
            self.CSave=self.get_attribute('CSave')[np.where(self.get_attribute('radius')==self.Rstar)][0]


    def get_attribute(self,attribute):
        values=np.array([getattr(radius,attribute) for radius in self.data])
        return values

    def get_attribute_at_radius(self,attribute,radius):
        radii=(self.get_attribute('radius')-radius)**2
        values=self.get_attribute(attribute)
        return values[np.argmin(radii)]

    def set_CoM_velocity(self,myobject=None,radius=None):
        velocity=self.Vgas

        if radius:
            velocity=self.get_attribute_at_radius('Vgas',float(radius.in_units('pc')))


        velocity=self.ds.arr(velocity,'km/s')
        if not myobject:
            myobject=self.ds.all_data()
        for i,(dim,function) in enumerate(zip(['x','y','z'],[field_vrel_x,field_vrel_y,field_vrel_z])):
            myobject.set_field_parameter('vcom_{0}'.format(dim),velocity[i])
            myobject.ds.add_field(("gas","vcom_{0}".format(dim)),function=function,units=self.ds.unit_system["velocity"],validators=[ValidateParameter('vcom_{0}'.format(dim))],force_override=True)
        #myobject.ds.add_field(('gas','v_mag'),function=F_vrel_mag,units=self.ds.unit_system["velocity"])
        return myobject

class Radius(object):
    def __init__(self,line):
        self.radius=float(line[0])
        self.dx_min=float(line[1])
        self.Msink=float(line[2])
        self.Mgas=float(line[3])
        self.Mstar=float(line[4])
        self.Mdm=float(line[5])
        self.Mbar=self.Msink+self.Mgas+self.Mstar
        self.Mtot=self.Mbar+self.Mdm
        self.CoMgas=np.array([float(item) for item in line[6:9]])
        self.CoMpart=np.array([float(item) for item in line[9:12]])
        self.Lgas=np.array([float(item) for item in line[12:15]])
        self.Lpart=np.array([float(item) for item in line[15:18]])
        self.Vgas=np.array([float(item) for item in line[18:21]])
        self.Vpart=np.array([float(item) for item in line[21:24]])
        self.rhoMAX_val=float(line[24])  
        self.rhoMAX_loc=np.array([float(item) for item in line[25:28]])
        self.spin=float(line[28])
        self.Vgas_mag=np.linalg.norm(self.Vgas)
        self.Lgas_mag=np.linalg.norm(self.Lgas)

        if(len(line)>29):
            self.sfr=float(line[29])
            self.Tave=float(line[30])
            self.Tvar=float(line[31])
            self.Mstar_young=float(line[32])
            self.CoMstar=np.array([float(item) for item in line[33:36]])
            self.CSave=np.sqrt(self.Tave*1.38064852E-23*5/3/(1.6727*1E-27))/1E3

        #normalised and magnitudes
        self.Lgas_norm=self.Lgas/np.linalg.norm(self.Lgas)
        self.Lgas_mag=np.linalg.norm(self.Lgas)
        self.Lpart_mag=np.linalg.norm(self.Lpart)

    def sink_displacement(self,sink):
        self.CoMgas_dist=float(sink.ds.quan(np.linalg.norm(self.CoMgas-sink.pos.v),'code_length').in_units('pc'))   #Distance between CoMgas and sink in pc
        self.CoMpart_dist=float(sink.ds.quan(np.linalg.norm(self.CoMpart-sink.pos.v),'code_length').in_units('pc'))   #Distance between CoMpart and sink in pc 

        #Distance  to densest cell
        self.rhoMAX_dist=float(sink.ds.quan(np.linalg.norm(self.rhoMAX_loc-sink.pos.v),'code_length').in_units('pc'))
        
        if hasattr(self,'CoMstar'):
            self.CoMstar_dist=float(sink.ds.quan(np.linalg.norm(self.CoMstar-sink.pos.v),'code_length').in_units('pc'))
            
        #Relative velocity between sink and gas bulk
        self.v_rel=np.linalg.norm(sink.v.in_units('km/s').v-self.Vgas)   #in km/s


