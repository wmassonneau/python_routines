import old_horizon
import numpy as np
import csv
import horizon

#####################

def add_mass(output,DM=False):

    old_horizon._separator()

    noAGN_out=old_horizon.pairup_output(output,DM=DM)
    
    pairs=old_horizon.read_twins(output,DM=DM,halos=True,original=True)

    filename=old_horizon.pair_name(output,noAGN_out,DM=DM,original=False)

    print "Saving as","{0}.png".format(filename)

    with open(filename,'wb') as f:
        writer=csv.writer(f,delimiter='\t')
        writer.writerow(["AGN","noAGN","M_AGN/M_noAGN","","M_AGN [Msun]","halo=1,subhalo=0"])
        for pair in pairs:
            writer.writerow([pair.AGN.id,pair.noAGN.id,pair.frac,pair.AGN.halo.mvir,int(pair.AGN.halo.halo*pair.noAGN.halo.halo)])


###################

def read_sinks(out_AGN):
    nstep_coarse=horizon.get_nstepcoarse(out_AGN)
    filename='./SINK_FILES/python_sink_{0}.txt'.format(str(nstep_coarse).zfill(5))
    sinks=[]
    with open(filename) as f:
        reader=csv.reader(f,delimiter='\t')
        next(reader)
        for row in reader:
            sinks.append(SINK(row[0].split()))
    return sinks

class SINK(object):
    def __init__(self,line):
        self.id=int(float(line[0]))
        self.mass=float(line[1])
        self.pos=np.array([float(line[i]) for i in range(2,5)])
        self.v=np.array([float(line[i]) for i in range(5,8)])
        self.Bondi=float(line[8])
        self.Edd=float(line[9])
        self.accr_mass=float(line[10])

    def find_halo(self,halos):
        count=0
        for halo in halos:
            dist=np.linalg.norm(self.pos-halo.pos)
            if dist<halo.rvir:
                count+=1
        print self.id,count
