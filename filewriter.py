import os
import csv

###### Write amr2map files for a variety of projections etc
def amr2map(profiles,folder='.',zooms=10):
    #Maximum level
     lma=profiles[0].ds.parameters['levelmax'] - 6

     filename='amr2map.sh'
     with open(filename,'wt') as f:

         f.write('#!/bin/bash')
         f.write('\n')

         for profile in profiles:
             sink=profile.sink
             for zoom in zooms:
                axis=profile.get_attribute_at_radius('Lgas_norm',zoom/2)
                print(axis)
                zoom_code=zoom/(profile.ds.length_unit.in_units('pc').v)
                zoom_name=round(zoom)
                lines=['amr2map.o -inp ./{0}/output_{1} -out DensMap_{1}_{2}_pc{11}.csv -xc {3} -yc {4} -zc {5} -rad {10} -typ dens -lma {6} -asc 1 -log 1 -dir a -amx {7} -amy {8} -amz {9} -ldr {2} -nss 2'.format(folder,str(profile.output).zfill(5),los,sink.pos[0].v,sink.pos[1].v,sink.pos[2].v,lma,profile.Lgas_norm[0],profile.Lgas_norm[1],profile.Lgas_norm[2],zoom_code,zoom_name) for los in ['x','y','z']]
                f.write('\n'.join(lines))
                f.write('\n')

### Write input file for halomaker
def inputfiles(first,last,skip=1,folder='..'):

    f=open('inputfiles_HaloMaker.dat','w')
    for out in range(first,last+1,skip):
        line="'{1}/output_{0}/'\tRa3\t1\t{0}\n".format(str(out).zfill(5),folder)
        f.write(line)

### Write input file for treemaker
def input_TreeMaker():
    foldername="../"
    files=os.listdir(foldername)
    tree_nums=[file[-3:] for file in files if file[0:11]=="tree_bricks"]

    print("I found",len(tree_nums),"files, ranging from",tree_nums[0],"to",tree_nums[-1])
    f=open('input_TreeMaker.dat','w')

    f.write("{0} 1\n".format(len(tree_nums)))
    for num in tree_nums:
        line="'../tree_bricks{0}'\n".format(num)
        f.write(line)
