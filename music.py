#####################################
# A variety of helper functions to 
# make setting up MUSIC conditions 
# much faster and simpler
#####################################
import numpy as np
import math

h=70.0/100   #in m/s/Mpc
Mpc=3.085677E22    #Mpc in m
G=6.67384E-11
Msun=1.9891E30

def which_level(boxlength,m_halo=3.E13,resolution=1.E3,omega_m=0.309,omega_b=0.0486,h=0.677,music=True):
    """This function calculates which level is needed to resolve a halo with a particular number of particles, for a given boxlength

    boxlength: Size of the box, in Mpc
    m_halo: Mass of the halo to be resolved = 3E13 by default
    resoltion: Number of particles in the halo = 1E3 by default
    omega_m=0.281    (9-year WMAP)
    omega_b=0.046    (9-year WMAP)
    h=0.697          (9-year WMAP)
    music: use critical density given by music (True) or calculate from h (False)
    """

    omega_DM=omega_m-omega_b
    print("=========================")
    print("boxlength:",boxlength,"Mpc, or",boxlength*h,"Mpc/h")
    print("The",m_halo,"Msun halo is resolved by",resolution,"particles.")
    
    mp=m_halo/resolution
    print("Mass per particle:","{:.2E}".format(mp*h),"Msun/h or","{:.2E}".format(mp),"Msun")

    npart=omega_DM*rho_c(music)*(boxlength*h)**3/(mp*h)
    print("Total number of particles:","{:.2E}".format(npart))
    #print "Number of particles per dimension","{0:.2f}".format(npart**(1/3.))
    level=np.log(npart)/np.log(2)/3
    print("Level:",level,"or rounded up",math.ceil(level))
    print("=========================")

def which_mass(boxlength_h,level,baryon=True,omega_m=0.309,omega_b=0.0486,h=0.677,music=True):
    """ This function can be used to calculate the mass of the DM particles, for a given refinement level and boxlength.
    This assumes that baryon=yes in MUSIC. Otherwise, music uses Omega_m, instead of Omega_m-Omega_b to calculate the mass.RAMSES always uses the latter.

    boxlength: size of box, in Mpc/h, ie same value as .config file
    level: refinement level
    omega_m=0.281    (9-year WMAP)
    omega_b=0.046    (9-year WMAP)
    h=0.697          (9-year WMAP)
    baryons: Calculate the baryon mass per cell  => default True
    music: if True, use critical density used by music. Otherwise, calculate from h given
    """
    omega_DM=omega_m-omega_b
    boxlength_h=float(boxlength_h)
    print("=========================")
    print("boxlen:",boxlength_h,"Mpc/h or",boxlength_h/h,"Mpc")
    print("level:",level)

    dx_h=boxlength_h/2**level
    print("dx:",dx_h,"Mpc/h or",dx_h/h,"Mpc")
    
    if baryon:
        mc_h=omega_b*rho_c(music)*(dx_h)**3
        print("cell mass: ","{:.2E}".format(mc_h),"Msun/h or","{:.2E}".format(mc_h/h),"Msun")

    mp_h=omega_DM*rho_c(music)*(dx_h)**3
    print("DM mass: ","{:.2E}".format(mp_h),"Msun/h or","{:.2E}".format(mp_h/h),"Msun")
    print("=========================")


def rho_c(music=True):
    """ Calculates the critical density. 

    music=True: Takes the value hardcoded in MUSIC
    music=False: Calculates it directly from the h given
    """
    h_cgs=h*1000/Mpc

    if music:
        rho_c=2.77519737e11           #in units (h^-1 Msun) /(h^-1 Mpc)^3
        rho_c_cgs=rho_c*Msun/Mpc**3/100**3*1000
        #print "critical density in cgs units:",rho_c_cgs,"g/cm3/h2"
    else:
        rho_c_cgs=3*(h_cgs*100)**2/(8*np.pi*G)
        #print "critical density in SI units:",rho_c_SI,"kg/m3"
        rho_c=rho_c_cgs*Mpc*Mpc*Mpc/Msun/h/h

    #print "critical density:",rho_c,"(h^-1 Msun) /(h^-1 Mpc)^3"
    return rho_c

