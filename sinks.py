###########################################################################
# This file plots sink related quantities printed by RAMSES
#
#
#
#
#
##########################################################################
import yt
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
from yt.utilities.physical_constants import clight,G,mp,kb
import csv
import matplotlib.lines as mlines
import os
from astropy import units as u
from astropy.cosmology import WMAP9 as cosmo

import ytutils
import utils
import sinkfile
import halos

H0=0.676999969482422E+02
Msun=u.Msun.cgs.scale
pc=u.pc.cgs.scale

mpl.rcParams['lines.linewidth']=2
fontsize_glob=15

def resolved_rbondi(resolution,cs=10):
    ''' Calculate minimum mass for which r_bondi > dx'''
    mass=resolution*pc*(cs*1E5)**2/G/Msun
    print("minimum resolved mass","{0:2.3e}".format(float(mass)))

##################################
# TOPIC: Main plottings
#################################

#Main timeseries plotting function
def bh_timeseries(fields,folders=["."],sinkfile=None,limits=[None,None],addendum=None,log_mass=False,nsmooth=1,legend_loc=0,legend_columns=1,legend_panel=0,create_file=False,png=False,rb=[0,0],ff_time=False,reverse=False,field_limits=None,font_size=fontsize_glob,x='time',marker=None,kyr=False,nsample=1,verbose=False,save=True,cumulative=False,big_plot=False,output_folder='.',cluster_phases=None):
    ''' Main timeseries functions that plots a range of quantities in "fields", one per panel, for
    simulations contained in "folders"

    marker = NONE : if not none, the solid line is replaced by individual markers for plot clariy'''


    if cumulative:
        non_cum_fields=['dotMacc','chi','spinmag','dotMbondi']
        not_plottable=set(fields).intersection(set(non_cum_fields))
        if(len(not_plottable)>0):
            print("The following fields cannot be plotted cumulatively:",not_plottable)
            print("Set cumulative=False or re-evaluate the fields. Returning")
            return
        else:
            print("Plotting CUMULATIVE quantities.")


    plt.clf()
    legend_lines=[]
    #Determine the number of quantities to be plotted
    nfields=len(fields)
    if nfields>1:
        fig,axes=plt.subplots(nfields,1,figsize=(7.5,2.7*nfields),sharex=True)
    else:
        fig,axes=plt.subplots(figsize=(7.5,3.5))
        axes=[axes]

    if big_plot:
        fig.set_size_inches(12,4*nfields)

    #Labels and units
    plotname='BHtimeseries'
    field_units={'mean_density':"amu/cm**3","mean_cs":"km/s","vrel_mag":"cm/s",'radii':'pc'}
    labels={'BHmass':r"$M_{BH}$ [$M_\odot$]",
            'mean_density':r"$\rho_\bullet$ [H/$cm^3$]",
            'dotMacc':r"$\dot{M}$ [$M_\odot$/yr]",
            'mean_cs':r"c$_{s,\bullet}$ [km/s]",
            'vrel_mag':r"$v_\bullet$ [km/s]",
            'radii':'Radii [pc]',
            'fdrag':r"$F^D_\bullet / F^D_{max}$",
            'vsink_mag':'$v_{BH}$ [cm/s]',
            'rWeight':'r_weight [$\Delta x_{min}$]',
            'vel_mag':'velocity [km/s]',
            'Ra':'rAccertion [pc]',
            'Rb':'rBondi [pc]',
            'Rbhl':'rBHL [pc]','mach':r"$\mathcal{M}_\bullet$",
            'vgas_mag':'weighted v_gas [cm/s]',
            'vrel_x':r"$v_{x,\bullet}$ [cm/s]",
            'x_code':r"x [$\Delta x_{min}$]",
            'd_center':r'distance to center [pc]',
            'chi':r'$\chi=\dot{M}_{BH}/\dot{M}_{Edd}$',
            'asink_mag':r'acceleration $a_{BH}$ [km/s$^2$]',
            'av_costheta':r'angle $\mathbf{v}_{BH} \cdot \mathbf{a}_{BH}/v_{BH}/a_{BH}$',
            'L_AGN':r'$L_{AGN,feed}$ [$erg/s$]',  #AGN luminosity, calculated from Efeed
            'Efeed':r'$E_{AGN,avail}$ [$erg$]',  #AGN feedback energy
            'Esave':r'$E_{save}$ [$erg$]',
            'Edeposit':r'$E_{AGN,deposited}$ [$erg$]',
            'fdeposit':r'$E_{AGN,depos}/E_{AGN,avail}$',
            'fbondi':r'$\dot{M}/\dot{M}_{BH}$',
            'spinmag':r'SMBH spin',
            'jet_theta':r'$\theta_{\rm jet}$ [deg]',
            'jet_phi':r'$\phi_{\rm jet}$ [deg]',
            'bhspin_theta':r'$\theta_{\rm SMBH}$ [deg]',
            'bhspin_phi':r'$\phi_{\rm SMBH}$ [deg]',
            'jgas_theta':r'$\theta_{\rm gas}$ [deg]',
            'jgas_phi':r'$\phi_{\rm gas}$ [deg]'
            }

    #Load colours
    if len(folders)>1:
        colours=utils.get_colours(rb[0],rb[1])
    else:
        colours=['k']

    #Check that the code does not run if there are less colours than folders
    if len(colours)<len(folders):
        print("There are not enough colours for the",len(folders),"folders. rb=",rb)
        return


    if reverse:
        folders=folders[::-1]
        colours=colours[::-1]

    #Main loop over different simulations to be plotted
    for j,(folder,colour) in enumerate(zip(folders,colours)):
        print("#####################")
        if not sinkfile:
            sinkfile=Sinkfile(folder,create_file=create_file,verbose=verbose)
        ds=sinkfile.ds

        if len(set(['phi','theta'])&set('_'.join(fields).split('_'))) > 0:
            #Add the angle information to the sink file
            if not hasattr(sinkfile,'bhspin_theta'):
                sinkfile.calculate_spin_angles()

        x_data=sinkfile.getattr(x)    #load xaxis (time)
        if x=='time':  # convert to Myr
            if kyr:
                x_data=x_data*1E-3
            else:
                x_data=x_data*1E-6
            if verbose:
                print("TIME RANGE",x_data[-1])

        legend_lines.append(mlines.Line2D([], [], color=colour,
                                          markersize=15, label=folders[j].split('/')[-1]))

        #Loop over quantities to be plotted
        for i,(y,ax) in enumerate(zip(fields[:nfields],axes)):   #loop over fields
            if(verbose):
                print("=== Plotting",i,y)
            try:
                y_data=sinkfile.getattr(y)
                #Take out Nan values
                nan_mask=np.isnan(y_data)
                if y in ['Efeed','Edeposit','fdeposit']:
                    if(verbose):
                        print("Not downsampling",y)
                    y_plot=y_data[~nan_mask]
                    x_plot=x_data[~nan_mask]
                else:
                    y_plot=shorten_data(y_data[~nan_mask],nsmooth=nsmooth,nsample=nsample)
                    x_plot=shorten_data(x_data[~nan_mask],nsmooth=nsmooth,nsample=nsample)

                if cumulative:
                    y_plot=np.cumsum(y_plot)

                if marker:
                    ax.scatter(x_plot,y_plot,color=colour,marker=marker,s=2)
                else:
                    ax.plot(x_plot,y_plot,color=colour)
                if(verbose):
                    print(y,y_plot.min(),y_plot.max())
            except:
                print("This is not a valid field to plot directly:",y)


            if y=='dotMacc' or y=='dotMbondi':
                if len(folders)==1:
                    edd_colour='darkgrey'
                else:
                    edd_colour=colour
                if j==0:
                    label_edd=r'$\dot{M}_{Edd}$'
                    label_bondi=r'$\dot{M}_{BHL}$'
                    label_supply=r'$\dot{M}_{max}$'
                else:
                    label_edd=None
                    label_bondi=None
                    label_supply=None
                #ax.axhline(1.5E-4,linestyle=':',color='grey')
                #ax.plot(x_plot,shorten_data(sinkfile.getattr('dotMedd'),nsmooth=nsmooth,nsample=nsample),color=edd_colour,linestyle='--',label=label_edd)
                ax.plot(x_plot,shorten_data(sinkfile.getattr('dotMbondi'),nsmooth=nsmooth,nsample=nsample),color=edd_colour,linestyle=':',label=label_bondi)

            elif y=='chi':
                ax.axhline(0.01,color='darkgrey',linestyle=':')
                ax.axhline(1.0,color='darkgrey',linestyle='-')
            elif y=='d_center':
                ax.axhline(0.0,linestyle=':',color='darkgrey')
                ax.axhline(sinkfile.dx_min,linestyle=':',color='darkgrey')
            elif y=='rg_scale':
                dx_min=shorten_data(sinkfile.dx_min,nsmooth=nsmooth,nsample=nsample)
                ax.plot(x_plot,dx_min,linestyle=':',color='darkgrey')
                ax.plot(x_plot,0.2*dx_min,linestyle='-',color='darkgrey')
            elif y=='BHmass':
                if not log_mass:
                    log_mass=max(sinkfile.getattr(y))/min(sinkfile.getattr(y))>100.0
            elif y=='acc_drag_x':
                ax.axhline(0.0,linestyle=':',color='darkgrey')
            elif y in ['x','y','z']:
                box_center=ds.quan(0.5,'code_length')
                ax.axhline(float(ds.quan(0.5,'code_length').in_units('pc')),color='grey',linestyle=':')
                ax.axhline(float(ds.quan(0.5,'code_length').in_units('pc')+ds.index.get_smallest_dx().in_units('pc')),color='grey',linestyle=':')
            elif y=='L_AGN':
                ax.axhline(1E44,color='grey',linestyle=':')
                ax.axhline(1E46,color='grey',linestyle=':')
            elif y=='Efeed' and cumulative:
                ax.axhline(1E63,color='grey',linestyle=':')
            #elif y in ['bhspin_theta','bhspin_phi','jgas_theta','jgas_phi']:
            #    ax.plot(x_plot,shorten_data(sinkfile.getattr('{0}_naive'.format(y)),nsmooth=nsmooth,nsample=nsample),color=colour,linestyle=':')

            #Do anything that only needs to be done once per plot
            if j==len(folders)-1:
                #Set y limits, scale and labels
                if field_limits:
                    try:
                        ax.set_ylim(field_limits[y][0],field_limits[y][1])
                    except:
                        if(verbose):
                            print("No field limit set",y,field_limits)
                        else:
                            foo=0

                if y in ['Esave','Efeed','Edeposit'] and not(cumulative):
                    ylims=ax.get_ylim()
                    ax.set_ylim(1E52,ylims[1])

                #plt.locator_params(axis='y',nbins=5)
                set_log_scales(ax,x,y,log_mass=log_mass)

                #ax.set_ylim(ylims[0]*0.9,ylims[1]*1.1)

                try:
                    if cumulative:
                         ax.set_ylabel(r'$\sum$'+labels[y],fontsize=font_size,labelpad=5)
                    else:
                        ax.set_ylabel(labels[y],fontsize=font_size,labelpad=5)
                except:
                    if(verbose):
                        print("Missing label",y)
                    ax.set_ylabel(y,fontsize=font_size,labelpad=5)

                #Set x limits, scale and labels
                xlims=list(ax.get_xlim())
                if x=='time':
                    #Sanity check for negative time
                    if xlims[0]<0:
                        ax.set_xlim(0,xlims[1])
                    if kyr:
                        xlabel='time [kyr]'
                    else:
                        xlabel='time [Myr]'
                    if ff_time:
                        xlabel='time [$t_{ff}$/$10^3$]'
                elif x=='aexp':
                    xlabel=r'expansion factor $a$'
                else:
                    xlabel=x  #For redshift

                if i==nfields-1:   #only add the x label to the bottom panel
                    ax.set_xlabel(xlabel,fontsize=font_size)
                    if x=='redshift':
                        print("Inverting")
                        ax.invert_xaxis()

                #Add legengs legends
                if i==legend_panel and len(folders)>1:
                    ax.legend(handles=legend_lines,frameon=False,loc=legend_loc,fontsize=font_size-4,ncol=legend_columns)
                if y=='dotMacc' or y=='dotMbondi':
                    ax.legend()

            ###### End of loop over fields

        sinkfile=None        #Clear for next folder, in case there is more than one

        ########## End of loop over folders

    #Set x-limits
    for k,limit in enumerate(limits):
        if limit:
            xlims[k]=limit
        if k==0:
            plotname+="_limits"
        axes[-1].set_xlim(xlims)

    #Fix label location and padding between plots
    plt.tight_layout(h_pad=0.00)
    for ax in axes:
        ax.yaxis.set_label_coords(-0.1,0.5)

    if len(folders)>1:
        plotname+="_compare{0}_fields{1}".format(len(folders),nfields)
    if log_mass:
        plotname+="_logM"
    else:
        plotname+="_linM"
    if marker:
        plotname+="_{0}".format(marker)
    if cumulative:
        plotname+='_cumulative'
    if save:
        utils.saveas(fig,plotname,pdf=not(png),addendum=addendum,output_folder=output_folder)

    return fig,plotname

##################################################
# TOPIC: Small utilities
##################################################

def smooth(data,nsmooth):
    '''Smooth data by averaging over nsmooth values'''
    if nsmooth==1:
        return np.array(data)
    data=data[:(len(data)//nsmooth)*nsmooth]   #cut to multiple of nsmooth for smoothing
    smoothed=[data[0]]+list(np.mean(data.reshape(-1,nsmooth),axis=1))     #average over nsmooth values
    return np.array(smoothed)

def sample(data,nsample):
    '''Sample the data at regular intervals'''
    if nsample==1:
        return np.array(data)
    return data[::nsample]
    #data=data[:(len(data)//nsample)*nsample]

def shorten_data(data,nsmooth=1,nsample=1):
    ''' This function is used to smooth or downsample data for plotting'''
    if nsmooth>1:
        return smooth(data,nsmooth)
    elif nsample>1:
        return sample(data,nsample)
    else:
        return np.array(data)

def resolved_rbondi(resolution,cs=10):
    ''' Calculate minimum mass for which r_bondi > dx'''
    mass=resolution*pc*(cs*1E5)**2/G/Msun
    print("minimum resolved mass","{0:2.3e}".format(float(mass)))

def set_log_scales(ax,x,y,log_mass):
    log_axes=['mean_density','mass_difference','r_bondi','Ra','Rb','Rbhl','radii','rWeight','Fdrag','fdrag',
              'asink_mag','dotMbondi','dpacc_mag','p_mag','f_edd','vsink_mag','L_AGN','rg_scale','fdeposit','Efeed']
    symlog_axes=['mach','vsink_x','vsink_y','vsink_z','dMsmbh','Edeposit','bhspin_x','bhspin_y','bhspin_z','jetfrac','dotMacc']
    if log_mass:
        log_axes.append('BHmass')
    if x in log_axes:
        ax.set_xscale('log')
    if y in symlog_axes:
        ax.set_yscale('symlog',linthreshy=1E-8)
        if y in ['d_center']:
            ax.set_yscale('symlog',linthreshy=1E0)
        if y in ['mach','dMsmbh','dotMacc','d_center']:
            ylim=ax.get_ylim()
            ax.set_ylim(0,ylim[1])
    if y in ['acc_drag_x']:
        ax.set_yscale('symlog',linthreshy=1E-8)
    if y in ['jetfrac','chi']:
        ax.set_yscale('symlog',linthreshy=1E-8)
        ylim=ax.get_ylim()
        ax.set_ylim(0,ylim[1])
    if y in log_axes:
        ax.set_yscale('log')
    return ax

def find_first(sinks=None):
    ''' this function finds the first output with a sink and loads that sink'''
    if not(sinks):
        sinks=[asink(output) for output in ytutils.existing_outputs()]
    for sink in sinks:
        if sink.exists:
            return sink.output

## A function that reads the sinks over all outputs
def read(output,folder='.',silent=False):
    filename='{1}/output_{0}/sink_{0}'.format(str(output).zfill(5),folder)
    if os.path.isfile(filename+'.out'):
        filename=filename+'.out'
        new=False
    else:
        filename=filename+'.info'
        new=True
    if not silent:
        print("opening",filename)

    ds=ytutils.load(output,folder,silent=silent)
    with open(filename) as f:
        mylist=f.read().splitlines()
    mysinks=np.array([Sink(line,ds,new=new) for line in mylist[4:-1]])
    return mysinks

# Star related utilites
#A function that finds the oldest star within the sink halo
def oldest_halo_star(sinkhalo=""):
    print("entering which_star")
    if sinkhalo=="":
        sinkhalo=halos.which_halo(findfirst())
    sinkhalo.makesphere()
    ids=sinkhalo.sphere[('stars','particle_identity')]
    return min(ids)

##################################################
# TOPIC: Classes
##################################################

class Sinkfile(object):
    def __init__(self,folder=".",create_file=False,verbose=False):
        foldername="{0}/{1}".format(folder,utils.datafolder)
        filename=foldername+"/sink_info.csv"
        if not(os.path.exists(filename)) or create_file:
            print("I need to create the sinkfile")
            sinkfile.write(folder)
        print("Opening",filename)
        with open(filename) as f:
            reader=csv.reader(f,delimiter=',')
            try:
                n_fields=int(next(reader)[-1])
            except:
                n_fields=19
                print("Old ramses file. Manually setting field number to",n_fields)
            fields=[]
            field_lengths=[]
            if verbose:
                print("Number of fields",n_fields)
            for i in range(n_fields):
                row=next(reader)[0].split()
                fields.append(row[1])
                field_lengths.append(int(float(row[2])))
            print("Available fields",fields)

            self.data=[FINEstep(row,fields,field_lengths) for row in reader if not row[0][0]=='#']
        if verbose:
            print("Number of datapoints",len(self.data))

        try:
            self.ds=ytutils.find_first_ds(folder)
            if not self.ds.cosmological_simulation:
                #Calculate quantities that need the dataset
                self.box_center=float(self.ds.length_unit.in_units('pc'))/2
                self.dx_min=self.ds.length_unit.in_units('pc')/2**self.ds.parameters['levelmax']
                self.calculate_boxcenter_distances()
            else:
                self.boxlen=self.ds.length_unit.in_units('Gpc')*(1+self.ds.current_redshift)
                print(self.boxlen)
                self.dx_min=self.boxlen.in_units('pc').v/(2.0**self.getattr('levelsink'))*self.getattr('aexp')
        except:
            self.ds=None
            print("!!!!!!!No dataset found in loading Sinkfile!!!")
            if verbose:
                print("Trying for info file instead")

            try:
                with open(folder+'/info_00001.txt') as f:
                    reader=csv.reader(f)
                    info=[row[0].split('=')[-1].lstrip() for row in reader]
                    levelmax=int(info[3])
                    boxlen=float(info[7])
                    unit_l=float(info[15])
                    boxsize=unit_l*boxlen/3.08E18
                    self.box_center=boxsize/2  #in pc
                    self.dx_min=boxsize/2**levelmax
                    if verbose:
                        print('boxsize [Mpc]',boxsize/1E6)
                        print('dx_min [pc]',self.dx_min)
                    self.calculate_boxcenter_distances()
            except:
                print("Boxcenter distances not calculated. Field d_center not available")
        print("Final time",self.data[-1].time/1E6,'Myr')

        #Process other quantities
        self.recalculate_dotMacc()

    def recalculate_dotMacc(self):
        #Recalculates the accretion rate post smoothing
        masses=self.getattr('BHmass')
        times=self.getattr('time')#10**6

        time_step=(times[1:]-times[:-1])
        dotMacc=(masses[1:]-masses[:-1])/time_step
        dotMacc=np.append(dotMacc[0],dotMacc)  #Add the zeroth value back in

        time_step=np.append(time_step,time_step[-1])  #Add the missing timestep at the end

        from yt.units import Msun,yr,c
        Msun=float(Msun.in_units('g'))
        yr=float(yr.in_units('s'))
        c=float(c.in_units('cm/s'))


        for datum,new_dotMacc,timestep in zip(self.data,dotMacc,time_step):
            setattr(datum,'dotMacc',new_dotMacc)
            setattr(datum,'fbondi',new_dotMacc/datum.dotMbondi)
            setattr(datum,'dt',timestep)
            chi=datum.dotMacc/datum.dotMedd
            if chi>0.01:
                epsilon=0.15
            else:
                epsilon=1.0
            epsilon=epsilon*0.1 #Radiative efficency of a thin disk
            if new_dotMacc>0:
                setattr(datum,'chi',chi)
            else:
                setattr(datum,'chi',np.nan)
            try:
                setattr(datum,'L_AGN',datum.Efeed/(timestep*yr))
            except:
                setattr(datum,'L_AGN',np.nan)

    def calculate_acceleration(self):
        #Calculate the acceleration onto the sink in the frame of the box
        times=self.getattr('time')
        time_step=(times[1:]-times[:-1])
        for dim in ['x','y','z']:
            velocities=self.getattr('vsink_{0}'.format(dim))
            accelerations=(velocities[1:]-velocities[:-1])/time_step
            accelerations=np.append(accelerations,accelerations[-1])
            for datum,acc in zip(self.data,accelerations):
                setattr(datum,'asink_{0}'.format(dim),acc)

        for datum,ax,ay,az in zip(self.data,self.getattr('asink_x'),self.getattr('asink_y'),self.getattr('asink_z')):
            setattr(datum,'asink',np.array([ax,ay,az]))
            setattr(datum,'asink_mag',np.linalg.norm([ax,ay,az]))

            #Calculate the angle between the sink velocity and acceleration
            costheta=np.dot(datum.asink,datum.vsink)/np.linalg.norm(datum.asink)/np.linalg.norm(datum.vsink)
            setattr(datum,'av_costheta',costheta)

    def calculate_boxcenter_distances(self):
        #Calculate the distance of the sink from the center of the box
        distances=self.getattr('position')-self.box_center
        [setattr(datum,'d_center',np.linalg.norm(distance)) for datum,distance in zip(self.data,distances)]

    def getattr(self,attribute):
        attributes=self.data[0].__dict__.keys()
        if attribute in attributes:
            values=np.array([getattr(item,attribute) for item in self.data])
        else:
            # Calculate missing values
            self.calculate_acceleration()
            try:
                # Try to load again
                values=np.array([getattr(item,attribute) for item in self.data])
            except:
                attributes=self.data[0].__dict__.keys()
                raise utils.EmptyException("{1} is not a valid attribute, choose one of {0}".format(attributes,attribute))

        return values

    def calculate_spin_angles(self):
        if not(hasattr(self.data[0],'bhspin_x')):
            print("Spin values not written to file. Returning")
            return
        jgas_vectors=utils.make_vector_array(self.getattr('jgas_x'),self.getattr('jgas_y'),self.getattr('jgas_z'))
        theta,phi=utils.post_process_polar_angles(jgas_vectors*1E12)
        [setattr(datum,'jgas_theta_cum',angle) for datum,angle in zip(self.data,theta)]
        [setattr(datum,'jgas_phi_cum',angle) for datum,angle in zip(self.data,phi)]
        [setattr(datum,'jgas_theta',angle) for datum,angle in zip(self.data,utils.project_to_range(theta,0,360))]
        [setattr(datum,'jgas_phi',angle) for datum,angle in zip(self.data,utils.project_to_range(phi,0,180))]

        bhspin_vectors=utils.make_vector_array(self.getattr('bhspin_x'),self.getattr('bhspin_y'),self.getattr('bhspin_z'))
        theta,phi=utils.post_process_polar_angles(bhspin_vectors)
        [setattr(datum,'bhspin_theta_cum',angle) for datum,angle in zip(self.data,theta)]
        [setattr(datum,'bhspin_phi_cum',angle) for datum,angle in zip(self.data,phi)]
        [setattr(datum,'bhspin_theta',angle) for datum,angle in zip(self.data,utils.project_to_range(theta,0,360))]
        [setattr(datum,'bhspin_phi',angle) for datum,angle in zip(self.data,utils.project_to_range(phi,0,180))]

        return

def gradient(times,data):
    dt=times[1:]-times[:-1]
    dx=data[1:]-data[1:-1]
    grad=list(dx/dt)
    grad=grad+[grad[-1]]
    return np.array(dx/dt)

## A class that reads the sink file and collects all infromation about it.
class Sink(object):
    def __init__(self,line,ds,new=False):
        data=[float(i) for i in line.split()]

        #Save ds information
        self.ds=ds
        self.t=ds.current_time
        self.z=ds.current_redshift
        self.output=ytutils.get_number(ds)
        self.ad=self.ds.all_data()

        #Add constants
        self.G=ytutils.link_to_ds(G,self.ds)

        #Add data from sinkfile
        self.id=int(data[0])
        self.mass=self.ds.quan(data[1],'Msun')
        self.pos=self.ds.arr(data[2:5],'code_length')/self.ds.parameters['boxlen']
        self.x=self.pos[0]
        self.y=self.pos[1]
        self.z=self.pos[2]
        self.v=self.ds.arr(data[5:8],'code_velocity')
        self.v_mag=self.ds.quan(np.linalg.norm(self.v),'code_velocity')

        #if the refinement radius is written to file
        if len(data)>8:
            self.r_refine=self.ds.quan(data[8],'code_length')/self.ds.parameters['boxlen']

    def which_halo(self,halos=None,npart=100,):
        self.halo=halos.which_halo(self.output,particles=[self],npart=npart,halos=halos)
        return self.halo

    def get_halo_dist(self,npart=100,halos=None):
        ''' determines the closest distance to a DM halo'''
        self.which_halo(npart=npart,halos=halos)
        self.dist=self.ds.arr(np.linalg.norm(self.halo.pos-self.pos),'code_length')
        if self.dist < self.halo.rvir:
            self.inhalo=True
        else:
            self.inhalo=False
        return self.dist

class FINEstep(object):
    def __init__(self,line,fields,field_lengths):
        Rscale_unit=6.67408E-8*1.988E33/1E10/3.085677E18
        index_field=0   #Index at which a field starts
        for i,field in enumerate(fields):
            if field_lengths[i]==1:
                data=line[sum(field_lengths[:i])]
                try:
                    data=float(data)   #Handle them all as floats. They get converted during smoothing anyway
                except:
                    data=0
            else:
                data=line[sum(field_lengths[:i]):sum(field_lengths[:i+1])]
                try:
                    data=np.array([float(item) for item in data])
                except:
                    data=np.zeros(len(data))
            setattr(self,field,data)

        self.redshift=(1/self.aexp)-1

        #Transcribe for easy of access
        self.vsink_mag=np.linalg.norm(self.vsink)*1E5
        self.vsink_x=self.vsink[0]*1E5
        self.vsink_y=self.vsink[1]*1E5
        self.vsink_z=self.vsink[2]*1E5

        self.x=self.position[0]
        self.y=self.position[1]
        self.z=self.position[2]

        try:
            self.dpacc_mag=np.linalg.norm(self.dpacc)*1E5
            self.dpacc_x=self.dpacc[0]*1E5
            self.dpacc_y=self.dpacc[1]*1E5
            self.dpacc_z=self.dpacc[2]*1E5
        except:
            tmp=0

        try:
            self.drag_on1=self.drag_on[0]
            self.drag_on2=self.drag_on[1]
        except:
            tmp=0

        #Calculate sink momentum
        self.p_x=self.vsink_x*self.BHmass
        self.p_y=self.vsink_y*self.BHmass
        self.p_z=self.vsink_z*self.BHmass
        self.p_mag=self.vsink_mag*self.BHmass  # in Msun*

        try:
            self.dpacc_mag=np.linalg.norm(self.dpacc)*1E5
            self.dpacc_x=self.dpacc[0]*1E5
            self.dpacc_y=self.dpacc[1]*1E5
            self.dpacc_z=self.dpacc[2]*1E5
            #calculate accreted momemntum
            self.acc_drag_x=self.dpacc_x/self.p_x
            self.acc_drag_y=self.dpacc_y/self.p_y
            self.acc_drag_z=self.dpacc_z/self.p_z
        except:
            tmp=0

        try:
            if self.Efeed>0:
                self.Edeposit=self.Efeed-self.Esave
                self.fdeposit=self.Edeposit/self.Efeed
            else:
                self.Efeed=np.nan
                self.Edeposit=np.nan
                self.fdeposit=np.nan
        except:
            self.Efeed=np.nan
            self.Edeposit=np.nan
            self.fdeposit=np.nan

        try:
        #Angular momentum of accreted gas
            self.jgas_x=self.jgas[0]
            self.jgas_y=self.jgas[1]
            self.jgas_z=self.jgas[2]
            self.jgas_mag=np.sqrt(self.jgas_x**2+self.jgas_y**2+self.jgas_z**2)

            self.jgas_theta_naive=np.rad2deg(np.arctan2(self.jgas_y,self.jgas_x)) #in the x-y plane of the box
            self.jgas_phi_naive=np.rad2deg(np.arccos(self.jgas_z/self.jgas_mag))    #to the z axis of the domain

            self.bhspin_x=self.bhspin[0]
            self.bhspin_y=self.bhspin[1]
            self.bhspin_z=self.bhspin[2]

            self.bhspin_theta_naive=np.rad2deg(np.arctan2(self.bhspin_y,self.bhspin_x)) #in the x-y plane of the box
            self.bhspin_phi_naive=np.rad2deg(np.arccos(self.bhspin_z))    #to the z axis of the domain
        except:
            tmp=0

        try:
            self.spinmag=abs(self.spinmag)
        except:
            tmp=0
        try:
            self.jetfrac=self.dMsmbh/(self.BHmass-self.dMsmbh)
        except:
            tmp=0

        #Recalculate the time for cosmological simulations
        if self.time<0:
            self.time=cosmo.age(self.redshift).value*10**9
