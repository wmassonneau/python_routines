##########################################################################
#  The routine in this file use the ramses version from October 2013
#  It provides a variety of routines to make phaseplots for the gas in
#  a single halo or the whole box. It uses the yt.profile routines to
#  extract the data, allowing one to overplot many different features.
#
#
##########################################################################
import yt
import ytutils
import utils
import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import pickle

#####################################
# TOPIC: Main plotting function
#######################################

# Main phaseplot plotting function, where the 2d profile is extracted using yt but plotted using pyplot. This allows for extra annotations, overplotting of profiles etc
def plot(output,region=None,x="density",y="temperature",z="cell_mass",addendum="",log=True,saveit=True,jeans=False,jeansargs={},xunit=None,yunit=None,zunit=None,annotation=None,folder='.',logs={},small_figure=False,title=None,fig=None,weight_field='ones',output_folder=None):
    ''' Flexible but currently quite basic plotting function'''

    print("If nothing special is required, use phaseplot.region for a quick, yt only plot")
    plt.clf()
    fontsize=17
    if not fig:
        fig,ax=plt.subplots(1,1,figsize=(12,10))
    else:
        ax=fig.gca()

    mpl.rcParams['font.size'] = fontsize

    #Load data and make the profile
    if not region:
        ds=ytutils.load(output,folder=folder)
        region=ds.all_data()
        print("Using whole box")
    else:
        ds=region.ds

    profile=yt.create_profile(region,[x,y],z,weight_field=weight_field,n_bins=256,logs=logs)

    #Set profile units as desired
    units=ytutils.Units()
    if not xunit:
        if hasattr(units,x.split('_')[-1]):
            xunit=getattr(units,(x.split('_')[-1]))
    if xunit:
        profile.set_x_unit(xunit)

    if not yunit:
        if hasattr(units,y.split('_')[-1]):
            yunit=getattr(units,(y.split('_')[-1]))
    if yunit:
        profile.set_y_unit(yunit)

    if not zunit:
        if hasattr(units,z.split('_')[-1]):
            zunit=getattr(units,(z.split('_')[-1]))
    if zunit:
        profile.set_field_unit(z,zunit)

    #Extract values from profile to be plotted, in correct units already
    X=np.array(profile.x)
    if not(xunit):
        xunit=profile.x.units
    Y=np.array(profile.y)
    if not(yunit):
        yunit=profile.y.units
    Z=np.array(np.transpose(profile[z]))     #Not sure why the transpose is necessary  but without it's  flipped.
    if not(zunit):
        zunit=profile[z].units
    print(xunit,yunit,zunit)

    #Sort out colorbar limits
    if log:
        norm = mpl.colors.LogNorm(vmin=Z[Z>0].min(),vmax=Z.max())
    else:
        norm = mpl.colors.Normalize(vmin=Z[Z>0].min(),vmax=Z.max())

    #Actually make the plot, log both axed and add the colorbar
    plot=ax.pcolormesh(X,Y,Z,norm=norm)
    if X.min()>0:
        ax.set_xscale('log')
    else:
        ax.set_xscale('symlog')

    if Y.min()>0:
        ax.set_yscale('log')
    else:
        ax.set_yscale('log')

    #ax.loglog()
    cb=fig.colorbar(plot,pad=0.0)

    #Format axis labels:
    ax.set_xlabel("{0} ({1})".format(x.capitalize(),xunit,fontsize=fontsize))
    ax.set_ylabel("{0} ({1})".format(y.capitalize(),yunit,fontsize=fontsize))
    cb.set_label("{0} ({1})".format(z.capitalize(),zunit,fontsize=fontsize))

    #Draw in jeans length line, if required
    if jeans:
        if x is not "density" or y is not "temperature":
            print("wrong fields! I cannot calculate the jeans length for this. Set x='density' and y='temperature'")
        else:
            rho,T=ytutils.jeans(ds,profile.y,**jeansargs)
            plt.plot(rho.in_units(units.density),T,'r--',c='k')

    if small_figure:
        fig.set_size_inches(8,6)

    if title:
        ax.set_title(title)

    plt.tight_layout()

    #Make plotname and save
    plotname="phaseplot_{0}_{1}_{2}".format(x,y,str(output).zfill(3))
    if saveit:
        utils.saveas(fig,plotname,addendum=addendum,output_folder=output_folder)

        #Return profile, for further analysis if required
    return profile,fig


###################################
# TOPIC: Quick plotting functions
##################################

# Quick phaseplot function for density-temperature plot, using yt all the way
def region(output,folder='.',region=None,addendum=None,time=False,annotation=None,saveit=False):
    utils._separator()
    ds=ytutils.load(output,folder=folder)
    if not region:
        region=ds.all_data()
        if addendum:
            addendum='box_'+addendum
        else:
            addendum='box'
    current_plot=yt.PhasePlot(region,'density','temperature',['cell_mass'],weight_field=None)
    current_plot.set_unit('density','amu/cm**3')
    current_plot.set_unit('temperature','K')
    current_plot.set_unit('cell_mass','Msun')

    if time:
        text="t={0} Myr".format("{:.2f}".format(float(ds.current_time.in_units('Myr'))))
        current_plot.annotate_text(0.1,0.1,text)#,text_args={'size':'xx-large','color':'k'})

    if annotation:
        current_plot.annotate_text((0.1,0.1),annotation)#,coord_system='axis',text_args={'size':'xx-large','color':'k'})

    figname='phaseplot_yt_{0}'.format(output)
    if saveit:
        utils.saveas(current_plot,figname,addendum=addendum)
    return current_plot



###########################################
# Input-output functions
##########################################

def save_profile(profile,filename=None):
    #Reload at the other end using profile=yt.load(filename)
    #Make profile plots using plot = yt.PhasePlot(profile.data,"density","temperature","cell_mass",weight_field=None)
    #Access the data using profile.data['density'] and such

    profile.save_as_dataset(filename)
    return
