import numpy as np
import csv
import sinks
import sys
import glob
from utils import  *

def write(folder):
    check_data_folder(folder)  #Check location and create data folder if necessary
    foldername="{0}/{1}".format(folder,datafolder)
    orig_file=Sinkfile(folder=folder)
    filename=foldername+'/sink_info.csv'

    step_data=[]
    key_lengths={'nstep_fine':1,
                 'nstep_coarse':1,
                 'aexp':1,
                 'time':1,
                 'BHmass':1,
                 'position':3,
                 'vsink':3,
                 'dotMbondi':1,
                 'dotMedd':1,
                 'rg_scale':1,
                 'spinmag':1,
                 'dpacc':3,
                 'lsink':3,
                 'f_edd':1,
                 'eta_jet':1,
                 'eta_disk':1,
                 'eps_sink':1,
                 'dMsmbh':1,
                 'Esave':1,
                 'Efeed':1,
                 'jgas':3,
                 'bhspin':3,
                 'levelsink':1,
                 }
    active_keys=['nstep_fine','nstep_coarse','aexp','time','BHmass','position','vsink','dotMbondi','dotMedd','rg_scale','dpacc','spinmag','f_edd','eta_jet','eta_disk','eps_sink','dMsmbh','Esave','Efeed','jgas','bhspin','levelsink']
    for i,step in enumerate(orig_file.data):
        step_data.append([])
        for key in active_keys:
            if key in orig_file.fields:  #Check if this file has data for a given key
                item=getattr(step,key)
                if isinstance(item,np.ndarray):   #Check if it is a numpy array
                    step_data[i]+=list(item)
                else:
                    step_data[i].append(item)
            else:       #Fill with dummy data
                step_data[i]+=list(np.zeros(key_lengths[key]))
    step_data=np.array(step_data)

    #Smooth down overly long and detailed files
    Nvalues=500000
    if len(step_data)>Nvalues: 
        print("Smoothing down to",Nvalues)
        nsmooth=len(step_data)/Nvalues
    else:
        print("Not smoothing")
        nsmooth=1
    smoothed=[]
    for column in range(np.shape(step_data)[1]):
        data=step_data[:,column]
        smoothed.append(list(np.mean(data.reshape(-1,nsmooth),axis=1)))     #average over nsmooth values                                     

    smoothed=np.array(smoothed)      #CAREFUL, ROWS AND COLUMNS ARE SWAPPED
    with open(filename,'w') as f:
        writer=csv.writer(f)
        #Write title
        writer.writerow(["## FIELD NUMBER:",len(active_keys)])
        for field in active_keys:
            writer.writerow(['# {0} {1}'.format(field,key_lengths[field])])
        for i in range(np.shape(smoothed)[1]):
            writer.writerow(smoothed[:,i])
    del orig_file
    return

class Sinkfile(object):
    def __init__(self,folder="."):
        sink_folders=[sink_folder for sink_folder in glob.glob("{0}/SINK*".format(folder))]
        if not(sink_folders):
            print("Warning: you are in the WRONG LOCATION")
            print("============================")
            sys.exit(1)
        raw_data={}
        fields=[]
        field_lengths=[]
        for i in range(len(sink_folders)):
            filename="./{0}/sink_00001.txt".format(sink_folders[i])
            print("Opening",filename)
            with open(filename) as f:
                reader=csv.reader(f,delimiter='\t')
                n_fields=0
                reached_data=False
                for row in reader:
                    if row[0][1]=='#':
                        if reached_data or i>0:
                            continue
                        else:
                            n_fields+=1
                            label=row[0].split()
                            if label[1]=='dotMacc':
                                label[1]='dotMbondi'    #Rename appropriately
                            elif label[1]=='xsink':
                                label[1]='position'
                            elif label[1]=='msink':
                                label[1]='BHmass'
                            elif label[1]=='acc_momentum':
                                label[1]='dpacc'
                            elif label[1]=='jsink':
                                label[1]='jgas'
                            try:
                                field_lengths.append(int(float(label[2])))
                            except:
                                field_lengths.append(1)
                            fields.append(label[1])
                    else:
                        reached_data=True
                        row_list=row[0].split()
                        raw_data[int(row_list[0])]=sinks.FINEstep(row_list,fields,field_lengths)   
        data=[]
        for step in np.sort(list(raw_data.keys())):
            data.append(raw_data[step])
        self.data=np.array(data)
        self.n_fields=n_fields
        self.fields=fields

    def getattr(self,attribute):
        attributes=self.data[0].__dict__.keys()
        if not attribute in attributes:
            raise utils.EmptyException("This is not a valid attribute, choose one of {0}".format(attributes))
        else:
            return np.array([getattr(item,attribute) for item in self.data])

