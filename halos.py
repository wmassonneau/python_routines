##########################################################################
#This function runs the halo finder on the dataset, and then allows further
#proecssing of the results. Definitely allows plotting and such.
#
#
#
#
##########################################################################
import ytutils as myutils
import pictures as oldplots
import yt
import os.path
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
import utils

npart_global=10

#print('Many of these functions are unneccesarily long or badly located. Create an object that contains all halos for a given output, then turn many of these functions into attributes of Halo or Halos')

##############################################
# TOPIC: Trace particles/halos through time
#############################################

#Given two output numbers, this this routine calculates what percentage of particles in the former one are contained in the latter
def progenitors(initial,final,npart=npart_global,all=True,massive=False,sink=False,tofile=False,common=0.1):

    if not sink and not massive:
        print("Please pick either the sink halo or the most massive halo")
        return

    if sink:
        f_halo=which_halo(final,npart=npart)
        print("The target halo is chosen to be: the SINK halo at z=",round(f_halo.ds.current_redshift,2))
    if massive:
        f_halo=most_massive(final,npart=npart)
        print("The target halo is chosen to be: the MOST MASSIVE halo at z=",round(f_halo.ds.current_redshift,2))

    print("It has a mass of",f_halo.mvir.in_units('Msun'))

    halos,fracs=match_halos(initial,f_halo,npart=npart,all=all,common=common,sink=sink)

    if tofile:
        filename="progenitors_coarse_{0}.txt".format(initial,npart)
        print("Saving progenitor halo ids in",filename)
        with open(filename,'w+') as f:
            for halo in halos:
                f.write("{0}\n".format(halo.id))
    return halos

#This routine finds all progenitors to halo_f in dataset number initial:
def match_halos(initial,f_halo,npart=npart_global,all=True,common=0.1,sink=False,perc_i=True,id=""):
    f_parts=set(np.array(f_halo.getDM()))
    print("At redshift", round(f_halo.ds.current_redshift,2),"(output",f_halo.output,") the target halo,",f_halo.id,", contains",len(list(f_parts)),"particles. -")
    if all:
        i_halos=readhalos(initial,npart=npart)
        print("At redshift", round(i_halos[0].ds.current_redshift,2),"(output",i_halos[0].output,") there are",len(i_halos),"halos.")
        fracs=[]
        halos=[]
        for i_halo in i_halos:
            i_parts=set(np.array(i_halo.getDM()))
            both=list(set(i_parts)&set(f_parts))
            #print "Halo",i_halo.id,"contains",len(i_parts),"particles!"
            if (perc_i and len(i_parts)>0):     #print percentage of particles from initial halo in final halo
                frac=float(len(both))/len(i_parts)
            else:      #print percentage of particles from final halo in initial halo
                frac=float(len(both))/len(f_parts)

            if frac>=common:
                print("Halo",i_halo.id,"contains",len(list(i_parts)),"particles. =>",frac*100,"%.")
                fracs.append(frac)
                halos.append(i_halo)
        return halos,fracs

    if sink:
        i_halo=which_halo(initial,npart=npart)
        i_parts=set(np.array(i_halo.getDM()))
        both=list(set(i_parts)&set(f_parts))
        frac=float(len(both))/len(i_parts)
        print("Sinkhalo",i_halo.id,"contains",len(list(i_parts)),"particles. =>",frac*100,"%.")

    if id:
        i_halo=identify_halo(initial,id,npart=npart)
        i_parts=set(np.array(i_halo.getDM()))
        both=list(set(i_parts)&set(f_parts))
        frac=float(len(both))/len(i_parts)
        print("Halo",i_halo.id,"contains",len(list(i_parts)),"particles. =>",frac*100,"%.")



####################################
# TOPIC: Plotting functions
####################################

#This routine was written to projection along the roation axis (?) of the plot
def projection(halo,field='density',zoom=None,addendum=None,sink=True,grids=False,particles=False,plotit=True,rsink=0):
    print(''' Using the information contained in the Halo object to set up the plot is a good idea but this function
is very out of date. Rewrite, then use''')

    output=halo.output
    ds=myutils.load(output)
    halo.angularmomentum()
    #If no zoom is given, use the virial radius of the halo
    if not zoom:
        zoom=2*halo.rvir

    # set output name
    if type(field)==tuple:
        plotname='halo_project_{0}_{1}kpc_{2}'.format(field[1],int(round(zoom.in_units('kpc'))),output)
    else:
        plotname='halo_project_{0}_{1}kpc_{2}'.format(field,int(round(zoom.in_units('kpc'))),output)

    #load the sink
    if sink:
        halo.which_sink()
        if halo.hassink:
            sink=halo.sink
        else: sink=False

    #Set the region to avoid plotting the entire depth
    left=halo.pos-zoom
    right=halo.pos+zoom
    region=ds.region(halo.pos,left,right)


    myplot=yt.OffAxisProjectionPlot(ds=ds,normal=halo.Lall,fields=field,center=halo.pos,width=zoom,depth=zoom,axes_unit='kpc')
    if grids: myplot.annotate_grids()
    if particles: myplot.annotate_particles((zoom,'kpc'))

    #if a sink is used, draw in sink and refinement radius

    if sink:
        myplot.annotate_marker(sink.pos,marker=',',plot_args={'s':1,'c':'k'})
        myplot.annotate_sphere(sink.pos,(rsink,'kpc'),{'fill':False})

    #add addendum and save
        #addendum adds the addendum (if given) and .png to the plotname
    plotname=myutils.addendum(plotname,addendum)
    if plotit==True:
        myplot.save(plotname)
    return myplot

##########################################
# TOPIC: Read, Write and Identify
###########################################

#Find the halo that contains the sink
def which_halo(output,particles=[],npart=npart_global,halos=None,subhalos=False,info=False,stars=False):
    import sinks
    if not halos:
        halos=readhalos(output,npart,stars=stars)
    if subhalos==True:
        subhalos_list=readsubhalos(output,npart,stars=stars)
        halos=np.concatenate((halos,subhalos_list), axis=0)
    if not particles:
        particles=sinks.read(output)

    ds=myutils.load(output)
    for particle in particles:
        dists=[ds.arr(np.linalg.norm(halo.pos-particle.pos),'code_length') for halo in halos]

    if info:
        print("The distance to the halo center is",min(dists).in_units('pc'))
        halo=halos[dists.index(min(dists))]
        print("That is",np.round(min(dists)/halo.rvir,3),"virial radii")
        print("The sinhalo id is",halo.id)
        print("##############################")

    return halos[dists.index(min(dists))]

#Find the most massive halo
def most_massive(output,npart=npart_global,stars=False,folder='.'):
    halos=readhalos(output,npart,stars=stars,folder=folder)
    masses=[halo.mvir for halo in halos]
    return halos[masses==max(masses)][0]

#Find the a halo with a given id
def identify_halo(output,id,npart=npart_global,halos="",subhalos=False,stars=False,folder='.'):
    halos=readhalos(output,npart,stars=stars,folder=folder)
    if subhalos==True:
        subhalos_list=readsubhalos(output,npart,stars=stars)
        halos=np.concatenate((halos,subhalos_list), axis=0)
    ds=myutils.load(output)
    ids=np.array([halo.id for halo in halos])
    return halos[ids==id][0]

#Find subhalos for a given halo
def identify_subhalos(myhalo,npart=npart_global):
    all_subhalos=readsubhalos(myhalo.output,npart=npart)
    subhalos=[subhalo for subhalo in all_subhalos if subhalo.parent==myhalo.id]
    return subhalos

#Read all halos
def readhalos(output,npart=npart_global,stars=False,folder='.'):
    if stars:
        filename="./{2}/Data/yt_galaxies_{0}_{1}.txt".format(str(output).zfill(3),npart,folder)
    else:
        filename="./{2}/Data/yt_halos_{0}_{1}.txt".format(str(output).zfill(3),npart,folder)
    print("opening ",filename)
    ds=myutils.load(output,folder)
    with open(filename) as f:
        mylist=f.read().splitlines()
    myhalos=np.array([Halo(line,ds,npart) for line in mylist])
    return myhalos

#Read all subhalos
def readsubhalos(output,npart=npart_global,stars=False):
    print('Rewrite halos.readhalos such that it also handles subhalos')
    if stars:
        filename="Data/yt_subgalaxies_{0}_{1}.txt".format(str(output).zfill(3),npart)
    else:
        filename="Data/yt_subhalos_{0}_{1}.txt".format(str(output).zfill(3),npart)
    print("opening ",filename)
    ds=myutils.load(output)
    with open(filename) as f:
        mylist=f.read().splitlines()
    myhalos=np.array([Subhalo(line,ds) for line in mylist])
    return myhalos

#Write halo files
def writehalos(halos):
    import csv
    filename="sinkhalos.txt"
    print("Saving sinkhalos as",filename)
    with open(filename,'w') as f:
        writer=csv.writer(f)
        for halo in halos:
            data=np.append([halo.ouput,halo.id],np.array(halo.pos))
            data=np.append(data,[np.array(halo.rvir),np.array(halo.mvir),halo.npart])
            writer.writerow(data)

#Read halos previously written to file
def rereadhalos():
    import csv
    filename="sinkhalos.txt"
    print("Opening",filename)
    with open(filename) as f:
        reader=csv.reader(f)
        halos=[]
        for row in reader:
            ds=myutils.load(int(float(row[7])))
            line='\t'.join(row[0:7])
            halos.append(halo(line,ds))
    return halos

##########################################
#TOPIC: Halo properties
##########################################
def baryon_fraction(output,halos=None):
    if not halos:
        halos=readhalos(output,500)
    ds=myutils.load(output)
    ana_frac=ds.parameters['omega_b']/ds.parameters['omega_m']
    print("analytic fraction",ana_frac)
    print("calculate fraction for",output)
    real_fracs=np.array([halo.masses() for halo in halos])
    return real_fracs

#This function prints information for halos of a given output
def print_halo_stats(output,npart=npart_global):
    ds=myutils.load(output)
    myutils.add_filters(ds)

    halos=readhalos(output,npart=npart)
    for halo in halos:
        stars=halo.sphere[('stars','particle_mass')]
        gas=halo.sphere[('gas','density')]*halo.sphere[('index','cell_volume')]
        print("Halo",halo.id,"has a mass of",halo.mvir.in_units('Msun'),". It containts",len(stars),"stars and",halo.npart,"DM particles.Gas mass:")
    return

#Calculate the total mass of a given halo    
def total_mass(halo,npart=npart_global):
    print('Turn into Halo class attribute')
    subs=identify_subhalos(halo,npart)
    subs=[sub.mvir.in_units('Msun') for sub in subs]
    print("The halo has mass","%.2E"%halo.mvir.in_units('Msun'),"Msun")
    print("There are",len(subs),"subhalos")
    Msubs=halo.ds.quan(np.sum(subs),'Msun')
    print("The subhalos have a mass of","%.2E"%Msubs,"Msun")
    print("The total mass of this halo is","%.2E"%(Msubs+halo.mvir).in_units('Msun'),"Msun")
    return

##########################################
# TOPIC: Classes and Objects
########################################

class Halo(object):
    def __init__(self,mylist,ds,npart_load):

        mylist=mylist.split()
        self.id=int(float(mylist[0]))
        self.pos=ds.arr([float(i) for i in mylist[1:4]],'code_length')
        self.rvir=ds.quan(float(mylist[4]),'code_length')
        self.mvir=ds.quan(float(mylist[5])*1E11,'Msun')
        self.mh=ds.quan(float(mylist[6])*1E11,'Msun')
        self.npart=int(float(mylist[7]))
        self.ds=ds
        self.output=myutils.get_number(ds)
        self.subhalo=False
        self.sphere=ds.sphere(self.pos,self.rvir)
        self.npart_load=npart_load
    def addfilters(self):
        myutils.add_filters(self.ds)
    def masses(self):
        self.mDM=self.sphere.quantities.total_quantity(('DM','particle_mass'))
        self.mStars=self.sphere.quantities.total_quantity(('stars','particle_mass'))
        self.mGas=self.sphere.quantities.total_quantity('cell_mass')
        bar_frac=(self.mGas+self.mStars)/(self.mGas+self.mStars+self.mDM)
        return bar_frac

    def makeprofile(self,x="radius",y="cell_mass",addendum="",kwargs={"accumulation":True,"weight_field":None,"fractional":False},plotit=True):
        plot=yt.ProfilePlot(self.sphere,x,y,**kwargs)
        plot.set_unit(x,myutils.units(x))
        if type(y)==tuple:
            y=y[1]
        plot.set_unit(y,myutils.units(y))
        self.profile=plot.profiles[0]
        if plotit:
            plotname="profile_{0}_{1}_{2}".format(x,y,str(self.ds)[-3:])
            plotname=myutils.addendum(plotname,addendum)
            plot.save(plotname)

    def angularmomentum(self):
        self.Lgas=self.sphere.quantities.angular_momentum_vector(use_gas=True,use_particles=False)
        self.Lparts=self.sphere.quantities.angular_momentum_vector(use_gas=False,use_particles=True)
        self.Lall=self.sphere.quantities.angular_momentum_vector(use_gas=True,use_particles=True)
        return self.Lall

    def getDM(self,write=False,frac=1.0,highres_tracer=False):
        self.addfilters()
        if not frac==1.0:
            sphere=self.ds.sphere(self.pos,self.rvir*frac)
        else:
            sphere=self.sphere
        if ('DM','particle_identifier')in self.ds.derived_field_list:
            #print "DM mass"
            ids=sphere[('DM','particle_identifier')]
        else:
            #print "io mass"
            ids=sphere[('io','particle_identifier')]
        print("There are",len(ids),"particles within",frac,"virial radii.That's",sphere.radius.in_units('kpc'))
        if write:
            if highres_tracer:
                filename="highres_tracer"
            else:
                filename="sinkpos_ids"
            
            print("I am opening a file called",filename)
            with open(filename,'w+') as f:
                f.write("{0}\n".format(int(len(ids))))
                for id in ids:
                    f.write("{0}\n".format(int(id)))
        return ids

    def which_sink(self,sinks=[]):
        import sinks
        self.hassink=False
        if not sinks:
            sinks=sinks.read(self.output)
        for sink in sinks:
            dist=self.ds.arr(np.linalg.norm(self.pos-sink.pos),'code_length')
            if dist<=self.rvir:
                self.hassink=True
                self.sink=sink[0]
                break 
        

class Subhalo(Halo):
    def __init__(self,mylist,ds):
        mylist=mylist.split()
        self.id=int(float(mylist[0]))
        self.pos=ds.arr([float(i) for i in mylist[2:5]],'code_length')
        self.rvir=ds.quan(float(mylist[5]),'code_length')
        self.mvir=ds.quan(float(mylist[6])*1E11,'Msun')
        self.mh=ds.quan(float(mylist[7])*1E11,'Msun')
        self.npart=int(float(mylist[8]))
        self.ds=ds
        self.output=int(str(ds)[-5:])
        self.parent=int(float(mylist[1]))
        self.subhalo=True
