import ytutils
import utils
import matplotlib.pyplot as plt
import csv

def whole_box(output,folder='.',fields=['density'],tofile=True,nbins=100,addendum=''):
    ds=ytutils.load(output,folder=folder)
    sp=ds.sphere([0.5,0.5,0.5],ds.length_unit*ds.parameters['boxlen']*2)
    plot(sp,fields=fields,tofile=tofile,nbins=nbins,addendum=addendum+'_wholebox_{0}'.format(output))
    return

def boxcenter_sphere(output,radius=10,radius_unit='kpc',folder='.',fields=['density'],tofile=True,nbins=100,addendum=''):
    ds=ytutils.load(output,folder=folder)
    sp=ds.sphere([0.5,0.5,0.5],ds.quan(radius,radius_unit))
    plot(sp,fields=fields,tofile=tofile,nbins=nbins,addendum=addendum+'_sphere_{0}'.format(output))
    return

def plot(data,fields=['density'],tofile=True,nbins=100,addendum=None):
    ''' Creates average profiles at a given radius and writes them to file
    data: YT data object, e.g. all_data
    '''

    if not 'density' in fields:
        fields=fields+['density']
    fig,axes=plt.subplots(len(fields),1,sharex=True)
    if(len(fields)==1):
        axes=[axes]
    #Create data and plot
    yt_profile=ytutils.yt.create_profile(data,['radius'],fields=fields,weight_field='ones',accumulation=False,n_bins=nbins,extrema={'radius':(data.ds.index.get_smallest_dx(),data.radius)},logs={'radius':False})
    existing_data=yt_profile['density']>0
    profiles={}
    profiles['radii']=yt_profile.x.in_units('cm')[existing_data]
    print(profiles['radii'][0].in_units('kpc'))
    for i,field in enumerate(fields):
        ax=axes[i]
        if field=='entropy':
            profiles[field]=yt_profile[field].in_units('keV*cm**2')[existing_data]
        else:
            profiles[field]=yt_profile[field].in_base('cgs')[existing_data]
        ax.plot(profiles['radii'].in_units('kpc'),profiles[field])
        ax.set_ylabel(field)
        ax.set_yscale('log')

    #Write data to file for later post processing
    filename='profiles'
    if addendum:
        filename+='_{0}.csv'.format(addendum)
    else:
        filename+='.csv'
    print("Saving data as",filename)

    with open(filename,'w') as f:
        writer=csv.writer(f)
        writer.writerow(['### All fields in cgs units except when stated explicitly otherwise'])
        writer.writerow(['## TIME = {0} Myr'.format(data.ds.current_time.in_units('Myr'))])
        writer.writerow(['# radii [cm]']+list(profiles['radii'].v))
        for field in fields:
            writer.writerow(['# {0} [{1}]'.format(field,profiles[field].units)]+list(profiles[field].v))
    
    #Format figure
    axes[-1].set_xlabel('radius [kpc]')

    #Save figure
    figname='profiles'
    for field in fields:
        figname+='_{0}'.format(field)
    utils.saveas(fig,figname,addendum=addendum,pdf=False)
    
      
    
